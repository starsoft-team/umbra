using System;
using Wrench;
using System.IO;
using System.Collections;
using System.Collections.Generic;


namespace Tribalwars{
	
	/// <summary>
	/// Only used for a few buildings.
	/// </summary>
	
	public class LevelCost{
		
		public Building Building;
		public float BuildRate;
		public JSObject RawData;
		public int Food;
		public int Wood;
		public int Clay;
		public int Iron;
		/// <summary>Points gained total (not from getting this level).</summary>
		public int TotalPoints;
		
		public LevelCost(Building building){
			
			Building=building;
			RawData=new JSArray();
			
		}
		
		public LevelCost(Building building,JSObject obj){
			
			RawData=obj;
			Building=building;
			
			BuildRate=GetFloat("build_time");
			
			Food=(int)GetFloat("food");
			Wood=(int)GetFloat("wood");
			Clay=(int)GetFloat("clay");
			Iron=(int)GetFloat("iron");
			TotalPoints=(int)GetFloat("points");
			
		}
		
		/// <summary># of seconds until all the costs are available in a given village.</summary>
		/// <returns>MaxValue if they can never be available, e.g. because the farm is full or it'd exhaust the warehouse.</returns>
		public int AvailableIn(Village village,out Reason notPossibleReason){
			
			// 1. Food:
			ResourceInfo resource=village.GetResource("food");
			
			if(resource.Count<Food){
				notPossibleReason=Reason.OutOfPeople;
				return int.MaxValue;
			}
			
			// 2. Wood:
			resource=village.GetResource("wood");
			int biggestTime=resource.AvailableIn(Wood);
			
			// 3. Clay:
			resource=village.GetResource("clay");
			int time=resource.AvailableIn(Clay);
			
			if(time>biggestTime){
				biggestTime=time;
			}
			
			// 4. Iron:
			resource=village.GetResource("iron");
			time=resource.AvailableIn(Iron);
			
			if(time>biggestTime){
				biggestTime=time;
			}
			
			if(biggestTime==int.MaxValue){
				
				// This happens when the warehouse is too small:
				notPossibleReason=Reason.WarehouseTooSmall;
				
			}else{
				
				// Great, it's possible - no fail reason:
				notPossibleReason=Reason.None;
			
			}
			
			return biggestTime;
			
		}
		
		public override string ToString(){
			
			return "Food: "+Food+", Wood: "+Wood+", Clay: "+Clay+", Iron: "+Iron+", Time: "+BuildRate+"s, Points (total): "+TotalPoints;
			
		}
		
		public JSObject this[string property]{
			get{
				return RawData[property];
			}
			set{
				RawData[property]=value;
			}
		}
		
		public string GetString(string property){
			JSObject val=RawData[property];
			
			if(val==null){
				return null;
			}
			
			return val.ToString();
		}
		
		public float GetFloat(string property){
			
			string value=GetString(property);
			
			if(value==null){
				return 0f;
			}
			
			return float.Parse(value);
		}
		
	}
	
}