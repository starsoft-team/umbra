using System;
using System.Collections;
using System.Collections.Generic;


namespace Tribalwars{

	public class FarmingRun{
		
		public List<Village> Farms;
		public List<Farmer> Farmers;
		private int RemainingMapRequests;
		
		
		public FarmingRun(List<Farmer> farmers){
			
			Farmers=farmers;
			
			if(Farmers.Count==0){
				return;
			}
			
			// Get the block of map segments surrounding these farmers.
			// -> All map segments within a 3x3 region around each farmer.
			Dictionary<uint,MapSection> mapRequests=new Dictionary<uint,MapSection>();
			
			foreach(Farmer farmer in Farmers){
				
				// Reset rem capacity:
				farmer.RemainingCapacity=farmer.Capacity;
				
				// Clear farms:
				farmer.FarmableVillages.Clear();
				
				int regionX=farmer.Village.X / 25;
				int regionY=farmer.Village.Y / 25;
				
				for(int y=-1;y<2;y++){
					
					for(int x=-1;x<2;x++){
						
						// Get the map code:
						uint code=(uint)((x+regionX)<<16) | (uint)(y+regionY);
						
						// Already requesting it?
						if(!mapRequests.ContainsKey(code)){
							
							// Get it:
							mapRequests[code]=MapSection.Get((x+regionX),(y+regionY));
							
						}
						
					}
					
				}
				
			}
			
			Account firstAccount=Farmers[0].Village.Account;
			
			RemainingMapRequests=mapRequests.Count;
			
			// Require a load of all map sections:
			foreach(KeyValuePair<uint,MapSection> kvp in mapRequests){
				
				kvp.Value.RequireLoad();
				
				// Get the content now:
				kvp.Value.GetContent(firstAccount,OnSectionLoaded);
				
			}
			
			
		}
		
		public void OnSectionLoaded(MapSection segment){
			
			if(Farms==null){
				Farms=new List<Village>();
			}
			
			// For each barb..
			foreach(Village village in segment.Villages){
				
				if(village.Owner==null){
					
					// Barb!
					Farms.Add(village);
					
				}
				
			}
			
			RemainingMapRequests--;
			
			if(RemainingMapRequests<=0){
				// Got all map data - send the farming run now!
				Send();
			}
			
		}
		
		public void Send(){
			
			// Greedy algo - pair all farms to all farmers, sort by distance, and accept the nearest ones.
			SortedDictionary<double,List<FarmFarmerPair>> distances=new SortedDictionary<double,List<FarmFarmerPair>>();
			
			foreach(Farmer farmer in Farmers){
				
				foreach(Village farm in Farms){
				
					FarmFarmerPair pair=new FarmFarmerPair();
					pair.Farmer=farmer;
					pair.Farm=farm;
					
					// Compute distance:
					double distance=farm.Distance(farmer.Village.X,farmer.Village.Y);
					
					// Add:
					List<FarmFarmerPair> set;
					if(!distances.TryGetValue(distance,out set)){
						
						set=new List<FarmFarmerPair>();
						distances[distance]=set;
						
					}
					
					// Add the pair:
					set.Add(pair);
					
				}
				
			}
			
			// Next, for each pair (starting from the shortest distances), accept it unless the farmer is over-subscribed.
			foreach(KeyValuePair<double,List<FarmFarmerPair>> kvp in distances){
				
				List<FarmFarmerPair> pairs=kvp.Value;
				
				foreach(FarmFarmerPair pair in pairs){
					
					// Accept if farmer isn't over subbed:
					
					if(pair.Farmer.RemainingCapacity>0){
						
						// Accept this link!
						pair.Farmer.RemainingCapacity--;
						
						// pair.Farm is to be farmed by pair.Farmer now!
						pair.Farmer.Add(pair.Farm);
						
					}
					
				}
				
			}
			
			// Finally, send the hits!
			foreach(Farmer farmer in Farmers){
				
				farmer.FarmNow();
				
			}
			
			
		}
		
	}

}