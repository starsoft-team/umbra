namespace Tribalwars{
	
	public class AccountData{
		
		public int ID;
		/// <summary>Username.</summary>
		public string Username;
		/// <summary>This accounts bpo.</summary>
		public int BashPointsOffense;
		/// <summary>This accounts bpd.</summary>
		public int BashPointsDefense;
		public Tribe Tribe;
		
		
		public AccountData(int id){
			ID=id;
		}
		
		public AccountData(){}
		
		public int TribeID{
			get{
				
				if(Tribe==null){
					return 0;
				}
				
				return Tribe.ID;
				
			}
		}
		
		public override string ToString(){
			
			string tribeTag="No tribe";
			
			if(Tribe!=null){
				tribeTag=Tribe.ToString();
			}
			
			return Username+" ("+tribeTag+")";
			
		}
		
	}
	
}