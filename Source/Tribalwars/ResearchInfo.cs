using System;
using System.Collections;
using System.Collections.Generic;
using Wrench;

namespace Tribalwars{

	public class ResearchInfo{
		
		public string Name;
		public bool Active;
		public bool Unlocked;
		public BuildingLevelInfo Building;
		
		
		public ResearchInfo(BuildingLevelInfo buildingInfo,string name){
			
			Name=name;
			Building=buildingInfo;
			
		}
		
	}
	
}