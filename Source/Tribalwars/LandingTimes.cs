using System;
using System.Collections;
using System.Collections.Generic;


namespace Tribalwars{
	
	public class LandingTimes{
		
		/// <summary>Gaps between landings in seconds.</summary>
		public int Spacing;
		/// <summary>The time of the first landing.</summary>
		public uint FirstLandTime;
		/// <summary>The movements in the order they should land.</summary>
		public List<ArmyMovement> Movements;
		
		
		public LandingTimes(uint firstLandTime,int spacing){
			
			Movements=new List<ArmyMovement>();
			FirstLandTime=firstLandTime;
			Spacing=spacing;
			
		}
		
		public override string ToString(){
			
			return GetText();
			
		}
		
		public void AddRam(Village from,Village to){
			
			ArmyMovement movement=new ArmyMovement(true,from,to);
			movement.Add("ram",1);
			
			Add(movement);
			
		}
		
		public static string ToHourMinutes(int dur){
			
			int hour=dur / 3600;
			
			dur-=(hour*3600);
			
			int min=dur / 60;
			
			dur-=(min * 60);
			
			return hour+"hours, "+min+" mins, "+dur+"s";
			
		}
		
		public void AddSword(Village from,Village to){
			
			ArmyMovement movement=new ArmyMovement(true,from,to);
			movement.Add("sword",1);
			
			Add(movement);
			
		}
		
		public void AddAxe(Village from,Village to){
			
			ArmyMovement movement=new ArmyMovement(true,from,to);
			movement.Add("axe",1);
			
			Add(movement);
			
		}
		
		public void AddNoble(Village from,Village to){
			
			ArmyMovement movement=new ArmyMovement(true,from,to);
			movement.AddNoble();
			
			Add(movement);
			
		}
		
		public string GetText(){
			
			uint landTime=FirstLandTime;
			
			string result="";
			
			int count=Movements.Count;
			
			for(int i=0;i<count;i++){
				
				// Get the movement:
				ArmyMovement movement=Movements[i];
				
				// What time?
				uint launchTime=movement.GetLaunchTime(landTime);
				
				int dur=movement.Duration;
				
				result+=Time.GetDate(launchTime).ToString("dd MM HH:mm:ss")+" ("+dur+", "+ToHourMinutes(dur)+") launch "+movement+"\r\n";
				
				// Next landing time:
				landTime=(uint)(Spacing+landTime);
				
			}
			
			return result;
			
		}
		
		public void Add(ArmyMovement movement){
			
			Movements.Add(movement);
			
		}
		
	}
	
}