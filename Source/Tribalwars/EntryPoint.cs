using System;
using System.IO;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using PowerUI;


namespace Tribalwars{


	public static class EntryPoint{
		
		/// <summary>A server used for official TW clients to connect to (in middleman mode).</summary>
		public static LocalServer LocalServer;
		
		
		public static void Start(){
			
			// Setup buildings and unit info:
			Buildings.Setup();
			Units.Setup();
			
			/*
			Village loki1=new Village(656,681,"TL001");
			Village loki2=new Village(654,681,"TL002");
			Village capitol=new Village(632,686,"The Capitol");
			Village jv=new Village(631,689,"Johnnyville");
			Village banzai=new Village(634,689,"Banzaiville");
			Village casaMia=new Village(664,667,"Casa mia");
			Village ilMio=new Village(666,664,"il mio villaggio");
			Village xzen1=new Village(656,676,"001 BALSURSUR");
			Village xzen3=new Village(655,677,"003 BALSURSUR");
			Village xzen4=new Village(654,674,"004 BALSURSUR");
			Village mini1=new Village(627,690,"village");
			Village mini2=new Village(628,689,"mini3-d-2");
			Village rivIW=new Village(617,692,"Inner Wall");
			Village blen=new Village(638,674,"Blenheim");
			
			LandingTimes landing=new LandingTimes(1447596000,0);
			*/
			
			//landing.AddNoble(rivIW,xzen1);
			
			/*
			landing.AddRam(capitol,xzen3);
			landing.AddNoble(capitol,xzen3);
			landing.AddSword(capitol,xzen3);
			landing.AddRam(ilMio,xzen3);
			landing.AddAxe(ilMio,xzen3);
			landing.AddNoble(casaMia,loki1);
			
			landing.AddNoble(mini1,xzen3);
			landing.AddNoble(mini2,xzen3);
			
			landing.AddNoble(mini1,loki2);
			landing.AddNoble(mini2,loki2);
			
			landing.AddNoble(mini1,xzen1);
			landing.AddNoble(mini2,xzen1);
			
			landing.AddNoble(mini1,loki1);
			landing.AddNoble(mini2,loki1);
			*/
			
			//landing.AddAxe(blen,loki2);
			//landing.AddAxe(blen,xzen4);
			
			//Console.WriteLine(landing.GetText());
			
			/*
			ScoutReport report=new ScoutReport("scout_test.txt");
			
			Console.WriteLine("Points: "+report.ApparentPoints);
			
			Console.WriteLine("Population: "+report.Population);
			
			// Stop there (block it from logging in whilst testing other stuff).
			// Just comment this out if you want to do a full test.
			return;
			*/
			
			if(Arguments.IsSet("map")){
				
				// Scan the map:
				// This will generate a bunch of files in Release/Accounts and Release/Villages
				MapScanner.Active=true;
				
			}
			
			if(Arguments.IsSet("verbose")){
				
				// Makes it display all the commands passing through.
				CommandLine.Verbose=true;
				
			}
			
			// Donation occurs by default now as its always looking for "Donor" villages.
			if(!Arguments.IsSet("nodonate")){
				TribeDonator.Active = true;
				
				// Start the donator:
				TribeDonator.Start();
				
			}
			
			// Get account IDs:
			string accountArgs=Arguments.Get("accounts");
			
			if(string.IsNullOrEmpty(accountArgs)){
				Console.WriteLine("Please provide account IDs. accounts:1,2,3..");
				return;
			}
			
			// Get IDs:
			string[] accounts=accountArgs.Split(',');
			
			// Get an account ID from args:
			for(int i=0;i<accounts.Length;i++){
				
				// Get the account ID:
				int accountID=int.Parse( accounts[i] );
				
				// Load an account:
				Account account=new Account( accountID );
				
				Console.WriteLine("Connecting with '"+account.Username+"'..");
				
				// ProJohnny is the major acc for resourceNet:
				// - We do this check to make sure that multiple accounts 
				//   don't simultaneously answer resource net requests 
				// (seeing as all connected accounts will all receive a copy of the forum message, so it'd reply multiple times otherwise)
				if(account.Username=="ProJohnny"){
					
					Console.WriteLine("ResourceNet active.");
					
					// Apply the account:
					ResourceNet.MainAccount=account;
					
				}
				
				// Connect:
				account.Connect();
				
				// When it's ready..
				account.addEventListener("onready",delegate(UIEvent e){
					
					// Get the account:
					Account acc=e.Account;
					
					if(acc.Tribe != null)
						Console.WriteLine("Tribe Name: " + acc.Tribe.Name);
					else
						Console.WriteLine("Tribe not loaded!");
					
					if(MapScanner.Active){
						
						// Start the map scanner:
						MapScanner.Start(acc);
						
					}
					
					// Start handling the resource depo:
					ResourceDeposit.OpenAndCollect(acc);
					
					Console.WriteLine("Connected to "+acc.Username+" ("+(acc.Tribe==null)+")");
					
					// acc.Client.Send("Map/getKingdoms");
					
					// acc.Client.Send("Kingdom/getInfo","{\"kingdom_id\":46}");
					
					/*
					foreach(KeyValuePair<int,Village> kvp in acc.Villages){
						
						int total=(kvp.Value.FarmingCapacity);
						
						Console.WriteLine("Farming capacity: "+kvp.Value.Name+": "+total);
						
					}
					*/
					
					/*
					Village village=acc.GetVillage("Tucson");
					
					Console.WriteLine("Village "+village.Name+". "+village.ResourceText());
					
					Console.WriteLine(village.BuildingText());
					
					village=acc.GetVillage("Phoenix");
					
					village.DonateAll();
					*/
					
					/*
					BuildingLevelInfo info=village.GetBuilding("academy");
					info.UpgradeNow();
					*/
					
					/*
					
					// Get banzaiville:
					Village village=acc.GetVillage("Banzaiville");
					
					if(village==null){
						Console.WriteLine("Village not found.");
					}else{
						Console.WriteLine("Village: "+village.ID);
					}
					
					// Get report:
					Report report=new Report("5244828_726738_81fafe358815d1f95",acc);
					
					report.GetContent(delegate(Report rep){
						
						ScoutReport scout=rep.Content as ScoutReport;
						
						if(scout!=null){
							
							Console.WriteLine("Got report contents!");
							
							scout.Points=711;
							
							Console.WriteLine(scout.ToString());
							
							Console.WriteLine("Camo? "+scout.IsCamoflauged+". "+scout.ApparentPoints);
							
						}
						
					});
					
					*/
					
					// Get the building:
					// BuildingLevelInfo building=village.GetBuilding("iron_mine");
					
					// The time until the next level is..
					// Console.WriteLine("Time for building level "+building.Level+" of '"+building.Building.RefName+"' is "+building.TimeToNextLevel);
					
					// Queue an upgrade now:
					// building.Upgrade();
					
				});
				
			}
			
		}
		
	}
	
}