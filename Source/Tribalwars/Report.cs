using System;
using System.Collections;
using System.Collections.Generic;
using Wrench;


namespace Tribalwars{
	
	public delegate void OnReportEvent(Report rep);
	
	/// <summary>
	/// Represents report info, e.g. scout reports.
	/// Use GetContent() to make sure the content of the report is available.
	/// </summary>
	
	public class Report{
		
		public static Report Parse(string text,Account loaderAccount){
			
			string[] pieces=text.Split(new string[]{"[report]"},StringSplitOptions.None);
			
			if(pieces.Length==1){
				return null;
			}
			
			string[] parts=pieces[1].Split(new string[]{"[/report]"},StringSplitOptions.None);
			
			// Get the report ID:
			string token=parts[0];
			
			return new Report(token,loaderAccount);
			
		}
		
		/// <summary>A set of loading reports.</summary>
		public static List<Report> AllLoading;
		
		public int ID;
		/// <summary> e.g. "attack" (by this account - *not* an incoming one), "defense" (from an incoming), "scouting".</summary>
		public string Type;
		/// <summary>Token if ID is unknown.</summary>
		public string Token;
		public string Title;
		public bool FullHaul;
		/// <summary>A specific type of report content. E.g. attack info etc.</summary>
		public ReportContent Content;
		/// <summary>True if a load is in progress.</summary>
		private bool Loading;
		public uint Time;
		/// <summary>An account used to request the report contents. Not necessarily the owner of the report.</summary>
		public Account Account;
		/// <summary>'1' is all clear, '3' is a total loss, '2' is with casualties.</summary>
		public int Status;
		/// <summary>Called when the report loads fully.</summary>
		public event OnReportEvent OnLoaded;
		
		
		public Report(string token,Account account){
			
			Token=token.Replace("_",".");
			Account=account;
			
		}
		
		public Report(JSObject data,Account account){
			
			Account=account;
			
			ID=int.Parse(data["id"].ToString());
			
			Type=data["type"].ToString();
			
			Token=data["token"].ToString();
			
			Title=data["title"].ToString();
			
			JSObject jsTemp=data["haul"];
			
			if(jsTemp!=null){
				
				// "partial" otherwise.
				FullHaul=(jsTemp.ToString()=="full");
				
			}
			
			Time=uint.Parse(data["time_created"].ToString());
			
			jsTemp=data["result"];
			
			if(jsTemp!=null){
				
				Status=int.Parse(jsTemp.ToString());
				
			}
			
		}
		
		/// <summary>True if this report is fully loaded and 'Content' is available.</summary>
		public bool FullyLoaded{
			get{
				return (Content!=null);
			}
		}
		
		public bool IsScout{
			get{
				return (Type=="scouting");
			}
		}
		
		public void GetContent(OnReportEvent onLoaded){
			
			if(FullyLoaded){
				
				onLoaded(this);
				return;
				
			}
			
			OnLoaded+=onLoaded;
			
			if(!Loading){
				
				Loading=true;
				
				if(AllLoading==null){
					AllLoading=new List<Report>();
				}
				
				AllLoading.Add(this);
				
				if(ID==0){
					
					// Get by token. Occurs with reports from the forums.
					Account.Client.Send("Report/getByToken","{\"token\":\""+Token+"\"}");
					
				}else{
				
					Account.Client.Send("Report/get","{\"id\":"+ID+"}");
				
				}
				
			}
			
		}
		
		public string Analyse(){
			
			return Content.Analyse();
			
		}
		
		public void GotData(JSObject args){
			
			// Type:
			string type=args["type"].ToString();
			
			if(Type==null){
				
				// Get title/ time etc:
				Type=type.Replace("Report","").ToLower();
				
				Time=uint.Parse(args["time_created"].ToString());
				
				Title=args["title"].ToString();
			
			}
			
			// Data specific to a particular type next:
			JSObject typeData=args[type];
			
			if(typeData==null){
				
				// Just a blank one:
				Content=new ReportContent();
				Content.Report=this;
				
				Loaded();
				return;
			}
			
			switch(type){
				
				case "ReportAttack":
					
					// an attack - can be either this account being or doing the attack.
					
					AttackReportContent attack=new AttackReportContent();
					
					UnitsSet attackTroops=new UnitsSet( typeData["attUnits"] as JSArray );
					UnitsSet attackLosses=new UnitsSet( typeData["attLosses"] as JSArray );
					
					attack.AttackerUnits=attackTroops;
					attack.AttackerLosses=attackLosses;
					
					UnitsSet defTroops=new UnitsSet( typeData["defUnits"] as JSArray );
					UnitsSet defLosses=new UnitsSet( typeData["defLosses"] as JSArray );
					
					attack.DefenderUnits=defTroops;
					attack.DefenderLosses=defLosses;
					
				break;
				
				case "ReportScouting":
					
					ScoutReport scout=new ScoutReport();
					
					bool unitsReport=typeData["commandType"].ToString()=="units";
					
					if(unitsReport){
						
						JSArray unitData=typeData["defUnitsVillage"] as JSArray;
						
						scout.Units=new UnitsSet(unitData);
						
					}else{
						
						JSArray buildData=typeData["defBuildings"] as JSArray;
						
						// Level set:
						BuildingLevels set=new BuildingLevels();
						
						set.Load(buildData);
						
						scout.Buildings=set;
						
						// Don't always get resources - check if their set:
						JSArray res=typeData["defResources"] as JSArray;
						
						if(res!=null){
							
							scout.Iron=int.Parse(res["iron"].ToString());
							scout.Clay=int.Parse(res["clay"].ToString());
							scout.Wood=int.Parse(res["wood"].ToString());
							
						}
						
					}
					
					// Set content:
					Content=scout;
					
				break;
				
				case "ReportTransportArrived":
					
					// received supplies
					
					// Just a blank one:
					Content=new ReportContent();
					
				break;
				
				case "ReportSupportStarted":
					
					// somebody supported a village
					
					// Just a blank one:
					Content=new ReportContent();
					
				break;
				
				case "ReportSupportBack":
					
					// support is now leaving
					
					// Just a blank one:
					Content=new ReportContent();
					
				break;
				
				//etc
				
				default:
					
					// Just a blank one:
					Content=new ReportContent();
					
				break;
				
			}
			
			Content.Report=this;
			
			Loaded();
			
		}
		
		private void Loaded(){
			
			OnLoaded(this);
			
		}
		
	}
	
}