using System;


namespace Tribalwars{
	
	public class ActiveEffect{
		
		public string Type;
		public bool Permanent;
		public uint Expires;
		public float Factor;
		
	}
	
}