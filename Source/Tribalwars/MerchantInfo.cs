using System;
using System.Collections;
using System.Collections.Generic;
using Wrench;

namespace Tribalwars{

	public class MerchantInfo{
		
		public int Busy;
		public int Free;
		public Village Village;
		
		public int Total{
			get{
				return Busy+Free;
			}
		}
		
		public Account Owner{
			get{
				return Village.Account;
			}
		}
		
		public MerchantInfo(Village village,int free,int busy){
			
			Village=village;
			Busy=busy;
			Free=free;
			
		}
		
	}
	
}