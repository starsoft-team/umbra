namespace Tribalwars{
	
	public class World{
		
		public static World Current=new World(13);
		
		public int ID;
		public float Speed=1f;
		
		
		public World(int id){
			ID=id;
		}
		
		public string CodeName{
			get{
				return "en"+ID;
			}
		}
		
	}

}