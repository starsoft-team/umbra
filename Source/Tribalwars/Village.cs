using System;
using System.Collections;
using System.Collections.Generic;
using Wrench;
using PowerUI;


namespace Tribalwars{
	
	public partial class Village{
		
		/// <summary>Parses a BB link typically from the forum or pm's.</summary>
		public static Village Parse(string forumLink){
			return Parse(forumLink,null);
		}
		
		/// <summary>Parses a BB link typically from the forum or pm's. If an account is given,
		/// The village will be the same village object as the one contained in the account.</summary>
		public static Village Parse(string forumLink,Account account){
			
			// Split it up:
			string[] villagePieces=forumLink.Split(new string[]{")[/village]"},StringSplitOptions.None);
			villagePieces=villagePieces[0].Split(new string[]{"[village="},StringSplitOptions.None);
			
			if(villagePieces.Length==1){
				
				// Might be just a name.
				if(account==null){
					return null;
				}
				
				return account.GetVillage(forumLink.Trim());
				
			}
			
			// Grab the village ID, name and coords:
			string villageInfo=villagePieces[1];
			
			//villageInfo is "ID]Name (X|Y"
			villagePieces=villageInfo.Split(']');
			
			int villageID;
			if( int.TryParse(villagePieces[0].Trim(),out villageID) ){
				
				// Got a village ID!
				if(account!=null){
					
					return account.GetVillage(villageID);
					
				}
				
				string nameAndCoords=villagePieces[1];
				
				// Get the name and coords:
				int bracketIndex=nameAndCoords.LastIndexOf('(');
				
				// Get the village name, excluding the space before the bracket:
				string villageName=nameAndCoords.Substring(0,bracketIndex-1);
				
				// Get the coords:
				string coords=nameAndCoords.Substring(bracketIndex+1);
				
				// Split the coords:
				villagePieces=coords.Split('|');
				
				// Parse the coords:
				int x=int.Parse(villagePieces[0]);
				int y=int.Parse(villagePieces[1]);
				
				// Apply the village ID:
				int id=villageID;
				
				// Controlling this village?
				Village village=Accounts.GetVillage(id);
				
				if(village==null){
					
					village=new Village(x,y,villageName);
					
					village.ID=id;
					
				}
				
				return village;
				
			}
			
			return null;
			
		}
		
		public int X;
		public int Y;
		public int ID;
		public World World;
		public int Points;
		public string Name;
		public AccountData Owner;
		
		/// <summary>Gets the connected account object.</summary>
		public Account Account{
			get{
				return Owner as Account;
			}
		}
		
		/// <summary>Shortcut to a client object which deals with sending/receiving.</summary>
		public GameClient Client{
			get{
				return Account.Client;
			}
		}
		
		/// <summary>Available storage.</summary>
		public int Storage;
		/// <summary>The last time that resources were updated at (UTC, seconds).</summary>
		public uint ResourcesLastTime;
		/// <summary>The groups this village is in (e.g. it's an offsensive/resource village). 
		/// Can be null. See IsInGroup and e.g. IsDonor.</summary>
		public GroupInfo[] Groups;
		/// <summary>Merchants in this village.</summary>
		public MerchantInfo Merchants;
		/// <summary>The units in this village.</summary>
		public Dictionary<string,CombatUnits> UnitInfo=new Dictionary<string,CombatUnits>();
		public Dictionary<string,BuildingLevelInfo> BuildingInfo=new Dictionary<string,BuildingLevelInfo>();
		public Dictionary<string,ResourceInfo> ResourceInfo=new Dictionary<string,ResourceInfo>();
		
		
		public Village(AccountData owner,World world,int id){
			Owner=owner;
			World=world;
			ID=id;
			
			SetupFull();
			
		}
		
		public Village(int x,int y,string name){
			X=x;
			Y=y;
			World=World.Current;
			Name=name;
		}
		
		public Village(AccountData owner,World world,int id,int x,int y){
			
			Owner=owner;
			World=world;
			ID=id;
			X=x;
			Y=y;
			
			SetupFull();
			
		}
		
		public UnitsSet OffensiveNoNoble{
			get{
				
				// Create the set:
				UnitsSet set=new UnitsSet(this);
				
				set.Add(UnitInfo,Units.OffensiveNoNoble);
				
				return set;
				
			}
		}
		
		public UnitsSet Offensive{
			get{
				
				// Create the set:
				UnitsSet set=new UnitsSet(this);
				
				set.Add(UnitInfo,Units.Offensive);
				
				return set;
				
			}
		}
		
		public UnitsSet Defensive{
			get{
				
				// Create the set:
				UnitsSet set=new UnitsSet(this);
				
				set.Add(UnitInfo,Units.Defensive);
				
				return set;
				
			}
		}
		
		/// <summary>Distance to a given point.</summary>
		public double Distance(int x,int y){
			
			double dx=(X-x);
			double dy=(Y-y);
			
            if((dy % 2)!=0){
				
				if((Y % 2)!=0){
					dx += .5;
				}else{
					dx += -.5;
				}
				
			}
			
			return Math.Sqrt(dx * dx + dy * dy * .75);
			
		}
		
		public void GetAcademy(UrgencySet set){
			
			// HQ, Warehouse, Acad itself.
			
			bool acad=true;
			
			if(GetQueuedLevel("headquarter")<20){
				
				// Do the HQ:
				set.AddBuilding(this,"headquarter");
				
				acad=false;
				
			}
			
			if(GetQueuedLevel("warehouse")<22){
				
				// Do the warehouse:
				set.AddBuilding(this,"warehouse");
				
				acad=false;
				
			}
			
			if(acad){
			
				set.AddBuilding(this,"academy");
				
			}
			
		}
		
		/// <summary>One-shot manage this village.</summary>
		/// <param name='threat'>If threat is high, the focus will be on troops and barracks. 
		/// Otherwise, the focus is on resources.</param>
		public void Manage(float threat){
			
			string toBuild=null;
			
			// Create the set for building:
			UrgencySet buildSet=new UrgencySet();
			
			if(IsDonor){
				
				// Donate everything:
				DonateAll();
				
			// Is it a coin village?
			}else if(IsCoins){
				
				// Does it now require an acad?
				if(RequireAcademy){
					
					// Get an academy.
					GetAcademy(buildSet);
					
				}else{
					
					// Mint all (or deliver to nearest academy):
					Village nearestAc=NearestAcademy;
					
					if(nearestAc==this){
						MintAll();
					}else if(nearestAc!=null){
						
						if(nearestAc.Distance(X,Y) < 8f){
							
							// Deliver to nearest acad:
							SendResources(30000,30000,30000,nearestAc.ID);
							
						}
						
					}
					
					// Recruit with the rest:
					RecruitNow(0,0,0,0);
					
					// Donate any excess:
					DonateExcess();
					
				}
			
			}else{
				
				// Growth mode.
				
				// Is the HQ too low to build what we want? If yes, favourite that.
				int level=GetQueuedLevel("headquarter");
				
				if(level<16){
					
					// Build HQ.
					toBuild="headquarter";
					
				}
				
				// How about the wall?
				level=GetQueuedLevel("wall");
				
				if(level!=20){
					
					// How much of a threat is there?
					if(threat>0.5){
						
						toBuild="wall";
						
					}
					
				}
				
				// Barracks?
				level=GetQueuedLevel("barracks");
				
				if(level<21){
					
					toBuild="barracks";
					
				}
				
				// Market?
				level=GetQueuedLevel("market");
				
				if(level<20){
					
					toBuild="market";
					
				}
				
				// Res buildings?
				toBuild=BestResourceToBuild();
				
				if(CheckCoinCertified()){
					
					AssignGroup("Coins");
				
				}
				
			}
			
			// -> I would rather be building something smaller (e.g. market, wall) 
			//    than have it waiting to build something big.
			
			string name=buildSet.BestChoiceName;
			
			if(name!=null){
				// Try queuing it!
				Build(name);
			}
			
			
		}
		
		/// <summary>Recruits whilst leaving the min amount of resources.</summary>
		public void RecruitNow(int minWood,int minClay,int minIron,int minFood){
			
			// How long is the recruit queue? if it's already ages (Say, >12h), do nothing.
			long msToGo=(RecruitQueueEnd - Time.NowMilliseconds);
			
			if(msToGo>12 * 60 * 60 * 1000){
				
				// 12h.
				return;
				
			}
			
			// Add 50% of the max amount of spears/swords/archers that we can.
			// Repeat until we have used up resources that do not extend below min counts.
			int woodAvailable=(int) (Wood - minWood);
			int clayAvailable=(int) (Clay - minClay);
			int ironAvailable=(int) (Iron - minIron);
			int foodAvailable=(int) (Food - minFood);
			
			// Queue stuff!
			
			while(true){
				
				int def=DefensiveState;
				string weakest;
				
				if(def==1){
					// Def!
					
					// How many def troops have we got in total in this village?
					int spearCount=GetQueuedCount("spear");
					int swordCount=GetQueuedCount("sword");
					int archCount=GetQueuedCount("archer");
					
					// Which of spear, sword and arch is the relative weakest?
					// -> Keyword: relative. We try to have a 1:1:0.5 ratio.
					// -> Thus, act like there are twice as many archers.
					archCount*=2;
					
					weakest="spear";
					int min=spearCount;
					
					if(swordCount<min){
						weakest="sword";
						min=swordCount;
					}
					
					if(archCount<min){
						weakest="archer";
						min=archCount;
					}
					
				}else if(def==0){
					// Off!
					
					// How many def troops have we got in total in this village?
					int axeCount=GetQueuedCount("axe");
					int lcCount=GetQueuedCount("light_cavalry");
					
					// Which of lc or axes is the relative weakest?
					// -> Keyword: relative. We try to have a 4:1 ratio.
					// -> Thus, act like there are 4x as many lc.
					lcCount*=4;
					
					weakest="axe";
					int min=axeCount;
					
					if(lcCount<min){
						weakest="light_cavalry";
						min=lcCount;
					}
					
				}else{
					break;
				}
				
				// How many of weakest can we make?
				UnitData unitData=Units.Get(weakest);
				
				// Get the amount it can make for each:
				int peopleReq=foodAvailable / unitData.Food;
				int woodReq=woodAvailable / unitData.Wood;
				int clayReq=clayAvailable / unitData.Clay;
				int ironReq=ironAvailable / unitData.Iron;
				
				int maxCount=peopleReq;
				
				if(woodReq<maxCount){
					maxCount=woodReq;
				}
				
				if(clayReq<maxCount){
					maxCount=clayReq;
				}
				
				if(ironReq<maxCount){
					maxCount=ironReq;
				}
				
				if(maxCount<=1){
					// Done!
					break;
				}
				
				if(maxCount>50){
					
					// Recruit half that many and go around again!
					maxCount/=2;
					
				}
				
				// Recruit maxCount of weakest.
				foodAvailable-=unitData.Food * maxCount;
				woodAvailable-=unitData.Wood * maxCount;
				clayAvailable-=unitData.Clay * maxCount;
				ironAvailable-=unitData.Iron * maxCount;
				
				// Units info:
				CombatUnits info=GetUnit(unitData.RefName);
				
				// Create the entry:
				RecruitQueueEntry queueEntry=new RecruitQueueEntry(info,maxCount,this);
				
				// Queue it now:
				RecruitQueue.Add(queueEntry);
				
			}
			
		}
		
		public string BestResourceToBuild(){
			
			// Which res building should be built?
			// -> Usually the one which is the lowest level
			// -> If they're equal, pick based on resource count
			// -> This applies if the lowest level has unusually lots too!
			
			// Levels:
			int clay=GetQueuedLevel("clay_pit");
			int iron=GetQueuedLevel("iron_mine");
			int timber=GetQueuedLevel("timber_camp");
		
			// Relative amounts:
			float clayCount=RelativeResourceCount("clay");
			float ironCount=RelativeResourceCount("iron");
			float woodCount=RelativeResourceCount("wood");
			
			// Get the spread:
			int min=clay;
			string minN="clay_pit";
			int max=clay;
			
			if(iron<min){
				min=iron;
				minN="iron_mine";
			}else if(iron==min){
				
				// Is iron "more" min?
				if(ironCount<clayCount){
					// Yep!
					minN="iron_mine";
				}
				
			}
			
			if(iron>max){
				max=iron;
			}
			
			if(timber<min){
				min=timber;
				minN="timber_camp";
			}else if(timber==min){
				
				// Is wood "more" min?
				if( woodCount<RelativeResourceCount(minN) ){
					// Yep!
					minN="timber_camp";
				}
				
			}
			
			if(timber>max){
				max=timber;
			}
			
			// If they're equal (+- 1 level of each other):
			bool equal=((max-min)<=1);
			
			if(equal){
				
				// Pick based on res amounts:
				// -> If one is doing really well, i.e. iron is piling up, don't upgrade that one!
				// Aka we simply pick the one which has the smallest # of resources right now.
				float minR=clayCount;
				minN="clay_pit";
				
				if(ironCount<minR){
					minN="iron_mine";
				}
				
				if(woodCount<minR){
					minN="timber_camp";
				}
				
			}
			
			// Build the min one:
			return minN;
			
		}
		
		public bool RequireAcademy{
			get{
				
				// Get the nearest one:
				Village nearest=NearestAcademy;
				
				if(nearest==null){
					return true;
				}
				
				// More than 8 tiles away, require one.
				return nearest.Distance(X,Y) > 8f;
				
			}
		}
		
		/// <summary>Nearest village to this one containing an acad.</summary>
		public Village NearestAcademy{
			get{
				
				// Nearest owned acad.
				Building acad=Buildings.Get("academy");
				
				if(GetQueuedLevel(acad)==1){
					return this;
				}
				
				Village nearest=null;
				double distance=double.MaxValue;
				
				foreach(KeyValuePair<int,Village> kvp in Account.Villages){
					
					if(kvp.Value.GetQueuedLevel(acad)==1){
						
						// It's got an academy. Nearest one so far?
						double newDist=kvp.Value.Distance(X,Y);
						
						if(newDist<distance){
							nearest=kvp.Value;
							distance=newDist;
						}
						
					}
					
				}
				
				return nearest;
				
			}
		}
		
		public bool CheckCoinCertified(){
			
			bool cert=(GetQueuedLevel("clay_pit")>=29 &&
						GetQueuedLevel("iron_mine")>=28 && 
						GetQueuedLevel("timber_camp")>=29 &&
						GetQueuedLevel("wall") == 20 &&
						GetQueuedLevel("barracks") >=21
						);
			
			return cert;
			
		}
		
		public void MintAll(){
			
			// How many for each res?
			int wood = TribeDonator.Floor(ResourceCount("wood"), 28000);
			int clay = TribeDonator.Floor(ResourceCount("clay"), 30000);
			int iron = TribeDonator.Floor(ResourceCount("iron"), 25000);
			
			int min=wood;
			
			if(clay<min){
				min=clay;
			}
			
			if(iron<min){
				min=iron;
			}
			
			if(min==0){
				
				// Can't mint any.
				return;
				
			}
			
			// Mint that many coins:
			MintCoins(min);
			
		}
		
		public void DonateExcess(){
			
			// Any resource above 90% should be donated so it drops to about 80% or so.
			float wood=RelativeResourceCount("wood");
			float clay=RelativeResourceCount("clay");
			float iron=RelativeResourceCount("iron");
			
			int woodD=0;
			int clayD=0;
			int ironD=0;
			
			// Donate down to 80%:
			int downTo=(int)((double)Storage * 0.8);
			
			if(wood>0.9f){
				
				// Donate excess wood:
				woodD=ResourceCount("wood") - downTo;
				
			}
			
			if(clay>0.9f){
				
				// Donate excess clay
				clayD=ResourceCount("clay") - downTo;
				
			}
			
			if(iron>0.9f){
				
				// Donate excess iron.
				ironD=ResourceCount("iron") - downTo;
				
			}
			
			// Donate!
			Donate(woodD,clayD,ironD);
			
		}
		
		public void DonateAll(){
			
			Donate(ResourceCount("wood"),ResourceCount("clay"),ResourceCount("iron"));
			
		}
		
		public void Donate(int wood,int clay,int iron){
			
			// IMPORTANT! Resources should only be donated in multiples of 100!
			// The client rounds to the nearest hundred, and not doing so might throw a red flag.
			// Besides, anything more is not counted towards the exp gained, either!
			wood = TribeDonator.Floor(wood, 100);
			clay = TribeDonator.Floor(clay, 100);
			iron = TribeDonator.Floor(iron, 100);
			
			if(wood==0 && clay==0 && iron==0){
				return;
			}
			
			Console.WriteLine("Donating "+wood+", "+clay+", "+iron+" from "+Name);
			
			Client.Send("TribeSkill/donate", "{\"village_id\":"+ID+",\"crowns\":0,\"resources\":{\"wood\":"+wood+",\"clay\":"+clay+",\"iron\":"+iron+"}}");
		
		}
		
		public bool IsInGroup(string name){
			
			if(Groups==null){
				return false;
			}
			
			for(int i=0;i<Groups.Length;i++){
				
				if(Groups[i].Name==name){
					return true;
				}
				
			}
			
			return false;
			
		}
		
		public bool IsInGroup(int id){
			
			if(Groups==null){
				return false;
			}
			
			for(int i=0;i<Groups.Length;i++){
				
				if(Groups[i].ID==id){
					return true;
				}
				
			}
			
			return false;
			
		}
		
		/// <summary>Assign a group by id to this village.</summary>
		public void AssignGroup(int id){
			
			// Assign now:
			Account.Client.Send("Group/link","{\"group_id\":"+id+",\"village_id\":"+ID+"}");
			
			// Note that Groups is updated when the server replies.
			
		}
		
		/// <summary>Assign a group by name to this village.</summary>
		public void AssignGroup(string name){
			
			// Get group from acc:
			GroupInfo gp=Account.GetGroup(name);
			
			// Assign now:
			AssignGroup(gp.ID);
			
		}
		
		/// <summary>Unassign a group by id from this village.</summary>
		public void UnassignGroup(int id){
			
			// Unassign now:
			Account.Client.Send("Group/unlink","{\"group_id\":"+id+",\"village_id\":"+ID+"}");
			
			// Note that Groups is updated when the server replies.
			
		}
		
		/// <summary>Unassign a group by name from this village.</summary>
		public void UnassignGroup(string name){
			
			// Get group from acc:
			GroupInfo gp=Account.GetGroup(name);
			
			// Unassign now:
			UnassignGroup(gp.ID);
			
		}
		
		/// <summary>Called by the server. Updates the group set.</summary>
		public void AddGroupNow(int id){
			
			GroupInfo gp=Account.GetGroup(id);
			
			if(gp==null){
				return;
			}
			
			if(Groups==null){
				Groups=new GroupInfo[1]{gp};
				return;
			}
			
			GroupInfo[] ns=new GroupInfo[Groups.Length+1];
			
			Array.Copy(Groups,0,ns,0,Groups.Length);
			
			ns[Groups.Length]=gp;
			
			Groups=ns;
			
		}
		
		/// <summary>Called by the server. Updates the group set.</summary>
		public void RemoveGroupNow(int id){
			
			if(Groups==null){
				return;
			}
			
			GroupInfo[] ns=new GroupInfo[Groups.Length-1];
			int outIndex=0;
			
			for(int i=0;i<Groups.Length;i++){
				
				if(Groups[i].ID!=id){
					
					ns[outIndex]=Groups[i];
					outIndex++;
					
				}
				
			}
			
			Groups=ns;
			
		}
		
		/// <summary>True if this is in a group called 'Coins'</summary>
		public bool IsCoins{
			get{
				return IsInGroup("Coins");
			}
		}
		
		/// <summary>True if this is in a group called 'Donor'</summary>
		public bool IsDonor{
			get{
				return IsInGroup("Donor");
			}
		}
		
		/// <summary>True if this is in a group called 'Resources'</summary>
		public bool IsResource{
			get{
				return IsInGroup("Resources");
			}
		}
		
		/// <summary>Assign this group to be offensive or defensive.</summary>
		public void AssignOffDef(bool offensive){
			
			if(offensive){
				
				AssignGroup("Offensive Village");
				
			}else{
				
				AssignGroup("Defensive Village");
				
			}
			
		}
		
		/// <summary>0 = Offensive, 1 = Defensive, 2 = neither (can't tell/ unassigned).</summary>
		public int DefensiveState{
			get{
				
				// Is this a defensive village?
				// -> Compare total off to total def (specifically just axes vs. spears/swords/arch)
				int axeCount=GetUnit("axe").Count;
				int spearCount=GetUnit("spear").Count;
				int swordCount=GetUnit("sword").Count;
				int archCount=GetUnit("archer").Count;
				int totalDef=(spearCount + swordCount + archCount);
				
				
				if(axeCount==0 && totalDef==0){
					
					// Quick check if there's a group called Defensive or Offensive:
					if(IsInGroup("Offensive Village") || IsInGroup("Attack") || IsInGroup("Offensive")){
						
						// Off:
						return 0;
						
					}
					
					if(IsInGroup("Defensive Village") || IsInGroup("Defend") || IsInGroup("Defensive")){
						
						// Def:
						return 1;
						
					}
					
					// It's neither! Assign off/def.
					return 2;
					
				}
				
				if( totalDef > axeCount ){
					// Def:
					return 1;
				}
				
				// Off:
				return 0;
				
			}
		}
		
		public void GetMerchantStatus(){
			
			Client.Send("Trading/getMerchantStatus","{\"village_id\":"+ID+"}");
			
		}
		
		public float RelativeResourceCount(string resource){
			
			return (float)( (double)GetResource(resource).CountNow / (double)Storage );
			
		}
		
		/// <summary>Gets the count of a particular resource available right now in this village.</summary>
		public int ResourceCount(string resource){
			
			return GetResource(resource).CountNow;
			
		}
		
		public bool SendResources(int wood,int clay,int iron,int targetVillage){
			
			// How many merchants are now gonna be busy?
			int merchants=ResourceNet.CountMerchants(wood + clay + iron);
			
			if(merchants==0){
				return true;
			}
			
			if(Merchants==null){
				Merchants=new MerchantInfo(this,0,merchants);
			}else{
				
				if(Merchants.Free<merchants){
					// Not enough merchants!
					return false;
				}
				
				// That many merch are now busy:
				Merchants.Free-=merchants;
				Merchants.Busy+=merchants;
				
			}
			
			// Send it off:
			Client.Send("Trading/sendResources","{\"start_village\":"+ID+",\"target_village\":"+targetVillage+",\"wood\":"+wood+",\"clay\":"+clay+",\"iron\":"+iron+"}");
			
			return true;
			
		}
		
		public void LoadMerchantStatus(JSObject data){
			
			int busy=int.Parse(data["busy"].ToString());
			int free=int.Parse(data["free"].ToString());
			
			if(Merchants==null){
				
				// Create the merchant info:
				Merchants=new MerchantInfo(this,busy,free);
				
			}else{
				
				Merchants.Busy=busy;
				Merchants.Free=free;
				
			}
			
		}
		
		public void RecruitNobles(int count){
			
			Client.Send("Academy/recruit","{\"village_id\":"+ID+",\"unit_type\":\"snob\",\"amount\":"+count+"}");
			
		}
		
		public void MintCoins(int count){
			
			Client.Send("Academy/mintCoins","{\"village_id\":"+ID+",\"amount\":"+count+"}");
			
		}
		
		public int TribeID{
			get{
				
				if(Owner==null){
					return 0;
				}
				
				return Owner.TribeID;
				
			}
		}
		
		public string Path{
			get{
				return Account.Path+"/"+Name+"_";
			}
		}
		
		public string ResourceText(){
			
			return Wood+"/"+Storage+" "+Clay+"/"+Storage+" "+Iron+"/"+Storage+" "+Food;
			
		}
		
		public string ToForumBB(){
			return "[village="+ID+"]"+Name+" ("+X+"|"+Y+")[/village]";
		}
		
		public override string ToString(){
			
			if(Name!=null){
				
				return Name+" ("+X+"|"+Y+")";
				
			}
			
			return "Village #"+ID;
			
		}
		
		public string ToDataString(){
			
			string owner;
			
			if(Owner==null){
				owner="0";
			}else{
				owner=Owner.ID.ToString();
			}
			
			return ID+","+owner+","+Points+","+X+","+Y+",'"+Name+"'";
			
		}
		
		/// <summary>Sets up the full info for this village.</summary>
		private void SetupFull(){
			
			if(Account==null || !Account.LoggedIn){
				// Umbra isn't logged in to the owner account.
				// That's fine - this village is basically someone elses.
				return;
			}
			
			// Setup queues:
			RecruitQueue=new CommandQueue();
			BuildingQueue=new CommandQueue();
			
			RecruitQueue.OnEvent+=delegate(string name,QueueEntry entry,TimedQueue queue){
				
				// e.g. OnRecruitComplete
				Run("onrecruit"+name,entry);
				
			};
			
			BuildingQueue.OnEvent+=delegate(string name,QueueEntry entry,TimedQueue queue){
				
				// e.g. OnBuildComplete
				Run("onbuild"+name,entry);
				
			};
			
			// Setup building info:
			foreach(KeyValuePair<string,Building> kvp in Buildings.All){
				
				BuildingInfo[kvp.Key]=new BuildingLevelInfo(this,kvp.Value);
				
			}
			
			// Resources:
			ResourceInfo["wood"]=new ResourceInfo("wood",this);
			ResourceInfo["clay"]=new ResourceInfo("clay",this);
			ResourceInfo["iron"]=new ResourceInfo("iron",this);
			ResourceInfo["food"]=new ResourceInfo("food",this);
			
			int free=(GetBuilding("market").Level);
			
			Merchants=new MerchantInfo(this,0,free);
			
		}
		
		public string BuildingText(){
			
			string result="";
			
			foreach(KeyValuePair<string,BuildingLevelInfo> kvp in BuildingInfo){
				
				result+=kvp.Key+"="+kvp.Value.Level+"\r\n";
				
			}
			
			return result;
			
		}
		
		public object Run(string name,params object[] extra){
			
			return Account.Run(name,extra);
			
		}
		
		public ArmyMovement LaunchAttack(int villageID,List<CombatUnits> units,List<CombatOfficers> officers){
			
			Village village=null;
			
			return LaunchAttack(village,units,officers,0,false);
			
		}
		
		public ArmyMovement LaunchAttack(Village targetVillage,List<CombatUnits> units,List<CombatOfficers> officers,uint timeAt,bool blockLate){
			
			return Launch(true,targetVillage,units,officers,timeAt,blockLate);
			
		}
		
		public ArmyMovement Launch(bool isAttack,Village targetVillage,List<CombatUnits> units,List<CombatOfficers> officers,uint timeAt,bool blockLate){
			
			ArmyMovement movement=new ArmyMovement(isAttack);
			movement.ToVillage=targetVillage;
			movement.FromVillage=this;
			movement.Units=units;
			movement.Officers=officers;
			
			// Any * units?
			foreach(CombatUnits unit in units){
				
				if(unit.Count==int.MaxValue){
					
					CombatUnits villageUnits=GetUnit(unit.UnitName);
					
					if(villageUnits!=null){
					
						unit.Count=(int)villageUnits.InTown;
					
					}else{
						
						unit.Count=0;
						
					}
					
				}
				
			}
			
			if(timeAt!=0){
				
				if(movement.LandAt(timeAt,blockLate)==-1){
					
					return null;
					
				}
				
			}else{
				
				// Send it now:
				movement.Submit();
				
			}
			
			return movement;
			
		}
		
		public UnitsSet GetAllUnits(){
			
			UnitsSet set=new UnitsSet();
			
			GetAllUnits(set);
			
			return set;
			
		}
		
		public void GetAllUnits(UnitsSet set){
		
			Dictionary<string,CombatUnits> units=UnitInfo;
			
			foreach(KeyValuePair<string,CombatUnits> kvc in units){
				
				set.Add(kvc.Key,kvc.Value.Count);
				
			}
			
		}
		
		public void UpdateResources(JSObject data){
			
			// Update the time:
			ResourcesLastTime=uint.Parse(data["res_last_update"].ToString());
			
			// Get storage count:
			Storage=int.Parse(data["storage"].ToString());
			
			// Get the resources:
			JSArray res=data["resources"] as JSArray;
			
			// Get resource counts:
			foreach(KeyValuePair<string,JSObject> kvp in res.Values){
				
				string resource=kvp.Key;
				int count=int.Parse(kvp.Value.ToString());
				
				GetResource(resource).Count=count;
				
			}
			
			// Resource rates are up next.
			res=data["production_rates"] as JSArray;
			
			// Get resource counts:
			foreach(KeyValuePair<string,JSObject> kvp in res.Values){
				
				string resource=kvp.Key;
				float ratePerHour=float.Parse(kvp.Value.ToString());
				
				GetResource(resource).SetRate(ratePerHour);
				
			}
			
			triggerEvent("onresources");
			
		}
		
		public void triggerEvent(string name){
			
			// Create an empty UIEvent for this element:
			UIEvent e=new UIEvent();
			e.Account=Account;
			e.Village=this;
			
			Account.triggerEvent(name,e);
			
		}
		
		public void LoadData(JSObject data){
			
			// General village info:
			JSObject villData=data["Village/village"];
			
			Name=villData["name"].ToString();
			
			// Coords:
			X=int.Parse(villData["x"].ToString());
			Y=int.Parse(villData["y"].ToString());
			
			// Building levels next!
			JSArray build=villData["buildings"] as JSArray;
			
			// Get levels:
			foreach(KeyValuePair<string,JSObject> kvp in build.Values){
				
				string buildingName=kvp.Key;
				int level=int.Parse(kvp.Value["level"].ToString());
				
				// Get the building info:
				BuildingLevelInfo building=GetBuilding(buildingName);
				
				// Apply level:
				building.Level=level;
				
				// Gonna load research stuff too (req to compute build duration).
				// That's just kvp.Value["researches"]:
				JSArray research=kvp.Value["researches"] as JSArray;
				
				building.Researches=new Dictionary<string,ResearchInfo>();
				
				if(research!=null && research.Values!=null){
				
					foreach(KeyValuePair<string,JSObject> rvp in research.Values){
						
						// Create it:
						ResearchInfo ri=new ResearchInfo(building,rvp.Key);
						
						// Unlocked or not:
						ri.Unlocked=(rvp.Value["unlocked"].ToString()=="true");
						
						// Add to set:
						building.Researches[rvp.Key]=ri;
						
					}
					
				}
				
			}
			
			// Resources - current counts and production rates:
			UpdateResources(villData);
			
			// Clear the queues:
			BuildingQueue.Clear();
			RecruitQueue.Clear();
			
			// Build queue info:
			JSObject buildQueueData=data["Building/queue"];
			
			UpdateBuildQueue(buildQueueData);
			
			// Load contents from queue files:
			QueueLoader.Load(this,Path+"building_queue.txt");
			QueueLoader.Load(this,Path+"unit_queue.txt");
			
			if(BuildQueueEndEvent==null){
				
				// Insta ready to schedule.
				BuildQueueReadyToSchedule();
				
			}
			
			// Unit info:
			JSObject unitData=data["Village/unitInfo"];
			
			UpdateUnits(unitData);
			
			if(RecruitQueueEndEvent==null){
				
				// Insta ready to schedule.
				RecruitQueueReadyToSchedule();
				
			}
			
		}
		
		public void GetUnitInfo(){
			
			Client.Send("Village/getUnitInfo","{\"village_id\":"+ID+"}");
			
		}
		
		public void UpdateUnits(JSObject data){
			
			// Get the units:
			JSArray res=data["available_units"] as JSArray;
			
			// Get unit counts:
			foreach(KeyValuePair<string,JSObject> kvp in res.Values){
				
				string unit=kvp.Key;
				int count=int.Parse(kvp.Value["total"].ToString());
				int inTown=int.Parse(kvp.Value["in_town"].ToString());
				
				CombatUnits unitInfo=GetUnit(unit);
				
				unitInfo.Count=count;
				unitInfo.InTown=inTown;
				
			}
			
			// Time to farm?
			Farming.SendNow(this);
			
			triggerEvent("onunits");
			
		}
		
		/// <summary>Total farming capacity, regardless of units being in town or not.</summary>
		public int FarmingCapacity{
			
			get{
				
				int total=0;
				
				foreach(KeyValuePair<string,CombatUnits> kvp in UnitInfo){
					
					total+=kvp.Value.FarmingCapacity;
					
				}
				
				return total;
				
			}
			
		}
		
		public int Wood{
			get{
				return GetResource("wood").CountNow;
			}
		}
		
		public int Clay{
			get{
				return GetResource("clay").CountNow;
			}
		}
		
		public int Iron{
			get{
				return GetResource("iron").CountNow;
			}
		}
		
		public int Food{
			get{
				return GetResource("food").CountNow;
			}
		}
		
		public CombatUnits GetUnit(string name){
			
			CombatUnits result;
			if(!UnitInfo.TryGetValue(name,out result)){
				
				result=new CombatUnits(this,name,0);
				UnitInfo[name]=result;
				
			}
			
			return result;
			
		}
		
		public ResourceInfo GetResource(string name){
			
			ResourceInfo result;
			ResourceInfo.TryGetValue(name,out result);
			return result;
			
		}
		
		public BuildingLevelInfo GetBuilding(string name){
			
			BuildingLevelInfo result;
			BuildingInfo.TryGetValue(name,out result);
			return result;
			
		}
		
		public void ResourcesChanged(){
			
		}
		
	}
	
}