using System;
using Wrench;


namespace Tribalwars{
	
	public class BuildQueueEntry : QueueEntry{
		
		public int Level;
		/// <summary>The level of a particular building in a particular village.</summary>
		public BuildingLevelInfo BuildingInstance;
		
		
		public Building Building{
			get{
				return BuildingInstance.Building;
			}
		}
		
		public BuildQueueEntry(BuildingLevelInfo building,int targetLevel,uint start,uint end){
			
			BuildingInstance=building;
			Level=targetLevel;
			InProgress=true;
			
			// Gets ticked when the build is actually done.
			
			Duration=(int)(end-start) * 1000;
			TimeCompletes=(long)end * 1000;
			
		}
		
		public override string ToString(){
			
			return "upgrade "+Building.RefName+" to "+Level+" (completes in "+((TimeCompletes-Time.NowMilliseconds)/1000)+"s)";
			
		}
		
		public BuildQueueEntry(BuildingLevelInfo building,int targetLevel,Village village){
			
			BuildingInstance=building;
			Level=targetLevel;
			
			// time in seconds:
			int seconds = BuildingInstance.TimeToLevel(Level);
			
			// Duration is in ms:
			Duration=seconds * 1000;
			
			// Get the current end of the BQ:
			long time = village.BuildQueueEnd;

			if(time == 0)
				time = Time.NowMilliseconds;

			TimeCompletes = time + Duration;
			
		}
		
		public LevelCost Cost{
			get{
				
				// Get requirements for this level:
				return Building.GetCost(Level,BuildingInstance.Village);
				
			}
		}
		
		/// <summary>Millisecond time when this entry is actually possible. MaxValue if it isn't at the moment 
		/// (e.g. because the warehouse isn't big enough, or not enough people).</summary>
		public long TimeWhenPossible(out Reason notPossibleReason){
			
			// Get the village:
			Village village=BuildingInstance.Village;
			
			// Get requirements for this level:
			LevelCost cost=Building.GetCost(Level,village);
			
			// How long until all resources are satisfied? Note that it's in seconds.
			int availableIn=cost.AvailableIn(village,out notPossibleReason);
			
			if(availableIn==int.MaxValue){
				
				// Not possible!
				return long.MaxValue;
				
			}
			
			return Time.NowMilliseconds;
			
		}
		
		public override string ToCommand(){
			
			return "upgrade "+Building.RefName+" to "+Level;
			
		}
		
		public void OfficiallyQueue(){
		
			if(InProgress){
				return;
			}
			
			InProgress=true;
			
			Console.WriteLine("Upgrading "+Building.RefName+"..");
			BuildingInstance.UpgradeNow();
			
		}
		
	}
	
}