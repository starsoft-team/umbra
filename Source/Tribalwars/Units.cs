using System;
using Wrench;
using System.IO;
using System.Collections;
using System.Collections.Generic;


namespace Tribalwars{

	public static class Units{
		
		public static Dictionary<string,UnitData> All;
		public static List<UnitData> Defensive;
		public static List<UnitData> Offensive;
		public static List<UnitData> OffensiveNoNoble;
		
		
		public static UnitData Get(string name){
			
			if(All==null){
				Setup();
			}
			
			UnitData result;
			All.TryGetValue(name,out result);
			
			return result;
			
		}
		
		public static void Setup(){
			
			Officers.Setup();
			
			All=new Dictionary<string,UnitData>();
			
			JSArray units=JSON.Parse(File.ReadAllText("tw-unit-info.txt")) as JSArray;
			
			foreach(KeyValuePair<string,JSObject> kvp in units.Values){
				
				UnitData unit=new UnitData(kvp.Key,kvp.Value);
				
				All[kvp.Key]=unit;
				
			}
			
			Offensive=new List<UnitData>();
			OffensiveNoNoble=new List<UnitData>();
			Defensive=new List<UnitData>();
			
			foreach(KeyValuePair<string,UnitData> kvp in All){
				
				if(kvp.Value.DefensiveUnit){
					
					Defensive.Add(kvp.Value);
					
				}else{
					
					Offensive.Add(kvp.Value);
					
					if(kvp.Key!="snob"){
						OffensiveNoNoble.Add(kvp.Value);
					}
					
				}
				
			}
			
		}
		
	}

}