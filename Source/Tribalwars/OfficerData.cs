using System;
using System.Collections;
using System.Collections.Generic;
using Wrench;


namespace Tribalwars{

	public class OfficerData{
		
		/// <summary>The internal name e.g. "bastard" (deceiver).</summary>
		public string RefName;
		public JSObject RawData;
		
		
		public OfficerData(string refName,JSObject data){
			
			RefName=refName;
			
			RawData=data;
			
		}
		
		public JSObject this[string property]{
			get{
				return RawData[property];
			}
		}
		
		public string GetString(string property){
			JSObject val=RawData[property];
			
			if(val==null){
				return null;
			}
			
			return val.ToString();
		}
		
		public int GetInt(string property){
			
			string value=GetString(property);
			
			if(value==null){
				return 0;
			}
			
			return int.Parse(value);
		}
		
	}

}