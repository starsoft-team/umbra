using System;
using Wrench;
using System.IO;
using System.Collections;
using System.Collections.Generic;


namespace Tribalwars{

	public static class Buildings{
		
		public static Dictionary<string,Building> All;
		
		public static Building Get(string name){
			
			if(All==null){
				Setup();
			}
			
			Building result;
			All.TryGetValue(name,out result);
			
			return result;
			
		}
		
		public static void Setup(){
			
			All=new Dictionary<string,Building>();
			
			JSArray buildings=JSON.Parse(File.ReadAllText("tw-building-info.txt")) as JSArray;
			
			foreach(KeyValuePair<string,JSObject> kvp in buildings.Values){
				
				Building building=new Building(kvp.Key,kvp.Value);
				
				All[kvp.Key]=building;
				
			}
			
			// Research next:
			Research.Setup();
			
		}
		
	}

}