using System;
using System.Collections;
using System.Collections.Generic;


namespace Tribalwars{

	public static class Farming{
		
		/// <summary>The max "farmable village" search distance around a given village.
		/// Too big and a village might end up with a ton of villages it'll simply never use because their too far away.</summary>
		public const int MaxSearchDistance=30;
		/// <summary>Average group capacity.</summary>
		public const int AverageCapacity=4400;
		
		// Let's recompute the farmable villages once every 12 hours
		public const int Minutes = 720;
		public const int Seconds = Minutes * 60;
		
		public static bool Active;
		private static System.Timers.Timer InternalTimer;
		
		public static bool Started
		{
			get { return InternalTimer != null; }
		}
		
		public static void Start()
		{
			if(Started)
				return;
			
			InternalTimer = new System.Timers.Timer();
			InternalTimer.Elapsed += Elapsed;
			InternalTimer.Interval = Seconds * 1000;
			InternalTimer.Start();
			
		}
		
		/// <summary>The method called when the system timer has waited for the specified interval.</summary>
		private static void Elapsed(object sender, System.Timers.ElapsedEventArgs e)
		{
			RecomputePriorities();
		}
		
		/// <summary>Recomputes which villages on the map are best for each village to farm.</summary>
		public static void RecomputePriorities(){
			
			List<Village> farmingVillages=new List<Village>();
			
			int totalCapacity=0;
			
			foreach(KeyValuePair<int,Account> kvp in Accounts.All){
				
				foreach(KeyValuePair<int,Village> vp in kvp.Value.Villages){
					
					// Get the village:
					Village village=vp.Value;
					
					// How many farming groups does this village have?
					int capacity=village.FarmingCapacity;
					
					if(capacity>=200){
						
						// Add it:
						farmingVillages.Add(village);
						
						totalCapacity+=capacity;
						
					}
					
				}
				
			}
			
			foreach(Village village in farmingVillages){
				
				// Total capacity is..
				int capacity=village.FarmingCapacity;
				
				// Need at least this many villages:
				int groups=FarmingGroups(capacity);
				
				// float proportion=(float)capacity / (float)totalCapacity;
				
			}
			
		}
		
		/// <summary>The number of farming groups for the given resource capacity.</summary>
		public static int FarmingGroups(int capacity){
			
			if(capacity<200){
				return 0;
			}
			
			// Round to the nearest 4.4k:
			int groups=(int)Math.Round((float)capacity / (float)AverageCapacity);
			
			if(groups==0){
				groups=1;
			}
			
			return groups;
			
		}
		
		public static void SendNow(Village village){
			
			// Note that there might not be enough units available.
			
			// Has the village got any groups in town at the moment?
			
			
		}
		
	}

}