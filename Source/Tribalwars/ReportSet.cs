using System;
using System.Collections;
using System.Collections.Generic;
using Wrench;


namespace Tribalwars{
	
	public class ReportSet{
		
		public static void Get(Account account){
			
			account.Client.Send(
				"Report/getListReverse",
				"{\"offset\":0,\"count\":25,\"query\":\"\",\"types\":[\"attack\",\"defense\",\"support\",\"scouting\"],\"filters\":[]}"
			);
			
		}
		
		public static ReportSet Load(JSObject data,Account account){
			
			// Create results set:
			ReportSet set=new ReportSet(account);
			
			// Total # of reports:
			set.Total=int.Parse(data["total"].ToString());
			
			// Apply offset:
			set.Offset=int.Parse(data["offset"].ToString());
			
			// Reports in the set:
			List<Report> results=new List<Report>();
			
			// Report list:
			JSArray reports=data["reports"] as JSArray;
			
			foreach(KeyValuePair<string,JSObject> kvp in reports.Values){
				
				results.Add(new Report(kvp.Value,account));
				
			}
			
			// Apply reports:
			set.Reports=results;
			
			return set;
			
		}
		
		public int Total;
		public int Offset;
		public Account Account;
		public List<Report> Reports;
		
		
		public ReportSet(Account account){
			Account=account;
		}
		
		public Report GetReport(int id){
			
			foreach(Report rep in Reports){
				
				if(rep.ID==id){
					return rep;
				}
				
			}
			
			return null;
			
		}
		
		public Report this[int index]{
			get{
				return Reports[index];
			}
		}
		
	}
	
}