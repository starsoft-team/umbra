using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;


namespace Tribalwars{
	
	public class UrgencySet{
		
		/// <summary>Below this urgency, stuff doesn't matter too much - we'll just pick the quickest things.</summary>
		public static double MinUrgency=0.3;
		
		/// <summary>Sorted by urgency.</summary>
		public SortedDictionary<double,List<UrgencyEntry>> Urgencies=new SortedDictionary<double,List<UrgencyEntry>>();
		/// <summary>Sorted by speed.</summary>
		public SortedDictionary<long,List<UrgencyEntry>> Speeds=new SortedDictionary<long,List<UrgencyEntry>>();
		
		
		public void AddBuilding(Village vill,string name){
			
		}
		
		/// <summary>Simply the most urgent one.</summary>
		public UrgencyEntry MostUrgent{
			get{
				
				foreach(KeyValuePair<double,List<UrgencyEntry>> kvp in Urgencies){
					
					return kvp.Value[0];
					
				}
				
				return null;
				
			}
		}
		
		/// <summary>The fastest building one.</summary>
		public UrgencyEntry FastestBuilding{
			get{
				
				foreach(KeyValuePair<long,List<UrgencyEntry>> kvp in Speeds){
					
					return kvp.Value[0];
					
				}
				
				return null;
				
			}
		}
		
		/// <summary>The name e.g. "clay_pit" of the best choice.</summary>
		public string BestChoiceName{
			get{
				
				UrgencyEntry bestChoice=BestChoice;
				
				if(bestChoice==null){
					return null;
				}
				
				return bestChoice.Name;
				
			}
		}
		
		/// <summary>If there's no particular rush (i.e. urgencies are low) then the best choice is the one which completes quickest.</summary>
		public UrgencyEntry BestChoice{
			get{
				
				// Get the most urgent one:
				UrgencyEntry mostUrgent=MostUrgent;
				
				// If it's urgency is low (<MinUrgency), pick whatever is fastest as well as urgent.
				if(mostUrgent.Urgency < MinUrgency){
					
					// Compute the ranks of all entries:
					ComputeRanks();
					
					// Rank sort:
					SortedDictionary<int,List<UrgencyEntry>> ranks=SortByRank();
					
					// Select the highest ranking entry:
					foreach(KeyValuePair<int,List<UrgencyEntry>> kvp in ranks){
						
						// First entry in highest ranking set:
						return kvp.Value[0];
						
					}
					
				}
				
				return mostUrgent;
				
			}
		}
		
		/// <summary>Sort by overall rank.</summary>
		public SortedDictionary<int,List<UrgencyEntry>> SortByRank(){
			
			// Sort by overall rank (sRank + uRank)
			
			SortedDictionary<int,List<UrgencyEntry>> r=new SortedDictionary<int,List<UrgencyEntry>>();
			
			foreach(KeyValuePair<double,List<UrgencyEntry>> kvp in Urgencies){
				
				foreach(UrgencyEntry e in kvp.Value){
					
					List<UrgencyEntry> set;
					if(!r.TryGetValue(e.Rank,out set)){
						
						set=new List<UrgencyEntry>();
						r[e.Rank]=set;
						
					}
					
					set.Add(e);
					
				}
				
			}
			
			return r;
			
		}
		
		public void ComputeRanks(){
			
			// Urgency rank first:
			int uRank=0;
			
			foreach(KeyValuePair<double,List<UrgencyEntry>> kvp in Urgencies){
				
				foreach(UrgencyEntry e in kvp.Value){
					
					e.Rank=uRank;
					uRank++;
					
				}
				
			}
			
			// Speed rank:
			int sRank=0;
			
			foreach(KeyValuePair<long,List<UrgencyEntry>> kvp in Speeds){
				
				foreach(UrgencyEntry e in kvp.Value){
					
					e.Rank+=sRank;
					sRank++;
					
				}
				
			}
			
		}
		
		public UrgencyEntry Add(string value,long dur,double urgency){
			
			UrgencyEntry ent=new UrgencyEntry(value,dur,urgency);
			
			// Reverse urgency (so it sorts properly):
			double flipUrg=1-urgency;
			
			List<UrgencyEntry> set;
			if(!Urgencies.TryGetValue(flipUrg,out set)){
				
				set=new List<UrgencyEntry>();
				Urgencies[flipUrg]=set;
				
			}
			
			// Add it in:
			set.Add(ent);
			
			// Add in speed next.
			
			if(!Speeds.TryGetValue(dur,out set)){
				
				set=new List<UrgencyEntry>();
				Speeds[dur]=set;
				
			}
			
			// Add it in:
			set.Add(ent);
			
			return ent;
		}
		
	}
	
}