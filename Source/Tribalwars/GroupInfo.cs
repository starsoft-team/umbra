using System;
using System.Collections;
using System.Collections.Generic;
using Wrench;

namespace Tribalwars{

	public class GroupInfo{
		
		public int ID;
		public int Icon;
		public string Name;
		public Account Owner;
		
		
		public GroupInfo(Account acc,JSObject data){
			
			Owner=acc;
			Name=data["name"].ToString();
			ID=int.Parse(data["id"].ToString());
			Icon=int.Parse(data["icon"].ToString());
			
		}
		
	}
	
}