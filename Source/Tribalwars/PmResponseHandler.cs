using System;

namespace Tribalwars{

	public class PmResponseHandler : ResponseHandler{
		
		public int FromID;
		public int MessageID;
		public Account ReceiverAccount;
		
		
		public PmResponseHandler(int message,int fromID,Account receivedBy){
			
			FromID=fromID;
			MessageID=message;
			ReceiverAccount=receivedBy;
			
		}
		
		public override void Send(string message){
			
			ReceiverAccount.ReplyTo(MessageID,message);
			
		}
		
	}
	
}