using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;


namespace Tribalwars{
	
	/// <summary>
	/// Results of a scout report. Can be loaded from a file -
	/// each line is e.g. "building=level" or "resource=count" or "resource:count" or "building level" etc.
	/// see scout_example.txt for an example.
	/// </summary>
	
	public class ScoutReport : ReportContent{
		
		public uint TimeAt;
		public int Wood;
		public int Clay;
		public int Iron;
		public int Points; // From the actual village (on the map).
		public UnitsSet Units=new UnitsSet();
		public BuildingLevels Buildings=new BuildingLevels();
		
		
		public ScoutReport(){}
		
		public ScoutReport(string filePath){
			
			string[] lines=File.ReadAllLines(filePath);
			
			for(int i=0;i<lines.Length;i++){
				
				string cmd=lines[i].Trim().ToLower();
				
				cmd=cmd.Replace(",","=").Replace(" ","=").Replace("\t","=");
				
				string[] pieces=cmd.Split('=');
				
				if(pieces.Length==1){
					continue;
				}
				
				string item=pieces[0];
				int level=int.Parse(pieces[1]);
				
				switch(item){
					
					case "wood":
						
						Wood=level;
						
					break;
					case "clay":
						
						Clay=level;
						
					break;
					case "iron":
						
						Iron=level;
						
					break;
					case "time":
						
						TimeAt=(uint)level;
						
					break;
					case "points":
						
						Points=level;
						
					break;
					default:
						
						Buildings.Add(item,level);
						
					break;
					
				}
				
			}
			
		}
		
		public override string Analyse(){
			
			// Is it camoflauged?
			if(Buildings.Count==0){
				return "Can't do anything with units reports at the moment.";
			}
			
			return "The buildings in this report add up to [B]"+ApparentPoints+"[/B] points.";
			
		}
		
		public void SaveToFile(string path){
			
			// Get it as some text:
			string text=ToString();
			
			// Write it out:
			File.WriteAllText(path,text);
			
		}
		
		public override string ToString(){
			
			// Buildings: name=level
			string result=Buildings.ToString();
			
			// points, resources etc:
			result+="points="+Points+"\r\n";
			result+="time="+TimeAt+"\r\n";
			result+="wood="+Wood+"\r\n";
			result+="iron="+Iron+"\r\n";
			result+="clay="+Clay;
			
			return result;
			
		}
		
		/// <summary>The apparent points from the buildings.</summary>
		public int ApparentPoints{
			get{
				return Buildings.TotalPoints;
			}
		}
		
		/// <summary>The apparent points from the buildings.</summary>
		public int Population{
			get{
				return Buildings.Population;
			}
		}
		
		public bool IsCamoflauged{
			get{
				
				return Buildings.Camoflauged(Points);
				
			}
		}
		
	}
	
}