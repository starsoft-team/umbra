using System;


namespace Tribalwars{
	
	public struct BattleSums{
		
		private readonly int[] Sums;
		
		
		public BattleSums(int inf,int cav,int arch){
			
			Sums=new int[3];
			Sums[0]=inf;
			Sums[1]=cav;
			Sums[2]=arch;
		}
		
		public int Total{
			get{
				return Sums[0]+Sums[1]+Sums[2];
			}
		}
		
		public int Infantry{
			get{
				return Sums[0];
			}
			set{
				Sums[0]=value;
			}
		}
		
		public int Cavalry{
			get{
				return Sums[1];
			}
			set{
				Sums[1]=value;
			}
		}
		
		public int Archers{
			get{
				return Sums[2];
			}
			set{
				Sums[2]=value;
			}
		}
		
		public int this[int index]{
			get{
				return Sums[index];
			}
			set{
				Sums[index]=value;
			}
		}
		
	}
	
}