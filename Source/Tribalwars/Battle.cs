using System;
using System.Collections;
using System.Collections.Generic;


namespace Tribalwars{
	
	/// <summary>
	/// This is a port of http://tw2calc.com/se. Thanks to them!
	/// It's been ported so it can potentially be used to compute
	/// the most optimal offensive army to take out a given defensive one.
	/// It uses UnitsSets, so the defensive army could come directly from a report.
	/// </summary>
	
	public class Battle{
		
		/// <summary>Ram attack value (comes from UnitData).</summary>
		public static int RamAttack;
		public static string[][] AttackTypeToUnits=new string[][]{
			
			// Inf
			new string[]{
				"spear", "sword", "axe", "ram", "catapult", "snob", "trebuchet", "doppelsoldner"
			},
			
			// Cav
			new string[]{
				"light_cavalry", "heavy_cavalry", "knight"
			},
			
			// Arch
			new string[]{
				"archer", "mounted_archer"
			}
			
		};
		
		public UnitsSet Attacker;
		public UnitsSet Defender;
		public int Wall;
		public bool NightBonus;
		public int WallAfter;
		public double Moral=1; // 0-1
		public double FullLuck=1; // 1+(LuckValue)
		public bool AttackerChurch=true;
		public bool DefenderChurch=true;
		public Dictionary<string,int> PaladinItems;
		
		public double Luck{
			get{
				return (FullLuck-1)*100;
			}
			set{
				FullLuck=(value/100)+1;
			}
		}
		
		public bool HasItem(string name){
			
			if(PaladinItems==null){
				return false;
			}
			
			return PaladinItems.ContainsKey(name);
			
		}
		
		public int ResultingWall(int rams,double churchBonus,bool morgenstern) {
			
			double palyItem = (morgenstern) ? 2 : 1;
			
			int wall=Wall;
			
			return (int)Math.Max(
				wall - Math.Round((rams * churchBonus * palyItem) / (4 * Math.Pow(1.09, wall))),
				Math.Round(wall / (2 * palyItem))
			);
		}
		
		public int WallAfterFight(double churchBonus,bool morgenstern) {
			
			int resultingWall = 0;
			int rammen=Attacker.Count("ram");
			int wall=Wall;
			
			double palyItem = (morgenstern) ? 2 : 1;
			
			if (rammen == 0 || wall == 0){
				return wall;
			}
			
			int defTotal=Defender.Total;
			
			double loseRateDefender;
			
			if(defTotal==0){
				
				loseRateDefender=1;
				
			}else{
			
				loseRateDefender=Defender.Losses / defTotal;
			
			}
			
			if(loseRateDefender == 1 ) {
				// Attacker wins
				
				double loseRateAttacker = Attacker.Losses / Attacker.Total;
				
				double maxDamage = (rammen * RamAttack * churchBonus * palyItem) / (4 * Math.Pow(1.09, wall));
				
				resultingWall =  wall - (int)Math.Round(maxDamage - 0.5 * maxDamage * loseRateAttacker);
				
			} else {
				
				//defender wins
				
				resultingWall =  wall - (int)Math.Round(rammen * RamAttack * churchBonus * palyItem * loseRateDefender / (8 * Math.Pow(1.09, wall)));
				
			}
			
			return (int)Math.Max(0, resultingWall);
			
		}
		
		public void Simulate(){
			
			// Update the defender units:
			Defender.RawTotal=Defender.Total;
			Attacker.RawTotal=Attacker.Total;
			
			Defender.SetInTown();
			Defender.ClearLosses();
			
			// And the attacker ones:
			Attacker.ClearLosses();
			
			double churchBonus=(AttackerChurch ? 1 : 0.5) * (DefenderChurch ? 1 : 2);
			
			double nightbonus;
			
			if(NightBonus){
				nightbonus=2;
			}else{
				nightbonus=1;
			}
			
			bool hasMorg=HasItem("morgenstern");
			
			int resultingWall = ResultingWall(Attacker.Count("ram"), churchBonus, hasMorg);
			
			double wallBonus = 1 + resultingWall * 0.05;
			double wallDefense;
			
			if(resultingWall == 0){
				
				wallDefense = 0;
				
			}else{
				
				wallDefense = Math.Round(Math.Pow(1.25, resultingWall) * 20);
				
			}
			
			double moralLuck=Moral * FullLuck * churchBonus;
			double wallNight=wallBonus * nightbonus;
			
			int loopLimiter=0;
			
			do{
				
				BattleSums attackStrength=Attacker.AttackSums();
				
				BattleSums defStrength=Defender.DefenseSums();
				
				BattleSums attackFood=Attacker.FoodSums();
				
				int attackSum = attackStrength.Total;
				int attackFoodSum = attackFood.Total;
				
				if(attackFoodSum==0){
					return;
				}
				
				// Apply in town values (effectively copies the data which is what we really want)
				Defender.SetInTown();
				
				// For each value in the sums..
				for(int i=0;i<3;i++){
					
					if(attackStrength[i] == 0){
						continue;
					}
					
					double ratio = (double)attackFood[i] / (double)attackFoodSum;
					
					if(ratio==0){
						continue;
					}
					
					double defense = (double)defStrength[i] * ratio * wallNight + wallDefense * ratio;
					
					double a = (double)attackStrength[i] * moralLuck / defense;
					
					if(a < 1) {
						
						// This is a wipeout (defender win)
						
						double c = Math.Sqrt(a) * a;
						
						foreach(KeyValuePair<string,CombatUnits> kvp in Defender.Units) {
							
							CombatUnits unit=kvp.Value;
							
							double losses=unit.InTown * c * ratio;
							
							unit.RawLosses+=losses;
							Defender.RawTotal-=losses;
							
						}
						
						// For each unit that is of this type..
						string[] set=AttackTypeToUnits[i];
						
						for(int u=0;u<set.Length;u++){
							
							string unitName=set[u];
							
							CombatUnits units=Attacker.GetUnit(unitName);
							
							if(units!=null){
								
								// Loosing the rest:
								double losses=units.Count-units.RawLosses;
								
								units.RawLosses+=losses;
								Attacker.RawTotal-=losses;
								
							}
							
						}
						
					} else {
						
						double c = Math.Sqrt(1/a) / a;
						
						foreach(KeyValuePair<string,CombatUnits> kvp in Defender.Units) {
							
							CombatUnits unit=kvp.Value;
							
							double losses=unit.InTown * ratio;
							unit.RawLosses += losses;
							Defender.RawTotal-=losses;
						
						}
						
						// For each unit that is of this type..
						string[] set=AttackTypeToUnits[i];
						
						for(int u=0;u<set.Length;u++){
							
							string unitName=set[u];
							
							CombatUnits units=Attacker.GetUnit(unitName);
							
							if(units!=null){
								
								double losses=c * (units.Count-units.RawLosses);
								
								units.RawLosses += losses;
								Attacker.RawTotal-=losses;
								
							}
							
						}
						
					}
					
				}
				
				loopLimiter++;
				
			}while(Attacker.RawTotal >= 1 && Defender.RawTotal >= 1 && loopLimiter<30);
			
			WallAfter=WallAfterFight(churchBonus,hasMorg);
			
		}
		
	}
	
}