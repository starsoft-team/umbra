using System;
using System.Collections;
using System.Collections.Generic;


namespace Tribalwars{
	
	public delegate void OnMapEvent(MapSection sec);
	
	public class MapSection{
		
		public static Dictionary<uint,MapSection> All;
		
		
		/// <summary>Get the suitable map sections that cover "range" village tiles around the given x/y coords.</summary>
		public static List<MapSection> GetInRange(int x,int y,int range){
			
			// Get range:
			int minX=x-range;
			int maxX=x+range;
			
			int minY=y-range;
			int maxY=y+range;
			
			// Clip:
			if(minX<0){
				minX=0;
			}
			
			if(minY<0){
				minY=0;
			}
			
			// Get in terms of map coords:
			int mapMinX=minX/25;
			int mapMinY=minY/25;
			
			int mapMaxX=maxX/25;
			int mapMaxY=maxY/25;
			
			// Create set:
			List<MapSection> sections=new List<MapSection>();
			
			// Loop that many:
			for(int mapX=mapMinX;mapX<=mapMaxX;mapX++){
				
				for(int mapY=mapMinY;mapY<=mapMaxY;mapY++){
					
					// Get it and add to results:
					sections.Add( Get(mapX,mapY) );
					
				}
				
			}
			
			return sections;
			
		}
		
		public static MapSection GetGameCoords(int x,int y){
			
			// Round to nearest 25 - just a basic /25:
			
			int mapX=x/25;
			int mapY=y/25;
			
			return Get(mapX,mapY);
			
		}
		
		public static MapSection Get(int mapX,int mapY){
			
			uint code=(uint)(mapX<<16) | (uint)mapY;
			
			if(All==null){
				
				All=new Dictionary<uint,MapSection>();
				
			}
			
			MapSection result;
			if(!All.TryGetValue(code,out result)){
				
				result=new MapSection(mapX,mapY);
				All[code]=result;
				
			}
			
			return result;
			
		}
		
		public int X;
		public int Y;
		/// <summary>True if a load is in progress.</summary>
		private bool Loading;
		/// <summary>The set of villages in this section.</summary>
		public List<Village> Villages;
		/// <summary>Called when the section loads fully.</summary>
		public event OnMapEvent OnLoaded;
		
		
		public MapSection(int x,int y){
			
			X=x;
			Y=y;
			
		}
		
		public void RequireLoad(){
			Loading=false;
			Villages=null;
			OnLoaded=null;
		}
		
		public void Add(Village village){
			
			if(Villages==null){
				
				Villages=new List<Village>();
				
			}
			
			Villages.Add(village);
			
		}
		
		/// <summary>True if this report is fully loaded and 'Villages' is available.</summary>
		public bool FullyLoaded{
			get{
				return (Villages!=null);
			}
		}
		
		public void Loaded(){
			
			if(Villages==null){
				
				Villages=new List<Village>();
				
			}
			
			OnLoaded(this);
			
		}
		
		public void GetContent(Account usingAccount,OnMapEvent onLoaded){
			
			if(FullyLoaded){
				
				onLoaded(this);
				return;
				
			}
			
			OnLoaded+=onLoaded;
			
			if(!Loading){
				
				Loading=true;
				
				usingAccount.Client.Send("Map/getVillagesByArea","{\"x\":"+(X * 25)+",\"y\":"+(Y*25)+",\"width\":25,\"height\":25}");
				
			}
			
		}
		
	}
	
}