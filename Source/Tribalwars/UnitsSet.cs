using System;
using System.Collections;
using System.Collections.Generic;
using Wrench;


namespace Tribalwars{
	
	/// <summary>
	/// Used to represent a set of units, e.g. from a scout report.
	/// </summary>
	
	public class UnitsSet{
		
		public static List<CombatUnits> Parse(string units,out List<CombatOfficers> officers){
			
			// Get rid of the and at the end, if they used one:
			units=units.Replace(" and ",",");
			
			// Output set:
			List<CombatUnits> createdUnits=new List<CombatUnits>();
			officers=null;
			
			string[] unitPieces=units.Split(',');
			
			for(int i=0;i<unitPieces.Length;i++){
				
				// x unitName, unitName=x, unitName:x etc. All ok.
				
				// Split the parts:
				string[] unitAndCount=unitPieces[i].Trim().Replace(':',' ').Replace('=',' ').Split(' ');
				
				int count;
				string unitName;
				
				if(unitAndCount.Length==1){
					
					count=1;
					unitName=unitAndCount[0].Trim();
					
				}else{
					
					// Get the numeric one:
					string part1=unitAndCount[0].Trim();
					string part2=unitAndCount[1].Trim();
					
					if(part1=="*"){
						
						unitName=part2;
						count=int.MaxValue;
						
					}else if(part2=="*"){
						
						unitName=part1;
						count=int.MaxValue;
						
					}else if(int.TryParse(part1,out count)){
						
						unitName=part2;
						
					}else{
						
						unitName=part1;
						count=int.Parse(part2);
						
					}
					
				}
				
				unitName=unitName.ToLower();
				
				if(Officers.Get(unitName)!=null){
					
					// It's an officer!
					
					// Create the set:
					CombatOfficers set=new CombatOfficers(unitName,count);
					
					if(officers==null){
						
						officers=new List<CombatOfficers>();
						
					}
					
					// Add it to result:
					officers.Add(set);
					
				}else{
				
					// Create the set:
					CombatUnits set=new CombatUnits(unitName,count);
					
					// Add it to result:
					createdUnits.Add(set);
					
				}
				
			}
			
			return createdUnits;
			
		}
		
		/// <summary>A cached version of Total used by the battle sim.</summary>
		public double RawTotal;
		/// <summary>Optional vill.</summary>
		public Village Village;
		/// <summary>The set of units in this set.</summary>
		public Dictionary<string,CombatUnits> Units=new Dictionary<string,CombatUnits>();
		
		
		public UnitsSet(){}
		
		public UnitsSet(Village village){
			Village=village;
		}
		
		public UnitsSet(JSObject data){
			Load(data);
		}
		
		public int Losses{
			get{
				
				int total=0;
				
				foreach(KeyValuePair<string,CombatUnits> kvp in Units){
					
					total+=kvp.Value.Losses;
					
				}
				
				return total;
				
			}
		}
		
		public int Total{
			get{
				
				int total=0;
				
				foreach(KeyValuePair<string,CombatUnits> kvp in Units){
					
					total+=kvp.Value.Count;
					
				}
				
				return total;
				
			}
		}
		
		public int TotalInTown{
			get{
				
				int total=0;
				
				foreach(KeyValuePair<string,CombatUnits> kvp in Units){
					
					total+=(int)kvp.Value.InTown;
					
				}
				
				return total;
				
			}
		}
		
		public void ClearLosses(){
			
			foreach(KeyValuePair<string,CombatUnits> kvp in Units){
				
				kvp.Value.RawLosses=0;
				
			}
			
		}
		
		public void SetInTown(){
			
			foreach(KeyValuePair<string,CombatUnits> kvp in Units){
				
				kvp.Value.InTown=kvp.Value.Count - kvp.Value.RawLosses;
				
			}
			
		}
		
		public CombatUnits GetUnit(string unit){
			
			CombatUnits units;
			Units.TryGetValue(unit,out units);
			
			return units;
			
		}
		
		public int Count(string unit){
			
			CombatUnits units;
			Units.TryGetValue(unit,out units);
			
			if(units==null){
				return 0;
			}
			
			return units.Count;
			
		}
		
		public BattleSums DefenseSums(){
			
			int inf=0;
			int cav=0;
			int arc=0;
			
			foreach(KeyValuePair<string,CombatUnits> kvp in Units){
				
				CombatUnits units=kvp.Value;
				int count=(units.Count - units.Losses);
				UnitData unitData=units.UnitData;
				
				inf+=unitData.DefenseInfantry * count;
				cav+=unitData.DefenseCavalry * count;
				arc+=unitData.DefenseArchers * count;
				
			}
			
			return new BattleSums(inf,cav,arc);
			
		}
		
		public int PopulationSum{
			get{
				
				int total=0;
				
				foreach(KeyValuePair<string,CombatUnits> kvp in Units){
					
					CombatUnits units=kvp.Value;
					total+=units.UnitData.Food * units.Count;
					
				}
				
				return total;
				
			}
		}
		
		public BattleSums FoodSums(){
			
			int inf=0;
			int cav=0;
			int arc=0;
			
			foreach(KeyValuePair<string,CombatUnits> kvp in Units){
				
				CombatUnits units=kvp.Value;
				UnitData unitData=units.UnitData;
				int food=unitData.Food * (units.Count - units.Losses);
				
				if(unitData.IsArcher){
					
					arc+=food;
					
				}else if(unitData.IsCavalry){
					
					cav+=food;
					
				}else{
				
					inf+=food;
				
				}
				
			}
			
			return new BattleSums(inf,cav,arc);
			
		}
		
		public BattleSums AttackSums(){
			
			int inf=0;
			int cav=0;
			int arc=0;
			
			foreach(KeyValuePair<string,CombatUnits> kvp in Units){
				
				CombatUnits units=kvp.Value;
				UnitData unitData=units.UnitData;
				int attack=unitData.Attack * (units.Count - units.Losses);
				
				if(unitData.IsArcher){
					
					arc+=attack;
					
				}else if(unitData.IsCavalry){
					
					cav+=attack;
					
				}else{
				
					inf+=attack;
				
				}
				
			}
			
			return new BattleSums(inf,cav,arc);
			
		}
		
		public string ToLossesBB(){
			
			string result="";
			
			foreach(KeyValuePair<string,CombatUnits> kvp in Units){
				
				result+=kvp.Value.ToLossesBB()+"[br]";
				
			}
			
			return result;
			
		}
		
		public string ToLossesString(){
			
			string result="";
			
			foreach(KeyValuePair<string,CombatUnits> kvp in Units){
				
				result+=kvp.Value.ToLossesString()+"\r\n";
				
			}
			
			return result;
			
		}
		
		public override string ToString(){
			
			string result="";
			
			foreach(KeyValuePair<string,CombatUnits> kvp in Units){
				
				result+=kvp.Value.ToString()+"\r\n";
				
			}
			
			return result;
			
		}
		
		public int this[string unitName]{
			get{
				
				return Units[unitName].Count;
				
			}
		}
		
		/// <summary>Adds a textual UnitName=Count. The equals can be a , or a space too.</summary>
		public CombatUnits Add(string cmd){
			
			cmd=cmd.Replace(",","=").Replace(" ","=").Replace("\t","=");
			
			string[] pieces=cmd.Split('=');
			
			if(pieces.Length==1){
				return null;
			}
			
			return Add(pieces[0],int.Parse(pieces[1]));
			
		}
		
		public void Add(Dictionary<string,CombatUnits> set,List<UnitData> allowedUnits){
			
			for(int i=0;i<allowedUnits.Count;i++){
				
				string name=allowedUnits[i].RefName;
				
				CombatUnits units;
				if(set.TryGetValue(name,out units)){
					
					// Add:
					Units[name]=units;
					
				}
				
			}
			
		}
		
		public CombatUnits Add(string unitName,int count){
			
			// Tidy up:
			unitName=unitName.ToLower().Trim();
			
			// Create the info:
			CombatUnits info=new CombatUnits(unitName,count);
			
			// Add:
			Units[unitName]=info;
			
			// Set village:
			info.Village=Village;
			
			return info;
			
		}
		
		public void Load(JSObject unitData){
			
			JSArray arr=unitData as JSArray;
			
			foreach(KeyValuePair<string,JSObject> kvp in arr.Values){
				
				JSObject unitCount=arr[kvp.Key];
				
				if(unitCount!=null){
					
					// The count:
					int count=int.Parse(unitCount.ToString());
					
					// Add it:
					Add(kvp.Key,count);
					
				}
				
			}
			
		}
		
	}
	
}