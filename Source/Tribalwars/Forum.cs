using System;
using PowerUI;
using Wrench;
using System.Collections;
using System.Collections.Generic;


namespace Tribalwars{

	public static class Forum{
		
		public const int PostsPerPage=25;
		public const int ThreadsPerPage=25;
		
		public static void NewThread(Account account,int boardID,string title,string messageBb){
			
			if(account==null || account.IsCoPlayer){
				
				account=ResourceNet.MainAccount;
				
			}
			
			account.Client.Send("Forum/createThread","{\"forum_id\":"+boardID+",\"name\":\""+Escape(title)+"\",\"text\":\""+Escape(messageBb)+"\"}");
			
		}
		
		public static void Post(Account account,int threadID,string messageBb){
			
			if(account==null || account.IsCoPlayer){
				
				account=ResourceNet.MainAccount;
				
			}
			
			account.Client.Send("Forum/createPost","{\"thread_id\":"+threadID+",\"text\":\""+Escape(messageBb)+"\"}");
			
		}
		
		public static void Rename(Account account,int threadID,string newTitle){
			
			if(account==null || account.IsCoPlayer){
				
				account=ResourceNet.MainAccount;
				
			}
			
			account.Client.Send("Forum/renameThread","{\"thread_id\":"+threadID+",\"name\":\""+Escape(newTitle)+"\"}");
			
		}
		
		public static string Escape(string text){
			return text.Replace("\"","\\\"").Replace("\r\n","[br]");
		}
		
		public static void GetThreads(Account account,int boardID,int page,Callback gotResult){
			
			// Get page offset:
			int offset=page*ThreadsPerPage;
			
			// Add a callback:
			account.AddCallback("Forum/threadList/"+boardID,gotResult);
			
			// Send the request:
			account.Client.Send("Forum/listThreads","{\"forum_id\":"+boardID+",\"offset\":"+offset+",\"count\":"+ThreadsPerPage+"}");
			
		}
		
		public static void ThreadCreated(int board,JSObject data,Account account){
			
			// Creator ID:
			int creator=int.Parse(data["creator_id"].ToString());
			
			if(creator==account.ID){
				// Just a confirmation that the thread was created.
				return;
			}
			
			// New thread!
			
			// ID:
			int threadID=int.Parse(data["id"].ToString());
			
			// Was it on the resources board?
			if(board==1315){
				
				if(account!=ResourceNet.MainAccount){
					// Every connected account gets this message. We only want the main one to see it.
					return;
				}
				
				// Resources board just had a new thread created!
				
				if(ResourceNet.Handled.ContainsKey(threadID)){
					return;
				}
				
				ResourceNet.Handled[threadID]=true;
				
				// Create a callback:
				ResourcesCallback callback=new ResourcesCallback(creator,threadID);
				
				// Get the posts in the thread:
				GetPosts(account,threadID,0,callback);
				
			}
			
		}
		
		public static void GetPosts(Account account,int threadID,int page,Callback gotResult){
			
			// Get page offset:
			int offset=page*PostsPerPage;
			
			// Add a callback:
			account.AddCallback("Forum/postList/"+threadID,gotResult);
			
			// Send the request:
			account.Client.Send("Forum/listPosts","{\"thread_id\":"+threadID+",\"offset\":"+offset+",\"count\":"+PostsPerPage+",\"reverse\":true}");
			
		}
		
		// Forum/deleteThreads
		
		// BB:
		// [i]h[/i]
		// [b]h[/b]
		// [u]h[/u]
		// [spoiler][/spoiler]
		// [player=PLAYERID]Player name[/player]
		// [village=VILLAGEID]Village Name (X|Y)[/village]
		// [tribe=TRIBEID]Tribe Name[/tribe]
		// [report]REPORTID[/report]
		// [size=medium]normal sized text[/size]
		// 
		
	}

}