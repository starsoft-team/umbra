using System;
using Wrench;
using System.Collections;
using System.Collections.Generic;


namespace Tribalwars{
	
	public class ArmyMovement{
		
		public static int PrenoticeTime=5;
		public static int LatencySeconds=0;
		
		/// Is this an attack?
		public bool Attack;
		/// Target village
		public Village ToVillage;
		/// <summary>Landing time.</summary>
		public uint LandingTime;
		/// Source village
		public Village FromVillage;
		/// <summary>The units in the movement.</summary>
		public List<CombatUnits> Units;
		/// <summary>The officers in the movement.</summary>
		public List<CombatOfficers> Officers;
		/// <summary>The catapult target. Use CatapultTarget instead.</summary>
		private Building RawCatapultTarget;
		
		
		public ArmyMovement(bool attack){
			
			Attack=attack;
			
		}
		
		public ArmyMovement(bool attack,Village fromVillage,Village toVillage){
			
			Attack=attack;
			FromVillage=fromVillage;
			ToVillage=toVillage;
			
		}
		
		public override string ToString(){
			
			return FromVillage.Name+" -> "+ToVillage.Name;
			
		}
		
		/// <summary>The time in seconds to send this attack at if the given landing time is required.</summary>
		public uint GetLaunchTime(uint landingTime){
			
			// How long will it take in seconds?
			int duration=Duration;
			
			return (uint)(landingTime-duration);
		}
		
		/// <summary>Computed duration.</summary>
		public int Duration{
			get{
				
				return (int) ( FromVillage.Distance(ToVillage.X,ToVillage.Y) * (double)Speed );
			
			}
		}
		
		public void AddNoble(){
			Add("snob",1);
		}
		
		public void Add(string unit,int count){
			
			if(Units==null){
				Units=new List<CombatUnits>();
			}
			
			CombatUnits set=GetUnit(unit);
			
			if(set==null){
				
				set=new CombatUnits(FromVillage,unit,count);
				
				Units.Add(set);
				
			}else{
				
				set.Count+=count;
				
			}
			
		}
		
		public CombatOfficers GetOfficer(string name){
			
			if(Officers==null){
				return null;
			}
			
			foreach(CombatOfficers ofc in Officers){
				
				if(ofc.UnitName==name){
					return ofc;
				}
				
			}
			
			return null;
			
		}
		
		public CombatUnits GetUnit(string name){
			
			if(Units==null){
				return null;
			}
			
			foreach(CombatUnits units in Units){
				
				if(units.UnitName==name){
					return units;
				}
				
			}
			
			return null;
			
		}
		
		/// <summary>Speed in seconds/tile.</summary>
		public int Speed{
			get{
				
				if(Units==null){
					return 0;
				}
				
				if(GetOfficer("bastard")!=null){
					
					return UnitData.Get("snob").Speed;
					
				}
				
				int slowest=0;
				
				foreach(CombatUnits units in Units){
					
					int speed=units.Speed;
					
					if(speed>slowest){
						slowest=speed;
					}
					
				}
				
				return slowest;
				
			}
		}
		
		public Building CatapultTarget{
			get{
				
				if(RawCatapultTarget==null){
					RawCatapultTarget=Buildings.Get("headquarter");
				}
				
				return RawCatapultTarget;
				
			}
			set{
				RawCatapultTarget=value;
			}
		}
		
		public bool Support{
			get{
				return !Attack;
			}
		}
		
		public int LandAt(uint time,bool blockLate){
			
			Console.WriteLine("Request to land at "+Time.GetDate(time).ToString("dd MM HH:mm:ss"));
			
			return SendAt((uint)(time-Duration),blockLate);
			
		}
		
		public int SendAt(uint time,bool blockLate){
			
			// Shift it by the latency:
			time=(uint)(time+LatencySeconds);
			
			int minsPrenotice=PrenoticeTime;
			
			long msPrenotice=(long)(minsPrenotice * 60 * 1000);
			
			long timeAt=(long)time * 1000;
			
			int inSec=(int)((timeAt-Time.NowMilliseconds) / 1000);
			
			long preNotice=timeAt-msPrenotice;
			
			if(inSec<0 && blockLate){
				// Failed!
				return -1;
			}
			
			Console.WriteLine(Time.Now+", "+Time.NowMilliseconds+". "+Duration+". Sending at "+time+". s until notice: "+((preNotice - Time.NowMilliseconds) / 1000)+". s until hit: "+inSec);
			
			TimeEvent notice=TimedQueues.Wait(preNotice,delegate(TimeEvent te){
				
				// x minute notice!
				
				string type;
				
				if(Attack){
					type="Attack";
				}else{
					type="Support";
				}
				
				string msg=type+" from "+FromVillage.ToForumBB()+" to "+ToVillage.ToForumBB()+" is leaving in "+minsPrenotice+" minutes to land at "+Time.GetDate((uint)(time+Duration)).ToString("dd MM HH:mm:ss")+" (clock latency of "+LatencySeconds+" seconds).";
				
				Console.WriteLine(msg);
				
				Forum.Post(FromVillage.Account,12016,msg);
				
			});
			
			notice.OnDebug=delegate(TimeEvent te){
				
				return "Attack prenotice: "+((preNotice - Time.NowMilliseconds) / 1000)+"s";
				
			};
			
			TimeEvent attackEvent=TimedQueues.Wait(timeAt, delegate(TimeEvent te){
				
				Submit();
				
			});
			
			ArmyMovement movement=this;
			
			attackEvent.OnDebug=delegate(TimeEvent te){
				
				return "Attack: "+movement.ToString();
				
			};
			
			return inSec;
			
		}
		
		/// <summary>Sends a created army movement.</summary>
		public void Submit(){
			
			string type;
			
			if(Attack){
				type="attack";
			}else{
				type="support";
			}
			
			string officers="";
			
			if(Officers!=null){
				
				for(int i=0;i<Officers.Count;i++){
					
					// Get officers:
					CombatOfficers officer=Officers[i];
					
					if(officers!=""){
						officers+=",";
					}
					
					officers+="\""+officer.UnitName+"\":"+officer.Count;
					
				}
				
			}
			
			string units="";
			
			if(Units!=null){
				
				for(int i=0;i<Units.Count;i++){
					
					// Get units:
					CombatUnits unit=Units[i];
					
					if(units!=""){
						units+=",";
					}
					
					units+="\""+unit.UnitName+"\":"+unit.Count;
					
				}
				
			}
			
			string cataTarget=CatapultTarget.RefName;
			
			FromVillage.Client.Send(
				"Command/sendCustomArmy","{\"start_village\":"+FromVillage.ID+
				",\"target_village\":"+ToVillage.ID+",\"type\":\""+type+"\",\"units\":{"+units+
				"},\"officers\":{"+officers+"},\"icon\":0,\"catapult_target\":\""+cataTarget+"\"}"
			);
			
		}
		
	}
	
}