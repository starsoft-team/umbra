using System;
using System.Collections;
using System.Collections.Generic;
using Wrench;


namespace Tribalwars{
	
	/// <summary>
	/// Used to represent a set of building levels, e.g. from a scout report.
	/// </summary>
	
	public class BuildingLevels{
		
		public static int GetTotalPoints(Dictionary<string,BuildingLevelInfo> levels){
			
			int apparentPoints=0;
			
			// For each building..
			foreach(KeyValuePair<string,Building> kvp in Buildings.All){
				
				// It's level (if any):
				BuildingLevelInfo levelInfo;
				
				// Get the level for this building:
				if(levels!=null && levels.TryGetValue(kvp.Key,out levelInfo)){
					
					// Got a level for it - how many points does that level gain (total)?
					apparentPoints+=levelInfo.TotalPoints;
					
				}
				
			}
			
			return apparentPoints;
			
		}
		
		public static int GetTotalPopulation(Dictionary<string,BuildingLevelInfo> levels){
			
			int apparentPoints=0;
			
			// For each building..
			foreach(KeyValuePair<string,Building> kvp in Buildings.All){
				
				// It's level (if any):
				BuildingLevelInfo levelInfo;
				
				// Get the level for this building:
				if(levels!=null && levels.TryGetValue(kvp.Key,out levelInfo)){
					
					// Got a level for it - how many pop does that level require?
					apparentPoints+=levelInfo.TotalPopulation;
					
				}
				
			}
			
			return apparentPoints;
			
		}
		
		/// <summary>Optional vill.</summary>
		public Village Village;
		/// <summary>The set of levels in this set.</summary>
		public Dictionary<string,BuildingLevelInfo> Levels=new Dictionary<string,BuildingLevelInfo>();
		
		
		public int Count{
			get{
				return Levels.Count;
			}
		}
		
		public BuildingLevels(){}
		
		public BuildingLevels(Village village):this(village,true){}
		
		public BuildingLevels(Village village,bool cloneLevels){
			Village=village;
			
			// Clone?
			if(cloneLevels){
				
				foreach(KeyValuePair<string,BuildingLevelInfo> kvp in village.BuildingInfo){
					
					Levels[kvp.Key]=kvp.Value;
					
				}
				
			}
			
		}
		
		public override string ToString(){
			
			string result="";
			
			foreach(KeyValuePair<string,BuildingLevelInfo> kvp in Levels){
				
				result+=kvp.Value.ToString()+"\r\n";
				
			}
			
			return result;
			
		}
		
		public int this[string buildingName]{
			get{
				
				return Levels[buildingName].Level;
				
			}
		}
		
		/// <summary>Adds a textual BuildingName=Level. The equals can be a , or a space too.</summary>
		public BuildingLevelInfo Add(string cmd){
			
			cmd=cmd.Replace(",","=").Replace(" ","=").Replace("\t","=");
			
			string[] pieces=cmd.Split('=');
			
			if(pieces.Length==1){
				return null;
			}
			
			return Add(pieces[0],int.Parse(pieces[1]));
			
		}
		
		public void Load(JSObject buildData){
			
			foreach(KeyValuePair<string,Building> kvp in Buildings.All){
				
				JSObject buildingLevel=buildData[kvp.Key];
				
				if(buildingLevel!=null){
					
					// The building level:
					int level=int.Parse(buildingLevel.ToString());
					
					// Add it:
					Add(kvp.Value,level);
					
				}
				
			}
			
		}
		
		public BuildingLevelInfo Add(Building building,int level){
			
			// Create the info:
			BuildingLevelInfo info=new BuildingLevelInfo(building,level);
			
			// Add:
			Levels[building.RefName]=info;
			
			// Set village:
			info.Village=Village;
			
			return info;
			
		}
		
		public BuildingLevelInfo Add(string building,int level){
			
			// Tidy up:
			building=building.ToLower().Trim();
			
			// Create the info:
			BuildingLevelInfo info=new BuildingLevelInfo(building,level);
			
			// Add:
			Levels[building]=info;
			
			// Set village:
			info.Village=Village;
			
			return info;
			
		}
		
		public int Population{
			get{
				
				return GetTotalPopulation(Levels);
				
			}
		}
		
		public int TotalPoints{
			get{
				
				return GetTotalPoints(Levels);
				
			}
		}
		
		/// <summary>Is this set of levels camoflauged?</summary>
		public bool Camoflauged(int points){
			
			// The total # of points from the actual buildings. 
			// It's camoflauged if this does not match the given points.
			return TotalPoints!=points;
			
		}
		
	}
	
}