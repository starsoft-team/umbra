using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Wrench;
using PowerUI;


namespace Tribalwars{
	
	/// <summary>
	/// Scans the map grabbing various useful changes every x minutes.
	/// </summary>
	
	public static class MapScanner{
		
		public static string GenerateMap(bool dynam){
			
			string dyn="0";
			
			if(dynam){
				dyn="1";
			}
			
			string result=HttpMain.Post("http://www.tw2-tools.com/en13/map/settings/index.php?__ajax=1","map-zoom=1&map-x=500&map-y=500&map-bg-color=004000&map-pv=1&map-pv-color=541900&map-bv-color=666666&map-grid=1&map-grid-color=888888&map-vpts-min=900&map-vpts-max=12000&map-legend=1&map-legend-bg-color=004000&map-legend-text-color=ffffff&map-legend-cols=1&map-legend-corner-h=left&map-legend-corner-v=top&map-legend-pos-h=5&map-legend-pos-v=5&map-t%5B0%5D%5Benabled%5D=1&map-t%5B0%5D%5Bcolor%5D=63cfa6&map-t%5B0%5D%5Bname%5D=RLP&map-t%5B1%5D%5Benabled%5D=1&map-t%5B1%5D%5Bcolor%5D=78db6d&map-t%5B1%5D%5Bname%5D=BzB&map-t%5B2%5D%5Benabled%5D=1&map-t%5B2%5D%5Bcolor%5D=59b528&map-t%5B2%5D%5Bname%5D=LOD&map-t%5B3%5D%5Benabled%5D=1&map-t%5B3%5D%5Bcolor%5D=6b945c&map-t%5B3%5D%5Bname%5D=ZRG&map-t%5B4%5D%5Benabled%5D=1&map-t%5B4%5D%5Bcolor%5D=2969ff&map-t%5B4%5D%5Bname%5D=Nub&map-t%5B5%5D%5Benabled%5D=1&map-t%5B5%5D%5Bcolor%5D=d6fa20&map-t%5B5%5D%5Bname%5D=(K)&map-t%5B6%5D%5Benabled%5D=1&map-t%5B6%5D%5Bcolor%5D=ebf288&map-t%5B6%5D%5Bname%5D=ALF&map-t%5B7%5D%5Benabled%5D=1&map-t%5B7%5D%5Bcolor%5D=e3e014&map-t%5B7%5D%5Bname%5D=B-H&map-t%5B8%5D%5Benabled%5D=1&map-t%5B8%5D%5Bcolor%5D=76cc52&map-t%5B8%5D%5Bname%5D=S-I&map-t%5B9%5D%5Benabled%5D=1&map-t%5B9%5D%5Bcolor%5D=6fb373&map-t%5B9%5D%5Bname%5D=twh&map-t%5B10%5D%5Benabled%5D=1&map-t%5B10%5D%5Bcolor%5D=c273d4&map-t%5B10%5D%5Bname%5D=TDB&map-dynamic="+dyn);
			
			if(result==null){
				return null;
			}
			
			// Parse:
			JSObject json=JSON.Parse(result);
			
			if(json==null || json["redirect"]==null){
				return null;
			}
			
			return json["redirect"].ToString();
			
		}
		
		/// <summary>Every x mins.</summary>
		public const int Minutes=30;
		/// <summary>Every x sec.</summary>
		public const int Seconds=Minutes * 60;
		
		/// <summary>True if the scanner should be running. Use the "map" command line arg to set this.</summary>
		public static bool Active;
		/// <summary>The number of accounts that profile info is currently pending for.</summary>
		public static int AccountsPending;
		/// <summary>The account to request with. Must have been loaded properly before this.</summary>
		public static Account Account;
		/// <summary>The current village data file being written to.</summary>
		public static DataFileWriter VillageWriter;
		/// <summary>The current account data file being written to.</summary>
		public static DataFileWriter AccountWriter;
		/// <summary>The system timer that will time the callback.</summary>
		private static System.Timers.Timer InternalTimer;
		
		/// <summary>True if it's been started.</summary>
		public static bool Started{
			get{
				return InternalTimer!=null;
			}
		}
		
		public static void Start(Account account){
			
			if(Started){
				return;
			}
			
			Account=account;
			
			InternalTimer=new System.Timers.Timer();
			InternalTimer.Elapsed+=Elapsed;
			InternalTimer.Interval=Seconds * 1000;
			InternalTimer.Start();
			
			if(!Directory.Exists("Villages")){
				Directory.CreateDirectory("Villages");
			}
			
			if(!Directory.Exists("Accounts")){
				Directory.CreateDirectory("Accounts");
			}
			
			ScanNow();
			
		}
		
		/// <summary>The method called when the system timer has waited for the specified interval.</summary>
		private static void Elapsed(object sender,System.Timers.ElapsedEventArgs e){
			
			ScanNow();
		
		}
		
		public static void ScanNow(){
			
			Console.WriteLine("Map scan starting..");
			
			// Clear pending #:
			AccountsPending=0;
			
			// Clear profiles that have been requested:
			Accounts.ClearProfiles();
			
			// Get the current time:
			uint time=Time.Now;
			
			// Create the writers:
			VillageWriter=new DataFileWriter("Villages/"+time+".txt");
			AccountWriter=new DataFileWriter("Accounts/"+time+".txt");
			
			// Get the sections to load:
			List<MapSection> sections=MapSection.GetInRange(631,683,100);
			
			// For each one..
			foreach(MapSection section in sections){
				
				// Clear it's loaded state:
				section.RequireLoad();
				
				// Load the actual villages now:
				
				section.GetContent(Account,delegate(MapSection sec){
					
					// Villages loaded!
					
					// For each village, are the points > 200? if yes, get player info.
					foreach(Village village in sec.Villages){
						
						if(village.Owner==null){
							// Barbarian
							continue;
						}
						
						if(village.Points>100){
							
							// Get the owners profile using account as the server link to request it:
							
							if(!village.Account.RequestedProfile){
								
								// On got event:
								village.Account.addEventListener("onprofile",delegate(UIEvent er){
									
									Account acnt=er.Account;
									
									// Get it as a data string:
									string dataString=acnt.ToDataString();
									
									AccountsPending--;
									
									// Write the account data out:
									AccountWriter.WriteLine(dataString);
									
									if(AccountsPending==0){
										
										Console.WriteLine("Completed a map scan!");
										
										// Close writer:
										AccountWriter.Close();
										
										// Great - close village writer:
										VillageWriter.Close();
										
										AccountWriter=null;
										VillageWriter=null;
										
									}
									
								});
							
								AccountsPending++;
								
								// Req the profile:
								village.Account.GetProfile(Account);
								
								// Wait a sec..
								System.Threading.Thread.Sleep(50);
								
							}
							
							// Get village data:
							string villageData=village.ToDataString();
							
							// Write out the village data:
							VillageWriter.WriteLine(villageData);
							
						}
						
					}
					
				});
				
				// Wait a sec.. (don't exactly want to thrash their server lol)
				System.Threading.Thread.Sleep(50);
				
			}
			
			
		}
		
		
	}
	
	
}