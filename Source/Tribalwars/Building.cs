using System;
using System.Collections;
using System.Collections.Generic;
using Wrench;


namespace Tribalwars{

	public class Building{
		
		/// <summary>This time factor represents a rounding towards 5 minutes. (rounded up, because they are heartless creatures)</summary>
		public const float GlobalTimeFactor=300;
		
		/// <summary>The internal name e.g. "clay_pit".</summary>
		public string RefName;
		public JSObject RawData;
		public float Function;
		public float FunctionFactor;
		public float BuildRate;
		public int MaxLevel;
		public float BuildTimeFactor;
		public float BuildTimeOffset;
		public float PointsFactor;
		public float PointsOffset;
		public Dictionary<string,Research> Researches;
		/// <summary>Specifically defined costs for particular levels.</summary>
		public Dictionary<int,LevelCost> IndividualLevelCosts;
		
		
		public Building(string refName,JSObject data){
			
			RefName=refName;
			
			RawData=data;
			
			MaxLevel=(int)GetFloat("max_level");
			
			BuildRate=GetFloat("build_time");
			
			Function=GetFloat("function");
			
			FunctionFactor=GetFloat("function_factor");
			
			BuildTimeOffset=GetFloat("build_time_offset");
			
			BuildTimeFactor=GetFloat("build_time_factor");
			
			PointsOffset=GetFloat("points");
			
			PointsFactor=GetFloat("points_factor");
			
			// Got level costs?
			JSArray arr=this["individual_level_costs"] as JSArray;
			
			if(arr!=null && arr.Values!=null){
				
				IndividualLevelCosts=new Dictionary<int,LevelCost>();
			
				foreach(KeyValuePair<string,JSObject> kvp in arr.Values){
					
					LevelCost cost=new LevelCost(this,kvp.Value);
					
					IndividualLevelCosts[int.Parse(kvp.Key)]=cost;
					
				}
				
			}
			
			Researches=new Dictionary<string,Research>();
			
		}
		
		public void AddResearch(string name,JSObject data){
			
			Research research=new Research(this,name,data);
			
			Researches[name]=research;
			
		}
		
		public float WallFactor(Village village){
			
			if(village==null){
				return 1f;
			}
			
			Dictionary<string,Research> wall=Buildings.Get("wall").Researches;
			Dictionary<string,ResearchInfo> e=village.GetBuilding("wall").Researches;
			float f=1f;
			
			foreach(KeyValuePair<string,ResearchInfo> kvp in e){
				
				if(kvp.Value.Active){
					f-=wall[kvp.Key].Function;
				}
				
			}
			
			return f;

		}
		
		public LevelCost[] GetCosts(Village village){
			
			int hqLevel;
			float speed;
			
			if(village==null){
				
				hqLevel=1;
				speed=1f;
				
			}else{
				
				speed=village.World.Speed;
				
				BuildingLevelInfo f=village.GetBuilding("headquarter");
				hqLevel=f.Level;
				
			}
			
			return GetCosts(speed,hqLevel,village);
		}
		
		/// <summary>Gets all the costs of building at all levels.</summary>
		public LevelCost[] GetCosts(float speed,int hqLevel,Village village){
			
			LevelCost[] costs=new LevelCost[MaxLevel];
			
			for(int i=1;i<=MaxLevel;i++){
				
				costs[i-1]=GetCost(i,village);
				
			}
			
			return costs;
			
		}
		
		public LevelCost GetCost(int level,Village village){
			
			int hqLevel;
			float speed;
			
			if(village==null){
				
				hqLevel=1;
				speed=1f;
				
			}else{
				
				speed=village.World.Speed;
				
				BuildingLevelInfo f=village.GetBuilding("headquarter");
				hqLevel=f.Level;
				
			}
			
			return GetCost(level,speed,hqLevel,village);
		}
		
		public int TotalFood(int level){
			
			// Food first:
			float baseRate=GetFloat("food");
			float factor=GetFloat("food_factor");
			
			int c = GetLevelScale(baseRate, factor, level);
			
			return (int)Math.Round((float)c);
			
		}
		
		/// <summary>Gets all the costs of building at the given level.</summary>
		public LevelCost GetCost(int level,float speed,int hqLevel,Village village){
			
			if(level > MaxLevel){
				
				return null;
				
			}else if(IndividualLevelCosts!=null && IndividualLevelCosts.ContainsKey(level) ){
				
				return IndividualLevelCosts[level];
				
			}
			
			// Create the cost:
			LevelCost cost=new LevelCost(this);
			
			// Food first:
			float baseRate=GetFloat("food");
			float factor=GetFloat("food_factor");
			
			int b = GetLevelScale(baseRate, factor, level - 1);
			int c = GetLevelScale(baseRate, factor, level);
			
			cost.Food = (int)Math.Round((float)(c - b));
			
			// Get the build rate:
			cost.BuildRate=GetBuildTime(level,speed,hqLevel,village);
			
			string[] resources=new string[]{"wood", "clay", "iron"};
			
			for(int i=0;i<3;i++){
				
				string a=resources[i];
			
				baseRate=GetFloat(a);
				factor=GetFloat(a +"_factor");
				
				int required=GetLevelScale(baseRate, factor, level);
				
				if(a=="iron"){
					cost.Iron=required;
				}else if(a=="wood"){
					cost.Wood=required;
				}else{
					// clay
					cost.Clay=required;
				}
				
			}
			
			// Points:
			cost.TotalPoints=GetLevelScale(PointsOffset,PointsFactor,level);
			
			return cost;
			
		}
		
		/// <summary>Total points for a given level.</summary>
		public int GetPoints(int level){
			
			return GetLevelScale(PointsOffset,PointsFactor,level);
			
		}
		
		public int GetLevelForPoints(int matchPoints){
			
			for(int i=1;i<=MaxLevel;i++){
				
				// Get points:
				int points=GetLevelScale(PointsOffset,PointsFactor,i);
				
				if(points==matchPoints){
					
					return i;
					
				}
				
			}
			
			return -1;
			
		}
		
		public int GetLevelScale(float baseRate,float factor,int level){
			
			if(level==0){
				return 0;
			}
			
			return (int)Math.Round(baseRate * Math.Pow(factor, level - 1));
			
		}
		
		public float GetLevelScaleFloat(float baseRate,float factor,int level){
			
			if(level==0){
				return 0f;
			}
			
			return (float)(baseRate * Math.Pow(factor, level - 1));
			
		}
		
		public int GetBuildTime(int targetLevel,Village village){
			
			int hqLevel;
			float speed;
			
			if(village==null){
				
				hqLevel=1;
				speed=1f;
				
			}else{
				
				speed=village.World.Speed;
				
				BuildingLevelInfo f=village.GetBuilding("headquarter");
				hqLevel=f.Level;
				
			}
			
			return GetBuildTime(targetLevel,speed,hqLevel,village);
			
		}
		
		public int GetBuildTime(int targetLevel,float speed,int hqLevel,Village village){
			
			float d;
			
			if(RefName=="wall"){
				d=WallFactor(village);
			}else{
				d=1f;
			}
			
			Building e=Buildings.Get("headquarter");
			
			float j;
			
			if(hqLevel==0){
				j=0f;
			}else{
				j=e.Function * (float)Math.Pow(e.FunctionFactor,hqLevel-1);
			}
			
			float k=BuildRate * (float)Math.Pow(BuildTimeFactor,targetLevel+BuildTimeOffset/targetLevel);
			
			// Array.isArray(c.individual_level_costs) ? 
				// k = parseInt(k * j * d / speed, 10)
				// else
				// c.individual_level_costs[b] && (k = parseInt(Math.round(c.individual_level_costs[b].build_time * j), 10)) || q > k ? 
				// k
				// else
				// parseInt(Math.round(k / q) * q, 10)
			
			if(IndividualLevelCosts!=null){
				
				if( IndividualLevelCosts.ContainsKey(targetLevel) ){
					
					k=(float)Math.Round(IndividualLevelCosts[targetLevel].BuildRate*j);
					
				}
				
			}else{
			
				k=(k*j*d/speed);
			
			}
			
			if( GlobalTimeFactor>k ){
				
				return (int)k;
			
			}
			
			return (int) ( Math.Round(k/GlobalTimeFactor)*GlobalTimeFactor );
			
		}
		
		public void DisplayRequirements(){
			DisplayRequirements(1f,1,null);
		}
		
		public void DisplayRequirements(Village village){
			
			int hqLevel;
			float speed;
			
			if(village==null){
				
				hqLevel=1;
				speed=1f;
				
			}else{
				
				speed=village.World.Speed;
				
				BuildingLevelInfo f=village.GetBuilding("headquarter");
				hqLevel=f.Level;
				
			}
			
			DisplayRequirements(speed,hqLevel,village);
			
		}
		
		public void DisplayRequirements(float speed,int hqLevel,Village village){
			
			Console.WriteLine("-Requirements for "+RefName+" (Speed "+speed+", HQ "+hqLevel+")-");
			
			for(int i=1;i<=MaxLevel;i++){
				
				Console.WriteLine("Level "+i+" - "+GetCost(i,village).ToString() );
				
			}
			
			Console.WriteLine("");
			
		}
		
		public JSObject this[string property]{
			get{
				return RawData[property];
			}
		}
		
		public string GetString(string property){
			JSObject val=RawData[property];
			
			if(val==null){
				return null;
			}
			
			return val.ToString();
		}
		
		public float GetFloat(string property){
			
			string value=GetString(property);
			
			if(value==null){
				return 0f;
			}
			
			return float.Parse(value);
		}
		
	}

}