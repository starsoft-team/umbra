using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using Wrench;


namespace Tribalwars{
	
	public static class ResourceDeposit{
		
		
		public static void OpenAndCollect(Account account){
			
			account.Client.Send("ResourceDeposit/open");
			
			// Results in OnOpened being called.
			
		}
		
		public static void CollectJob(Account account,int jobID,int intoVillage){
			
			Console.WriteLine("Collect job "+jobID);
			
			account.Client.Send("ResourceDeposit/collect","{\"job_id\":"+jobID+",\"village_id\":"+intoVillage+"}");
			
		}
		
		public static void StartJob(Account account,int jobID){
			
			Console.WriteLine("Start job "+jobID);
			
			account.Client.Send("ResourceDeposit/startJob","{\"job_id\":"+jobID+"}");
			
			// Results in OnStarted being called.
			
		}
		
		public static void WaitUntil(long time,OnTimeEvent run){
			
			TimedQueues.Wait(time,3000,8000,run);
			
		}
		
		public static void OnCollected(JSObject data,GameClient client){
			
			// Make sure the collected job is removed:
			string index;
			JSObject job=GetJob(client,data,out index);
			
			if(job!=null){
				
				client.Account.ResourceJobs.Values.Remove(index);
				
			}
			
			StartAnyJob(client.Account);
			
		}
		
		public static void OnStarted(JSObject data,GameClient client){
			
			// Get the account data and add a time_completed like so:
			string index;
			JSObject job=GetJob(client,data,out index);
			
			if(job!=null){
				job["time_completed"]=data["time_completed"];
			}
			
			// Await complete so we can queue it up.
			TryAwaitCollect(data,client);
			
		}
		
		private static JSObject GetJob(GameClient client,JSObject data,out string index){
			
			if(client.Account.ResourceJobs!=null){
			
				JSObject id=data["id"];
				
				if(id==null){
					id=data["job_id"];
				}
				
				foreach(KeyValuePair<string,JSObject> kvp in client.Account.ResourceJobs.Values){
					
					// Any collectibles/ in progress?
					JSObject job=kvp.Value;
					
					if(job["id"].ToString()==id.ToString()){
						
						index=kvp.Key;
						return job;
						
					}
					
				}
				
			}
			
			index=null;
			return null;
			
		}
		
		/// <returns>True if the job was in progress.</summary>
		private static bool TryAwaitCollect(JSObject job,GameClient client){
			
			// if it has time_completed, its in progress.
			JSObject tc=job["time_completed"];
			
			if(tc!=null){
				
				long time=long.Parse(tc.ToString());
				
				JSObject id=job["job_id"];
				
				if(id==null){
					id=job["id"];
				}
				
				int jobID=int.Parse(id.ToString());
				
				if(time<=Time.Now){
					
					// Collect it right now:
					CollectJob(client.Account,jobID,client.Account.AnyVillage.ID);
					
				}else{
					
					// Wait for time to pass, then collect it:
					WaitUntil(time * 1000,delegate(TimeEvent e){
						
						// Try collect and start now:
						TryCollectAndStart(client);
						
					});
					
				}
				
				return true;
				
			}
			
			return false;
			
		}
		
		public static void WaitNewJobs(Account account){
			
			Console.WriteLine("Waiting for new res jobs.. "+account.NewResourceJobs);
			
			WaitUntil(account.NewResourceJobs,delegate(TimeEvent e){
				
				// Open + collect:
				OpenAndCollect(account);
				
			});
			
		}
		
		public static bool StartAnyJob(Account account){
			
			if(account.ResourceJobs==null || account.ResourceJobs.Values==null){
				WaitNewJobs(account);
				return false;
			}
			
			foreach(KeyValuePair<string,JSObject> kvp in account.ResourceJobs.Values){
				
				// Any collectibles/ in progress?
				JSObject job=kvp.Value;
				
				JSObject tc=job["time_completed"];
				
				if(tc==null){
					
					// Start it:
					StartJob(account,int.Parse(job["id"].ToString()));
					
					// Quit:
					return true;
					
				}
				
			}
			
			WaitNewJobs(account);
			return false;
			
		}
		
		public static void OnOpened(JSObject data,GameClient client){
			
			// ResourceDeposit/info
			
			JSArray jobs=data["jobs"] as JSArray;
			
			// Apply time to acc:
			client.Account.NewResourceJobs=long.Parse(data["time_next_reset"].ToString()) * 1000;
			
			// Apply to acc:
			client.Account.ResourceJobs=jobs;
			
			TryCollectAndStart(client);
			
		}
		
		public static void TryCollectAndStart(GameClient client){
			
			JSArray jobs=client.Account.ResourceJobs;
			
			bool inProgress=false;
			
			if(jobs!=null && jobs.Values!=null){
				
				foreach(KeyValuePair<string,JSObject> kvp in jobs.Values){
					
					// Any collectibles/ in progress?
					JSObject job=kvp.Value;
					
					bool progress=TryAwaitCollect(job,client);
					
					if(progress){
						inProgress=true;
					}
					
				}
				
			}
			
			if(!inProgress){
				
				// Start one (any - really doesnt matter) now:
				StartAnyJob(client.Account);
				
			}
			
		}
		
	}
	
}