using System;
using Wrench;


namespace Tribalwars{
	
	public class CombatUnits{
		
		/// <summary>Losses when this units set is used in the sim.</summary>
		public double RawLosses;
		public string UnitName; // e.g. spear, sword, axe, knight (paladin)
		public int Count; // # of these units
		public double InTown; // When used in Village objects (the UnitInfo set), how many are currently in town.
		public Village Village; // optional source village
		
		
		public CombatUnits(Village village,string unit,int count){
			
			Village=village;
			UnitName=unit;
			Count=count;
			
		}
		
		public CombatUnits(string unit,int count){
			
			UnitName=unit.Trim().ToLower();
			Count=count;
			
		}
		
		public int Losses{
			get{
				return (int)Math.Round(RawLosses);
			}
		}
		
		public string ToLossesBB(){
			
			return UnitName+" losses: [color=cc3300]"+Losses+"[/color] / "+Count;
			
		}
		
		public string ToLossesString(){
			
			return UnitName+" losses: "+Losses+" / "+Count;
			
		}
		
		public override string ToString(){
			
			return UnitName+"="+Count;
			
		}
		
		public UnitData UnitData{
			get{
				return Units.Get(UnitName);
			}
		}
		
		public int Speed{
			get{
				
				UnitData data=UnitData;
				
				if(data==null){
					Console.WriteLine("UNIT NOT FOUND: '"+UnitName+"'");
				}
				
				return data.Speed;
				
			}
		}
		
		/// <summary>The farming capacity of these units, if their suitable to farm with.</summary>
		public int FarmingCapacity{
			get{
				
				// Get unit data:
				UnitData data=UnitData;
				
				if(!data.GoodForFarming){
					
					return 0;
					
				}
				
				int baseRate=data.Carry * Count;
				
				if(Village!=null){
					
					baseRate = (int) (baseRate * Village.Account.CarryCapacityBonus);
					
				}
				
				return baseRate;
			}
		}
		
	}
	
}