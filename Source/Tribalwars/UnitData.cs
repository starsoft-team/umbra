using System;
using System.Collections;
using System.Collections.Generic;
using Wrench;


namespace Tribalwars{

	public class UnitData{
		
		public static UnitData Get(string name){
			return Units.Get(name);
		}
		
		/// <summary>The internal name e.g. "clay_pit".</summary>
		public string RefName;
		public JSObject RawData;
		public int Wood;
		public int Clay;
		public int Iron;
		public int Food;
		public int Carry;
		public int Speed; // in seconds per tile
		public int BuildTime;
		public bool IsArcher;
		public bool IsCavalry;
		public int Attack; // Attack str
		public int DefenseInfantry; // def str
		public int DefenseCavalry; // def str
		public int DefenseArchers; // def str
		public bool GoodForFarming;
		public int BashPointsDefense;
		public int BashPointsOffense;
		public bool DefensiveUnit;
		
		
		public UnitData(string refName,JSObject data){
			
			RefName=refName;
			
			RawData=data;
			
			Wood=GetInt("wood");
			Clay=GetInt("clay");
			Iron=GetInt("iron");
			Carry=GetInt("load");
			Food=GetInt("food");
			DefenseInfantry=GetInt("def_inf");
			DefenseCavalry=GetInt("def_kav");
			DefenseArchers=GetInt("def_arc");
			Speed=GetInt("speed") * 60;
			BuildTime=GetInt("build_time");
			BashPointsOffense=GetInt("points_att");
			BashPointsDefense=GetInt("points_def");
			
			GoodForFarming=(Carry!=0);
			DefensiveUnit=(BashPointsDefense > BashPointsOffense);
			
			Attack=GetInt("attack");
			
			if(refName=="ram"){
				Battle.RamAttack=Attack;
			}
			
			// Figure out which type of attacker this one is:
			if(refName=="light_cavalry" || refName=="heavy_cavalry" || refName=="knight"){
				
				IsCavalry=true;
				
			}else if(refName=="archer" || refName=="mounted_archer"){
				
				IsArcher=true;
				
			}
			
		}
		
		public double RecruitTime(Village village){
			
			double h;
			
			if("snob" == RefName || "doppelsoldner" == RefName || "trebuchet" == RefName || "knight" == RefName){
				
				h=1;
				
			}else{
				
				// Get the barracks:
				BuildingLevelInfo barracks=village.GetBuilding("barracks");
				
				Building e = barracks.Building;
				h = e.GetLevelScaleFloat(e.Function,e.FunctionFactor,barracks.Level);
				
				// Recruit speed effect (tribe skill)
				double c=village.Account.RecruitSpeedBonus;
				
				if(c!=1f){
					
					h /= c;
					
				}
				
			}
			
			return (double)BuildTime / village.World.Speed * h;
			
		}
		
		public JSObject this[string property]{
			get{
				return RawData[property];
			}
		}
		
		public string GetString(string property){
			JSObject val=RawData[property];
			
			if(val==null){
				return null;
			}
			
			return val.ToString();
		}
		
		public int GetInt(string property){
			
			string value=GetString(property);
			
			if(value==null){
				return 0;
			}
			
			return int.Parse(value);
		}
		
	}

}