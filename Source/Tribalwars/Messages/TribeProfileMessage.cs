using System;
using System.Collections;
using System.Collections.Generic;
using Wrench;


namespace Tribalwars{
	
	/// <summary>
	/// Info about a tribe.
	/// </summary>
	
	public class TribeProfileMessage : MessageHandler{
		
		public override string Type{
			get{
				return "Tribe/profile";
			}
		}
		
		public override void Handle(JSObject args,GameClient client){
			
			// Get account obj:
			int id=int.Parse(args["character_id"].ToString());
			
			// Get:
			Tribe tribe=Tribes.GetOrCreate(id);
			
			tribe.Description=args["description"].ToString();
			tribe.Name=args["name"].ToString();
			tribe.Points=int.Parse(args["total_points"].ToString());
			tribe.Rank=int.Parse(args["rank"].ToString());
			
			tribe.BashPointsOffense=int.Parse(args["bash_points_off"].ToString());
			tribe.BashPointsDefense=int.Parse(args["bash_points_def"].ToString());
			
			tribe.Level=int.Parse(args["level"].ToString());
			
			tribe.Members=int.Parse(args["members"].ToString());
			
			// Not sure what this is but it sounds fun!
			tribe.KillScore=int.Parse(args["kill_score"].ToString());
			
		}
		
	}
	
}