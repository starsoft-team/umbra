using System;
using Wrench;


namespace Tribalwars{
	
	public class CharacterSelectedMessage : MessageHandler{
		
		public override string Type{
			get{
				return "Authentication/characterSelected";
			}
		}
		
		public override void Handle(JSObject args,GameClient client){
			
			Console.WriteLine("-Logged in!-");
			
			// Tribe ID:
			JSObject tribe=args["tribe_id"];
			
			if(tribe!=null){
			
				int tribeID=int.Parse(tribe.ToString());
			
				client.Account.Tribe=Tribes.GetOrCreate(tribeID,"");
				
			}
			
			// Logged in!
			client.Account.triggerEvent("onconnected");
			
			// Get villages and other account info:
			client.Send("Character/getInfo");
			
			// Get daily login bonus:
			client.Send("DailyLoginBonus/getInfo");
			
		}
		
	}
	
}