using System;
using System.Collections;
using System.Collections.Generic;
using Wrench;


namespace Tribalwars{
	
	public class ReportViewMessage : MessageHandler{
		
		public override string Type{
			get{
				return "Report/view";
			}
		}
		
		public override void Handle(JSObject args,GameClient client){
			
			string token=args["token"].ToString();
			
			// Get the report by its token:
			for(int i=0;i<Report.AllLoading.Count;i++){
				
				Report current=Report.AllLoading[i];
				
				if(current.Token==token){
					
					// Got it!
					
					// Remove:
					Report.AllLoading.RemoveAt(i);
					
					// Got the data:
					current.GotData(args);
					
					break;
					
				}
				
			}
			
		}
		
	}
	
}