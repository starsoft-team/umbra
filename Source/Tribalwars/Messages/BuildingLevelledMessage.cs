using System;
using Wrench;


namespace Tribalwars{
	
	/// <summary>
	/// Used when a building gets updated.
	/// </summary>
	
	public class BuildingLevelledMessage : MessageHandler{
		
		public override string Type{
			get{
				return "Building/levelChanged";
			}
		}
		
		public override void Handle(JSObject args,GameClient client){
			
			// Village ID:
			int villageID=int.Parse(args["village_id"].ToString());
			
			// Get the village.
			Village village=client.Account.GetVillage(villageID);
			
			if(village==null){
				return;
			}
			
			// Building level:
			int level=int.Parse(args["level"].ToString());
			
			// Building:
			Building building=Buildings.Get(args["building"].ToString());
			
			// Get building info in the village:
			BuildingLevelInfo info=village.GetBuilding(building.RefName);
			
			info.Level=level;
			
		}
		
	}
	
}