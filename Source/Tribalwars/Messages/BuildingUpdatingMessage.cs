using System;
using Wrench;


namespace Tribalwars{
	
	/// <summary>
	/// Used when a building gets updated.
	/// </summary>
	
	public class BuildingUpdatingMessage : MessageHandler{
		
		public override string Type{
			get{
				return "Building/upgrading";
			}
		}
		
		public override void Handle(JSObject args,GameClient client){
			
			// Village ID:
			int villageID=int.Parse(args["village_id"].ToString());
			
			// Get the village.
			Village village=client.Account.GetVillage(villageID);
			
			if(village==null){
				return;
			}
			
			// Job info:
			JSObject job=args["job"];
			
			// Building level:
			int level=int.Parse(job["level"].ToString());
			
			// Building:
			Building building=Buildings.Get(job["building"].ToString());
			
			// Completion time:
			uint timeComplete=uint.Parse(job["time_completed"].ToString());
			
			// Started at:
			uint start=uint.Parse(job["time_started"].ToString());
			
			// Update the queue entry:
			BuildQueueEntry bqe=village.UpdateQueueEntry(building,level,timeComplete,start);
			
			// It's in progress:
			bqe.InProgress=true;
			
			// Shuffle the queue - if bqe is after anything, it should swap places if those entries are not in progress.
			// This doesnt make much of a difference other than keeping the queue tidy.
			
			QueueEntry current=bqe.Previous;
			
			while(current!=null){
				
				BuildQueueEntry entry=current as BuildQueueEntry;
				
				
				if(entry==null || !entry.InProgress){
					
					// "push" it upwards so it's the first thing after bqe.
					// We'll do this by removing then doing an insert after.
					current.Remove();
					
					village.BuildingQueue.InsertAfter(current,bqe);
					
				}
				
				current=current.Previous;
			}
			
			// Update all queue times:
			village.RecomputeBuildTimes();
			
			// Refresh the queue waiter:
			village.AwaitBuildQueueDone();
			
		}
		
	}
	
}