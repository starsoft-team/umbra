using System;
using Wrench;


namespace Tribalwars{
	
	public class WelcomeMessage : MessageHandler{
		
		public override string Type{
			get{
				return "System/welcome";
			}
		}
		
		public override void Handle(JSObject args,GameClient client){
			
			// Auth now:
			client.Send("Authentication/login","{\"name\":\""+client.Account.Username+"\",\"token\":\""+client.Account.Token+"\"}");
			
		}
		
	}
	
}