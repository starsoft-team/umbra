using System;
using Wrench;
using System.Collections;
using System.Collections.Generic;


namespace Tribalwars{
	
	public class MerchantStatusMessage : MessageHandler{
		
		public override string Type{
			get{
				return "Trading/merchantStatus";
			}
		}
		
		public override void Handle(JSObject args,GameClient client){
			
			// Get village ID:
			int villageID=int.Parse(args["village_id"].ToString());
			
			// Get village:
			Village village=client.Account.GetVillage(villageID);
			
			// Update village merchants:
			village.Merchants=new MerchantInfo(
				village,
				int.Parse(args["free"].ToString()),
				int.Parse(args["busy"].ToString())
			);
			
		}
		
	}
	
}