using System;
using Wrench;


namespace Tribalwars{
	
	/// <summary>
	/// New item in inv.
	/// </summary>
	
	public class PremiumItemMessage : MessageHandler{
		
		public override string Type{
			get{
				return "Premium/itemChange";
			}
		}
		
		public override void Handle(JSObject args,GameClient client){
			
		}
		
	}
	
}