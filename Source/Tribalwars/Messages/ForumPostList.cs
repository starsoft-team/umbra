using System;
using Wrench;


namespace Tribalwars{
	
	public class ForumPostList : MessageHandler{
		
		public override string Type{
			get{
				return "Forum/postList";
			}
		}
		
		public override void Handle(JSObject args,GameClient client){
			
			// Thread ID:
			int threadID=int.Parse(args["thread_id"].ToString());
			
			// Posts:
			JSArray posts=args["posts"] as JSArray;
			
			// Total # of posts:
			int totalPosts=int.Parse(args["total"].ToString());
			
			// Run the callback(s):
			client.Account.RunCallback("Forum/postList/"+threadID,posts,totalPosts);
			
		}
		
	}
	
}