using System;
using Wrench;


namespace Tribalwars{
	
	public class ArmyCancelledMessage : MessageHandler{
		
		public override string Type{
			get{
				return "Command/cancelled";
			}
		}
		
		public override void Handle(JSObject args,GameClient client){
			
		}
		
	}
	
}