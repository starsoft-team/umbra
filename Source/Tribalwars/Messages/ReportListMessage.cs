using System;
using System.Collections;
using System.Collections.Generic;
using Wrench;


namespace Tribalwars{
	
	public class ReportListMessage : MessageHandler{
		
		public override string Type{
			get{
				return "Report/list";
			}
		}
		
		public override void Handle(JSObject args,GameClient client){
			
			// Load:
			client.Account.Reports=ReportSet.Load(args,client.Account);
			
			// Run the onreports event:
			client.Account.triggerEvent("onreports");
			
		}
		
	}
	
}