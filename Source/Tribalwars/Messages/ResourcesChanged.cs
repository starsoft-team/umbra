using System;
using Wrench;


namespace Tribalwars{
	
	public class ResourcesChangedMessage : MessageHandler{
		
		public override string Type{
			get{
				return "Village/resourcesChanged";
			}
		}
		
		public override void Handle(JSObject args,GameClient client){
			
			JSObject vID=args["villageId"];
			
			if(vID==null){
				vID=args["village_id"];
				
				if(vID==null){
					return;
				}
				
			}
			
			// Which village?
			int id=int.Parse(vID.ToString());
			
			// Get that village:
			Village village=client.Account.GetVillage(id);
			
			if(village==null){
				return;
			}
			
			// Update it's resources:
			village.UpdateResources(args);
			
		}
		
	}
	
}