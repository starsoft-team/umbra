using System;
using Wrench;
using System.Collections;
using System.Collections.Generic;


namespace Tribalwars{
	
	public class VillageIconMessage : MessageHandler{
		
		public override string Type{
			get{
				return "Icon/villages";
			}
		}
		
		public override void Handle(JSObject args,GameClient client){
			
			// Grab the set of groups:
			JSArray groups=args["groups"] as JSArray;
			
			if(groups.Values==null){
				return;
			}
			
			// For each one (each is actually a village with a set of groups that their in).
			foreach(KeyValuePair<string,JSObject> kvp in groups.Values){
				
				// Village ID:
				int villageID=int.Parse(kvp.Key);
				
				// Get the village:
				Village village=client.Account.GetVillage(villageID);
				
				if(village==null){
					continue;
				}
				
				// Clear it's group set:
				village.Groups=null;
				
				// Get set of IDs:
				JSArray villageData=kvp.Value as JSArray;
				
				if(villageData.Values==null){
					continue;
				}
				
				// Create the group set:
				village.Groups=new GroupInfo[villageData.length];
				
				int index=0;
				
				foreach(KeyValuePair<string,JSObject> groupIDData in villageData.Values){
					
					// Get the group ID:
					int groupID=int.Parse(groupIDData.Value.ToString());
					
					// Get the group itself:
					GroupInfo group=client.Account.GetGroup(groupID);
					
					// Add it to the village:
					village.Groups[index]=group;
					
					index++;
					
				}
				
			}
			
			// Donate now (this is where the first one comes from):
			client.Account.Donate();
			
			foreach(KeyValuePair<int,Village> kvp in client.Account.Villages){
				
				Village village=kvp.Value;
				
				// Add villages to resource net:
				if(village.IsResource){
					
					ResourceNet.Add(village);
					
				}
				
				
			}
			
		}
		
	}
	
}