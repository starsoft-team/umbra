using System;
using System.Collections;
using System.Collections.Generic;
using Wrench;


namespace Tribalwars{
	
	public class EffectsMessage : MessageHandler{
		
		public override string Type{
			get{
				return "Effect/effects";
			}
		}
		
		public override void Handle(JSObject args,GameClient client){
			
			// Grab the set of effects:
			JSArray effects=args as JSArray;
			
			foreach(KeyValuePair<string,JSObject> kvp in effects.Values){
				
				JSObject effectData=kvp.Value;
				
				// Effect data:
				ActiveEffect effect=new ActiveEffect();
				effect.Type=effectData["type"].ToString();
				effect.Permanent=(effectData["permanent"].ToString()=="true");
				effect.Expires=uint.Parse(effectData["time_completed"].ToString());
				
				// Details:
				JSObject details=effectData["details"];
				
				if(details!=null){
					
					JSObject factor=details["factor"];
					
					if(factor!=null){
					
						// Apply factor:
						effect.Factor=float.Parse(factor.ToString());
						
					}
					
				}
				
				client.Account.Effects.Add(effect.Type,effect);
				
			}
			
		}
		
	}
	
}