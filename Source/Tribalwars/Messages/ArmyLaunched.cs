using System;
using Wrench;


namespace Tribalwars{
	
	public class ArmyLaunchMessage : MessageHandler{
		
		public override string Type{
			get{
				return "Command/sent";
			}
		}
		
		public override void Handle(JSObject args,GameClient client){
			
			// Which village?
			int id=int.Parse(args["home"]["id"].ToString());
			
			// time?
			uint time=uint.Parse(args["time_completed"].ToString());
			
			string type=(args["type"].ToString().ToLower());
			
			if(args["direction"].ToString()=="back"){
				// Ignore returns.
				return;
			}
			
			if(type!="attack" && type!="support"){
				return;
			}
			
			// Get that village:
			Village village=client.Account.GetVillage(id);
			
			string dateStr=Time.GetDate(time).ToString("HH:mm:ss");
			
			int targetID=int.Parse(args["target"]["id"].ToString());
			int targetX=int.Parse(args["target"]["x"].ToString());
			int targetY=int.Parse(args["target"]["y"].ToString());
			
			Village toVillage=new Village(targetX,targetY,args["target"]["name"].ToString());
			toVillage.ID=targetID;
			
			string msg=type+" from "+village.ToForumBB()+" to "+toVillage.ToForumBB()+" sent! Landing at "+dateStr+".";
			
			Log.Add("ARMY SENT! "+msg);
			
			Forum.Post(client.Account,12016,msg);
			
		}
		
	}
	
}