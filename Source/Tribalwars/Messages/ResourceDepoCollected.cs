using System;
using System.Collections;
using System.Collections.Generic;
using Wrench;


namespace Tribalwars{
	
	public class ResourceDepoCollected : MessageHandler{
		
		public override string Type{
			get{
				return "ResourceDeposit/collected";
			}
		}
		
		public override void Handle(JSObject args,GameClient client){
			
			ResourceDeposit.OnCollected(args,client);
			
		}
		
	}
	
}