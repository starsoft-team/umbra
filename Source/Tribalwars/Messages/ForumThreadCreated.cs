using System;
using Wrench;


namespace Tribalwars{
	
	public class ForumThreadCreated : MessageHandler{
		
		public override string Type{
			get{
				return "Forum/threadCreated";
			}
		}
		
		public override void Handle(JSObject args,GameClient client){
			
			// Grab the thread data:
			args=args["thread"];
			
			// Board ID:
			int boardID=int.Parse(args["forum_id"].ToString());
			
			// Created:
			Forum.ThreadCreated(boardID,args,client.Account);
			
		}
		
	}
	
}