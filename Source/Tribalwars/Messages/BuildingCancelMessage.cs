using System;
using Wrench;


namespace Tribalwars{
	
	/// <summary>
	/// Used when a building gets updated.
	/// </summary>
	
	public class BuildingCancelMessage : MessageHandler{
		
		public override string Type{
			get{
				return "Building/jobCancelled";
			}
		}
		
		public override void Handle(JSObject args,GameClient client){
			
			// Village ID:
			int villageID=int.Parse(args["village_id"].ToString());
			
			// Get the village.
			Village village=client.Account.GetVillage(villageID);
			
			if(village==null){
				return;
			}
			
			// Job info:
			JSObject job=args["job"];
			
			// Building level:
			int level=int.Parse(job["level"].ToString());
			
			// Building:
			Building building=Buildings.Get(job["building"].ToString());
			
			// Get the entry:
			BuildQueueEntry entry=village.GetBuildQueueObject(building,level);
			
			if(entry!=null){
				
				// Remove the entry:
				entry.Remove();
				
				// Update all queue times:
				village.RecomputeBuildTimes();
				
			}
			
			// Refresh the queue waiter:
			village.AwaitBuildQueueDone();
			
		}
		
	}
	
}