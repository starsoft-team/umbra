using System;
using Wrench;


namespace Tribalwars{
	
	/// <summary>
	/// Used when a recruit job finishes.
	/// </summary>
	
	public class RecruitJobMessage : MessageHandler{
		
		public override string Type{
			get{
				return "Unit/recruitJobFinished";
			}
		}
		
		public override void Handle(JSObject args,GameClient client){
			
			// Village ID:
			int villageID=int.Parse(args["village_id"].ToString());
			
			// Get the village.
			Village village=client.Account.GetVillage(villageID);
			
			if(village==null){
				return;
			}
			
			// Update the recruit queue and units:
			village.GetUnitInfo();
			
		}
		
	}
	
}