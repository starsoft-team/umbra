using System;
using Wrench;


namespace Tribalwars{
	
	public class ArmyChangedMessage : MessageHandler{
		
		public override string Type{
			get{
				return "Village/armyChanged";
			}
		}
		
		public override void Handle(JSObject args,GameClient client){
			
			// Which village?
			int id=int.Parse(args["village_id"].ToString());
			
			// Get that village:
			Village village=client.Account.GetVillage(id);
			
			// Go get the army data:
			village.GetUnitInfo();
			
		}
		
	}
	
}