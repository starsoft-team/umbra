using System;
using Wrench;


namespace Tribalwars{
	
	public class BuildQueueMessage : MessageHandler{
		
		public override string Type{
			get{
				return "Building/queue";
			}
		}
		
		public override void Handle(JSObject args,GameClient client){
			
			// Village ID:
			int villageID=int.Parse(args["village_id"].ToString());
			
			// Get the village.
			Village village=client.Account.GetVillage(villageID);
			
			// Update the build queue:
			village.UpdateBuildQueue(args);
			
		}
		
	}
	
}