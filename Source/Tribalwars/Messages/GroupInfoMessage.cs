using System;
using Wrench;
using System.Collections;
using System.Collections.Generic;


namespace Tribalwars{
	
	public class GroupInfoMessage : MessageHandler{
		
		public override string Type{
			get{
				return "Group/groups";
			}
		}
		
		public override void Handle(JSObject args,GameClient client){
			
			client.Account.Groups=new Dictionary<int,GroupInfo>();
			
			// Grab the set of groups:
			JSArray groups=args["groups"] as JSArray;
			
			foreach(KeyValuePair<string,JSObject> kvp in groups.Values){
				
				// Get group data object:
				JSObject groupData=kvp.Value;
				
				// Create the group:
				GroupInfo group=new GroupInfo(client.Account,groupData);
				
				// Add it:
				client.Account.Groups[group.ID]=group;
				
			}
			
		}
		
	}
	
}