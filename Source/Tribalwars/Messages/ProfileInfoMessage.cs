using System;
using System.Collections;
using System.Collections.Generic;
using Wrench;


namespace Tribalwars{
	
	/// <summary>
	/// Info about a remote player.
	/// </summary>
	
	public class ProfileInfoMessage : MessageHandler{
		
		public override string Type{
			get{
				return "Character/profile";
			}
		}
		
		public override void Handle(JSObject args,GameClient client){
			
			// Get account obj:
			int id=int.Parse(args["character_id"].ToString());
			
			// Get:
			Account acc=Accounts.Get(id);
			
			if(acc==null){
				return;
			}
			
			acc.BashPointsOffense=int.Parse(args["bash_points_off"].ToString());
			acc.BashPointsDefense=int.Parse(args["bash_points_def"].ToString());
			
			JSObject tribeID=args["tribe_id"];
			
			if(tribeID!=null){
				
				string tribeIDString=tribeID.ToString();
				
				if(tribeIDString!=null){
				
					acc.Tribe=Tribes.GetOrCreate(int.Parse(tribeIDString),"");
					
				}
				
			}
			
			// Trigger profile event:
			acc.triggerEvent("onprofile");
			
		}
		
	}
	
}