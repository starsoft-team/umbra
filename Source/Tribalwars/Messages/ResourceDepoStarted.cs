using System;
using System.Collections;
using System.Collections.Generic;
using Wrench;


namespace Tribalwars{
	
	public class ResourceDepoStarted : MessageHandler{
		
		public override string Type{
			get{
				return "ResourceDeposit/jobStarted";
			}
		}
		
		public override void Handle(JSObject args,GameClient client){
			
			ResourceDeposit.OnStarted(args,client);
			
		}
		
	}
	
}