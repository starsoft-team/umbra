using System;
using Wrench;
using System.Collections;
using System.Collections.Generic;


namespace Tribalwars{
	
	public class MerchantReturnedMessage : MessageHandler{
		
		public override string Type{
			get{
				return "Transport/returned";
			}
		}
		
		public override void Handle(JSObject args,GameClient client){
			
			/*
			// This message only has transID in it (which village did it go from!):
			int transportID=int.Parse(args["transport_id"].ToString());
			
			*/
			
			// -> Need to update merchant count in the "from" village
			
		}
		
	}
	
}