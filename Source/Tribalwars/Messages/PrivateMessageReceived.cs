using System;
using Wrench;


namespace Tribalwars{
	
	public class PrivateMessageRecieved : MessageHandler{
		
		public override string Type{
			get{
				return "Message/new";
			}
		}
		
		public override void Handle(JSObject args,GameClient client){
			
			if(client.Account!=ResourceNet.MainAccount){
				return;
			}
			
			// Got a message! Handle it now.
			
			// Msg ID:
			int msgID=int.Parse(args["message_id"].ToString());
			
			// Title:
			// string title=args["title"].ToString();
			
			// Content:
			string content=args["message"]["content"].ToString();
			
			if(content.StartsWith("[br][br][quote=") || content.StartsWith("[quote=") || content.StartsWith("[br][quote=") || content.StartsWith("[br][br][br][quote=")){
				
				content=content.Replace("[/quote]","");
				
				int indexOf=content.IndexOf('q');
				
				content=content.Substring(indexOf);
				
				indexOf=content.IndexOf(']');
				
				content=content.Substring(indexOf);
				
			}
			
			// From:
			int from=int.Parse(args["message"]["character_id"].ToString());
			
			// Try and get the account - we might actually be in control of it:
			Account account=Accounts.Get(from);
			
			// Create the response handler - any output goes into this:
			PmResponseHandler response=new PmResponseHandler(msgID,from,client.Account);
			
			// Run the message on the umbra command line now!
			// Note that if account is null, Umbra isn't controlling this account. Only basic commands will be allowed.
			CommandLine.Run(content,response,account,null);
			
			// E.g. "show queue", "queue", "army", "analyse [report link]"
			
		}
		
	}
	
}