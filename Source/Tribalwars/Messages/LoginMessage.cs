using System;
using Wrench;
using System.Web;
using System.Collections;
using System.Collections.Generic;


namespace Tribalwars{
	
	public class LoginMessage : MessageHandler{
		
		public override string Type{
			get{
				return "Login/success";
			}
		}
		
		public override void Handle(JSObject args,GameClient client){
			
			JSArray chars=args["characters"] as JSArray;
			
			if(chars!=null){
				
				string idStr=client.Account.ID.ToString();
				
				string worldStr=World.Current.CodeName;
				
				foreach(KeyValuePair<string,JSObject> kvp in chars.Values){
					
					if(kvp.Value["character_id"].ToString()==idStr && kvp.Value["world_id"].ToString()==worldStr){
						
						client.Account.Username=kvp.Value["character_name"].ToString();
						
						Console.WriteLine("Name: "+client.Account.Username);
						
					}
					
				}
				
			}
			
			// ID and select the character:
			client.Send("System/identify","{\"platform\":\"browser\",\"device\":\""+HttpUtility.UrlEncode(HttpMain.Agent)+"\",\"api_version\":\"9.*.*\"}");
			
			client.Send("Authentication/selectCharacter","{\"id\":\""+client.Account.ID+"\",\"world_id\":\"en"+World.Current.ID+"\"}");
			
		}
		
	}
	
}