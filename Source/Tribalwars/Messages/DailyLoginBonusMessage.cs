using System;
using Wrench;


namespace Tribalwars{
	
	public class DailyLoginBonusMessage : MessageHandler{
		
		public override string Type{
			get{
				return "DailyLoginBonus/info";
			}
		}
		
		public override void Handle(JSObject args,GameClient client){
			
			if(args["reward_collected"].ToString()=="false"){
				
				Console.WriteLine("Collecting daily login reward..");
				
				client.Send("DailyLoginBonus/claimReward");
				
			}
			
		}
		
	}
	
}