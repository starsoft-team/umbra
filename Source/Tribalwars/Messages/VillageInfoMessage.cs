using System;
using Wrench;


namespace Tribalwars{
	
	public class VillageInfoMessage : MessageHandler{
		
		public override string Type{
			get{
				return "VillageBatch/villageData";
			}
		}
		
		public override void Handle(JSObject args,GameClient client){
			
			// Array of villages:
			client.Account.SetupVillages(args);
			
			// Got village info!
			client.Account.triggerEvent("onready");
			
			// Groups and village icons:
			client.Send("Icon/getVillages");
			
		}
		
	}
	
}