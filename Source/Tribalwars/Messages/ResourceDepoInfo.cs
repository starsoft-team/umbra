using System;
using System.Collections;
using System.Collections.Generic;
using Wrench;


namespace Tribalwars{
	
	public class ResourceDepoInfo : MessageHandler{
		
		public override string Type{
			get{
				return "ResourceDeposit/info";
			}
		}
		
		public override void Handle(JSObject args,GameClient client){
			
			ResourceDeposit.OnOpened(args,client);
			
		}
		
	}
	
}