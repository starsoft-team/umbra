using System;
using Wrench;


namespace Tribalwars{
	
	public class QuestProgressMessage : MessageHandler{
		
		public override string Type{
			get{
				return "Quest/progress";
			}
		}
		
		public override void Handle(JSObject args,GameClient client){
			
			Console.WriteLine("Quest progress in!");
			
		}
		
	}
	
}