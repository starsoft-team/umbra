using System;
using Wrench;


namespace Tribalwars{
	
	public class ArmyInfoMessage : MessageHandler{
		
		public override string Type{
			get{
				return "Village/unitInfo";
			}
		}
		
		public override void Handle(JSObject args,GameClient client){
			
			// Which village?
			int id=int.Parse(args["village_id"].ToString());
			
			// Get that village:
			Village village=client.Account.GetVillage(id);
			
			// Update it's army:
			village.UpdateUnits(args);
			
		}
		
	}
	
}