using System;
using Wrench;


namespace Tribalwars{
	
	public class ForumThreadList : MessageHandler{
		
		public override string Type{
			get{
				return "Forum/threadList";
			}
		}
		
		public override void Handle(JSObject args,GameClient client){
			
			// Board ID:
			int boardID=int.Parse(args["forum_id"].ToString());
			
			// Threads:
			JSArray threads=args["threads"] as JSArray;
			
			// Total # of threads:
			int totalThreads=int.Parse(args["total"].ToString());
			
			// Run the callback(s):
			client.Account.RunCallback("Forum/threadList/"+boardID,threads,totalThreads);
			
		}
		
	}
	
}