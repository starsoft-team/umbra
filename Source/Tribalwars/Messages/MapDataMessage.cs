using System;
using System.Collections;
using System.Collections.Generic;
using Wrench;


namespace Tribalwars{
	
	public class MapDataMessage : MessageHandler{
		
		public override string Type{
			get{
				return "Map/villageData";
			}
		}
		
		public override void Handle(JSObject args,GameClient client){
			
			// At least 1 village so we can figure out which region this response is even for.
			
			JSArray vills=args["villages"] as JSArray;
			
			if(vills==null || vills.Values==null){
				return;
			}
			
			MapSection section=null;
			
			foreach(KeyValuePair<string,JSObject> kvp in vills.Values){
				
				JSObject villageInfo=kvp.Value;
				
				// Village coords:
				int x=int.Parse(villageInfo["x"].ToString());
				int y=int.Parse(villageInfo["y"].ToString());
				
				// Village ID:
				int villageID=int.Parse(villageInfo["id"].ToString());
				
				// Owner name:
				JSObject ownerIDObj=villageInfo["character_id"];
				string owner;
				int ownerID;
				
				if(ownerIDObj==null){
					
					// Barb village!
					owner=null;
					ownerID=0;
					
				}else{
					
					string idString=ownerIDObj.ToString();
					
					if(idString==null){
						
						// ..also barb village!
						owner=null;
						ownerID=0;
					
					}else{
						
						owner=villageInfo["character_name"].ToString();
						ownerID=int.Parse(idString);
						
					}
					
				}
				
				// Get the section now:
				int mapX=x/25;
				int mapY=y/25;
				
				// Section code:
				uint code=(uint)(mapX<<16) | (uint)mapY;
				
				// Get it:
				
				if(section==null){
					
					if(!MapSection.All.TryGetValue(code,out section)){
						// This shouldn't happen.
						continue;
					}
					
				}
				
				
				// Get the owner account:
				Account account;
				
				if(ownerID==0){
					
					// Barbarian
					account=null;
					
				}else{
					
					account=Accounts.GetOrCreate(ownerID);
					account.Username=owner;
					
				}
				
				// int tribeID=int.Parse(villageInfo["tribe_id"].ToString());
				// string tribeName=villageInfo["tribe_name"].ToString();
				
				// Create the village:
				Village village=new Village(account,World.Current,villageID,x,y);
				
				// Points:
				village.Points=int.Parse(villageInfo["points"].ToString());
				
				// Name:
				village.Name=villageInfo["name"].ToString();
				
				// Add it:
				section.Add(village);
				
			}
			
			if(section!=null){
				
				section.Loaded();
				
			}
			
		}
		
	}
	
}