using System;
using Wrench;
using System.Collections;
using System.Collections.Generic;


namespace Tribalwars{
	
	public class GroupUnlinkedMessage : MessageHandler{
		
		public override string Type{
			get{
				return "Group/unlinked";
			}
		}
		
		public override void Handle(JSObject args,GameClient client){
			
			Account acc=client.Account;
			
			int vID=int.Parse(args["village_id"].ToString());
			int gID=int.Parse(args["group_id"].ToString());
			
			// Get the village:
			Village village=acc.GetVillage(vID);
			
			village.RemoveGroupNow(gID);
			
		}
		
	}
	
}