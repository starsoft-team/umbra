using System;
using System.Collections;
using System.Collections.Generic;
using Wrench;


namespace Tribalwars{
	
	public class CharInfoMessage : MessageHandler{
		
		public override string Type{
			get{
				return "Character/info";
			}
		}
		
		public override void Handle(JSObject args,GameClient client){
			
			// Other info includes e.g. gold_coins, tribe_id, rank, points, game_over etc.
			if(args["game_over"].ToString()!="false"){
				Console.WriteLine("Ohno! This account has been attacked or something and no longer has villages.");
				return;
			}
			
			// Grab the set of village IDs:
			JSArray villages=args["villages"] as JSArray;
			
			string villageIDs="";
			
			foreach(KeyValuePair<string,JSObject> kvp in villages.Values){
				
				// id, name, x, y. Only using id here.
				
				string villageID=kvp.Value["id"].ToString();
				
				if(villageIDs==""){
					villageIDs=villageID;
				}else{
					villageIDs+=","+villageID;
				}
				
			}
			
			if(villageIDs==""){
				return;
			}
			
			client.Send("Group/getGroups");
			
			client.Send("VillageBatch/getVillageData","{\"village_ids\":["+villageIDs+"]}");
			
			// Get tribe etc effects:
			client.Send("Effect/getEffects");
			
		}
		
	}
	
}