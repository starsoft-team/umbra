using System;
using Wrench;


namespace Tribalwars{
	
	public class PrivateMessageSent : MessageHandler{
		
		public override string Type{
			get{
				return "Message/sent";
			}
		}
		
		public override void Handle(JSObject args,GameClient client){
			
		}
		
	}
	
}