using System;
using System.Collections;
using System.Collections.Generic;


namespace Tribalwars{
	
	/// <summary>
	/// A set of buildings that changed to match a certain amount of points gained.
	/// Usually will be just 1 building in the set (plus, figuring out more than 1 is an actual nightmare)
	/// </summary>
	
	public class LevelUpOption{
		
		/// <summary>The set of 1+ (usually 1) buildings that changed.</summary>
		public List<BuildingLevelInfo> Buildings=new List<BuildingLevelInfo>();
		
		
		public LevelUpOption(){}
		
		public LevelUpOption(BuildingLevelInfo levelInfo){
			
			// Add it:
			Buildings.Add(levelInfo);
			
		}
		
		public void Add(BuildingLevelInfo levelInfo){
			
			// Add it:
			Buildings.Add(levelInfo);
			
		}
		
		public override string ToString(){
			
			string builds="";
			
			foreach(BuildingLevelInfo bli in Buildings){
				
				if(builds!=""){
					builds+=", ";
				}
				
				builds+=bli.ToString();
				
			}
			
			return "Option: "+builds;
			
		}
		
	}
	
}