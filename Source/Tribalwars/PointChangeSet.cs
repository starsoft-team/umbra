using System;
using System.Collections;
using System.Collections.Generic;

namespace Tribalwars{

	/// <summary>
	/// Used to watch player point levels and figure out what they might have upgraded when the points change.
	/// </summary>

	public static class PointChangeSet{
		
		/// <summary>The set of points for each level for each building.
		public static int[][] PointMatrix;
		/// <summary>The point change and the set of possible buildings that caused it.</summary>
		public static Dictionary<int,List<BuildingLevelInfo>> Changes;
		
		
		private static void Setup(){
			
			if(Changes!=null){
				return;
			}
			
			Changes=new Dictionary<int,List<BuildingLevelInfo>>();
			
			// For each level of each building..
			PointMatrix=new int[Buildings.All.Count][];
			
			int matrixIndex=0;
			
			foreach(KeyValuePair<string,Building> kvp in Buildings.All){
				
				Building building=kvp.Value;
				
				int previousPoints=0;
				
				// Create a block:
				int[] pointBlock=new int[building.MaxLevel];
				
				for(int i=1;i<=building.MaxLevel;i++){
					
					// Get points:
					int points=building.GetPoints(i);
					
					// Get point change:
					int delta=points-previousPoints;
					
					// Update block:
					pointBlock[i-1]=delta;
					
					// Create level info:
					BuildingLevelInfo info=new BuildingLevelInfo(building,i);
					
					// Add to changes:
					List<BuildingLevelInfo> set;
					
					if(!Changes.TryGetValue(delta,out set)){
						
						// Create the set:
						set=new List<BuildingLevelInfo>();
						
						// Add to changes:
						Changes[delta]=set;
						
					}
					
					// Add to set:
					set.Add(info);
					
					// Update prev:
					previousPoints=points;
					
				}
				
				// Put the block into the matrix:
				PointMatrix[matrixIndex]=pointBlock;
				matrixIndex++;
				
			}
			
		}
		
		public static string ToForumBB(List<LevelUpOption> options){
			
			if(options==null){
				return "No options found. This probably means more than one building levelled up (not fully supported).";
			}
			
			string result="";
			
			foreach(LevelUpOption option in options){
				result+=option.ToString()+"[br]";
			}
			
			return result;
			
		}
		
		public static List<LevelUpOption> Changed(int pointsChangedBy){
			
			return Changed(pointsChangedBy,null);
			
		}
		
		public static List<LevelUpOption> Changed(int pointsChangedBy,BuildingLevels previous){
			
			if(Changes==null){
				Setup();
			}
			
			// Results set:
			List<LevelUpOption> possibles=null;
			
			// Get the potential options:
			List<BuildingLevelInfo> options;
			if(Changes.TryGetValue(pointsChangedBy,out options)){
				
				// Could be any of these options.
				foreach(BuildingLevelInfo chg in options){
					
					if(previous==null){
						
						// Yep - this is a possibility.
						if(possibles==null){
							
							// Create the options:
							possibles=new List<LevelUpOption>();
							
						}
						
						// Add it as a possibility (only 1 building change - most common):
						possibles.Add(new LevelUpOption(chg));
						
					}else{
						
						// Get the previous level for this building:
						int previousLevel=previous[chg.Building.RefName];
						
						// Does the change equal prev+1?
						if(chg.Level==previousLevel+1){
							
							// Yep - this is a possibility.
							if(possibles==null){
								
								// Create the options:
								possibles=new List<LevelUpOption>();
								
							}
							
							// Add it as a possibility (only 1 building change - most common):
							possibles.Add(new LevelUpOption(chg));
							
						}
						
					}
					
				}
				
			}
			
			/*
			// May have updated multiple buildings.
			// Try and figure out all combinations of buildings that adds to "pointsChangedBy".
			// Yikes.
			
			int matrixIndex=0;
			
			foreach(KeyValuePair<string,Building> kvp in Buildings.All){
				
				// Get the block of points:
				int[] pointBlock=PointMatrix[matrixIndex];
			
				// Get the previous level for this building:
				int previousLevel=previous[kvp.Value.RefName].Level;
				
				int max=kvp.Value.MaxLevel;
				
				// Find how many levels if any "fit" into points changed by.
				int maxIndex=max-1;
				
				for(int i=previousLevel;i<max;i++){
					
					// Get the point change:
					int points=pointBlock[i];
					
					if(points>pointsChangedBy){
						maxIndex=previousLevel;
						break;
					}
					
				}
				
				// previousLevel->maxIndex.
				if(previousLevel!=maxIndex){
					
					// There's 1 or more levels which have points that could be added together
					// to form our final value.
					// However, the question is, which ones actually do!
					
				}
				
				matrixIndex++;
				
			}
			*/
			
			return possibles;
			
		}
		
	}
	
}