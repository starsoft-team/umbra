using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using Wrench;


namespace Tribalwars{
	
	public class Research{
		
		public static void Setup(){
			
			JSArray rs=JSON.Parse(File.ReadAllText("tw-research-info.txt")) as JSArray;
			
			foreach(KeyValuePair<string,JSObject> kvp in rs.Values){
				
				string name=kvp.Key;
				JSObject value=kvp.Value;
				
				Building building=Buildings.Get(value["building"].ToString());
				
				building.AddResearch(name,value);
				
			}
			
		}
		
		public string Name;
		public float Function;
		public Building Building;
		public JSObject RawData;
		
		
		public Research(Building building,string name,JSObject data){
			Name=name;
			RawData=data;
			
			Function=GetFloat("function");
			
		}
		
		public JSObject this[string property]{
			get{
				return RawData[property];
			}
		}
		
		public string GetString(string property){
			JSObject val=RawData[property];
			
			if(val==null){
				return null;
			}
			
			return val.ToString();
		}
		
		public float GetFloat(string property){
			
			string value=GetString(property);
			
			if(value==null){
				return 0f;
			}
			
			return float.Parse(value);
		}
		
	}
	
}