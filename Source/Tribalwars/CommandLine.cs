using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;


namespace Tribalwars{

	public static class CommandLine{
		
		public static bool Verbose;
		/// <summary>Output goes here if it's not sent elsewhere.</summary>
		public static ResponseHandler DefaultOutput=new ConsoleHandler();
		
		
		public static void Run(string line,ResponseHandler response,Village village){
			
			if(village==null){
				Run(line,response,null,null);
				return;
			}
			
			Run(line,response,village.Account,village);
		}
		
		public static void Run(string line,ResponseHandler response,Account account,Village village){
			
			if(Verbose){
				Console.WriteLine(line);
			}
			
			if(line==null){
				return;
			}
			
			line=line.Trim();
			
			if(line==""){
				return;
			}
			
			if(response==null){
				response=DefaultOutput;
			}
			
			if(account!=null && account.CommandContext==null){
				
				// Create the commandline context now:
				account.CommandContext=new TwCommandContext();
				
			}
			
			if(village==null && account!=null){
				
				if(account.CommandContext!=null){
					
					// Grab the village:
					village=account.CommandContext.Village;
					
				}
				
			}
			
			int firstSpace=line.IndexOf(' ');
			
			string command=line;
			
			if(firstSpace!=-1){
				command=command.Substring(0,firstSpace);
			}
			
			command=command.ToLower();
			
			// Try and get a handler:
			CommandHandler handler=CommandHandler.Get(command);
			
			if(handler!=null){
				
				if(handler.AccountRequired && account==null){
					
					return;
					
				}
				
				handler.Handle(line,response,account,village);
				
				return;
				
			}
			
		}
		
	}
	
}