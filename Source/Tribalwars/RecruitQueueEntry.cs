using System;
using Wrench;


namespace Tribalwars{
	
	public class RecruitQueueEntry : QueueEntry{
		
		public int Count;
		/// <summary>The current unit count in a particular village.</summary>
		public CombatUnits UnitInstance;
		
		
		public UnitData Unit{
			get{
				return UnitInstance.UnitData;
			}
		}
		
		public RecruitQueueEntry(CombatUnits units,int count,uint start,uint end){
			
			UnitInstance=units;
			Count=count;
			InProgress=true;
			
			// Gets ticked when the recruit is actually done.
			
			Duration=(int)(end-start) * 1000;
			TimeCompletes=(long)end * 1000;
			
		}
		
		public override string ToString(){
			
			return "recruit "+Count+" "+Unit.RefName+" (completes in "+((TimeCompletes-Time.NowMilliseconds)/1000)+"s)";
			
		}
		
		public RecruitQueueEntry(CombatUnits units,int count,Village village){
			
			UnitInstance=units;
			Count=count;
			
			// time in seconds:
			int seconds = (int)( count * Unit.RecruitTime(village) );
			
			// Duration is in ms:
			Duration=seconds * 1000;
			
			// Get the current end of the RQ:
			long time = village.RecruitQueueEnd;
			
			if(time == 0)
				time = Time.NowMilliseconds;

			TimeCompletes = time + Duration;
			
		}
		
		public LevelCost Cost{
			get{
				
				// Get requirements for this level:
				return null;//Unit.GetCost(Count,UnitInstance.Village);
				
			}
		}
		
		/// <summary>Millisecond time when this entry is actually possible. MaxValue if it isn't at the moment 
		/// (e.g. because the warehouse isn't big enough, or not enough people).</summary>
		public long TimeWhenPossible(out Reason notPossibleReason){
			
			// Get the village:
			Village village=UnitInstance.Village;
			
			// Get requirements for this level:
			LevelCost cost=Cost;
			
			// How long until all resources are satisfied? Note that it's in seconds.
			int availableIn=cost.AvailableIn(village,out notPossibleReason);
			
			if(availableIn==int.MaxValue){
				
				// Not possible!
				return long.MaxValue;
				
			}
			
			return Time.NowMilliseconds;
			
		}
		
		public override string ToCommand(){
			
			return "recruit "+Count+" "+Unit.RefName;
			
		}
		
		public void OfficiallyQueue(){
		
			if(InProgress){
				return;
			}
			
			InProgress=true;
			
			Console.WriteLine("Recruiting "+Unit.RefName+"..");
			
			// Enqueue count x Unit.RefName:
			Village vill=UnitInstance.Village;
			
			vill.Client.Send("Barracks/recruit","{\"village_id\":"+vill.ID+",\"unit_type\":\""+Unit.RefName+"\",\"amount\":"+Count+"}");
			
		}
		
	}
	
}