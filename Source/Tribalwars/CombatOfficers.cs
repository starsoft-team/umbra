using System;
using Wrench;


namespace Tribalwars{
	
	public class CombatOfficers{
		
		public string UnitName; // e.g. loot_mater
		public int Count=1; // # of these units (usually 1)
		
		
		public CombatOfficers(string unit){
			
			UnitName=unit;
			Count=1;
			
		}
		
		public CombatOfficers(string unit,int count){
			
			UnitName=unit;
			Count=count;
			
		}
		
	}
	
}