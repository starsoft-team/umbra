using System;
using System.IO;


namespace Tribalwars{

	public static class QueueLoader{
		
		public static void Load(Village village,string file){
			
			if(!File.Exists(file)){
			
				return;
			}
			
			string[] contents=File.ReadAllLines(file);
			
			for(int i=0;i<contents.Length;i++){
				
				string line=contents[i];
				
				CommandLine.Run(line,null,village);
				
			}
			
		}
		
	}

}