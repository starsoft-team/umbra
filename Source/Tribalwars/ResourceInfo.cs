using System;
using System.Collections;
using System.Collections.Generic;


namespace Tribalwars{
	
	public class ResourceInfo{
		
		public string Name;
		public int Count;
		/// <summary>Rate in res/sec.</summary>
		public float Rate=0f;
		public Village Village;
		
		
		public ResourceInfo(string name,Village village){
			Name=name;
			Village=village;
		}
		
		public void SetRate(float perHour){
			
			Rate=perHour / 3600f;
			
		}
		
		/// <summary>The # of seconds that the given res count will be available in.</summary>
		/// <returns>MaxValue if the count is never available because it's more than the storage capacity of the village.</summary>
		public int AvailableIn(int available){
			
			if(available>Village.Storage){
				return int.MaxValue;
			}
			
			if(Count>=available){
				
				// Available now!:
				return 0;
				
			}
			
			// Offset to how many we actually need:
			available-=Count;
			
			// We gain 'Rate' of them per second. How many seconds until 'available' is, uh, available?
			
			return (int)Math.Ceiling( (float)available / Rate);
			
		}
		
		/// <summary>The count available at the given time.
		/// Based off the approx rate; doesn't take farming or queues into account.</summary>
		public int CountAt(uint time){
			
			int deltaTime=(int)(time - Village.ResourcesLastTime);
			
			int total=Count + (int)( (float)deltaTime * Rate );
			
			if(total>Village.Storage){
				return Village.Storage;
			}
			
			return total;
			
		}
		
		public int CountNow{
			get{
				
				return CountAt(Time.Now);
				
			}
		}
		
	}
	
}