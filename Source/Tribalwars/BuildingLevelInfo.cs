using System;
using System.Collections;
using System.Collections.Generic;
using Wrench;

namespace Tribalwars{

	public class BuildingLevelInfo{
		
		public int Level;
		public Village Village;
		public Building Building;
		public Dictionary<string,ResearchInfo> Researches;
		
		
		public BuildingLevelInfo(Building building,int level){
			
			Building=building;
			Level=level;
			
		}
		
		public BuildingLevelInfo(string buildingName,int level){
			
			Building=Buildings.Get(buildingName);
			Level=level;
			
		}
		
		public BuildingLevelInfo(Village village,Building building){
			
			Village=village;
			Building=building;
			
		}
		
		/// <summary>Total # of points gained by this level.</summary>
		public int TotalPoints{
			get{
				
				LevelCost cost=Building.GetCost(Level,null);
				
				return cost.TotalPoints;
				
			}
		}

		/// <summary>Total # of pop required for this level.</summary>
		public int TotalPopulation{
			get{
				
				if(Level==0){
					return 0;
				}
				
				int food=Building.TotalFood(Level);
				
				return food;
				
			}
		}
		
		public int TimeToLevel(int level){
			return Building.GetBuildTime(level,Village);
		}
		
		/// <summary>The time until the next level.</summary>
		public int TimeToNextLevel{
			get{
				
				return Building.GetBuildTime(Level+1,Village);
				
			}
		}
		
		public int NextComputedLevel{
			get{
				
				// Get the current max queue level for this building:
				int queuedLevel=Village.GetQueuedLevel(Building);
				
				return queuedLevel+1;
				
			}
		}
		
		public void Upgrade(){
			
			// Get the current max queue level for this building:
			int nextLevel=NextComputedLevel;
			
			// Create the entry:
			BuildQueueEntry queueEntry=new BuildQueueEntry(this,nextLevel,Village);
			
			// Queue it now:
			Village.BuildingQueue.Add(queueEntry);
			
		}
		
		public void UpgradeNow(){
			
			Village.Client.Send("Building/upgrade","{\"building\":\""+Building.RefName+"\",\"village_id\":"+Village.ID+",\"location\":\"hq\",\"premium\":false}");
			
		}
		
		public override string ToString(){
			
			return Building.RefName+"="+Level;
			
		}
		
	}
	
}