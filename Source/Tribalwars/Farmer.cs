using System;
using System.Collections;
using System.Collections.Generic;


namespace Tribalwars{

	public class Farmer{
		
		public int RemainingCapacity;
		public Village Village;
		public int Capacity;
		/// <summary>The set of assigned farmable villages from this one.</summary>
		public List<Village> FarmableVillages=new List<Village>();
		
		
		public Farmer(Village village,int capacity){
			
			Village=village;
			Capacity=capacity;
			
		}
		
		public void Add(Village farm){
			
			FarmableVillages.Add(farm);
			
		}
		
		public void FarmNow(){
			
			foreach(Village village in FarmableVillages){
				
				// Units to send:
				List<CombatUnits> units=new List<CombatUnits>();
				
				// Send the slowest unit groups first as the first in the set are the nearest villages.
				#warning add to units here!
				
				// Send the attack now!
				// Village.LaunchAttack(village.ID,units);
				
			}
			
		}
		
	}
	
}