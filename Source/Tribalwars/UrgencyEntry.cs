using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;


namespace Tribalwars{
	
	public class UrgencyEntry{
		
		/// <summary>Speed+Urgency rank.</summary>
		public int Rank;
		/// <summary>Urgency. Higher means more urgent.</summary>
		public double Urgency;
		/// <summary>E.g. "clay_pit".</summary>
		public string Name;
		/// <summary>The time in seconds to fulfil.</summary>
		public long Duration;
		
		
		public UrgencyEntry(string name,long dur,double urg){
			Name=name;
			Duration=dur;
			Urgency=urg;
		}
		
	}
	
}