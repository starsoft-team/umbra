using System;
using System.Collections;
using System.Collections.Generic;


namespace Tribalwars{
	
	public static class Accounts{
		
		/// <summary>All currently loaded accs.</summary>
		public static Dictionary<int,Account> All=new Dictionary<int,Account>();
		
		
		public static Village GetVillage(int id){
			
			foreach(KeyValuePair<int,Account> kvp in All){
				
				Village result=kvp.Value.GetVillage(id);
				
				if(result!=null){
					
					return result;
					
				}
				
			}
			
			return null;
			
		}
		
		/// <summary>Get an account by it's ID.</summary>
		public static Account Get(int id){
			
			Account result;
			All.TryGetValue(id,out result);
			
			return result;
			
		}
		
		public static void ClearProfiles(){
			
			foreach(KeyValuePair<int,Account> kvp in All){
				
				kvp.Value.RequestedProfile=false;
				
			}
			
		}
		
		/// <summary>Get an account by it's ID (used for accounts that umbra can't log in to).</summary>
		public static Account GetOrCreate(int id){
			
			return GetOrCreate(id,false);
			
		}
		
		/// <summary>Get an account by it's ID.</summary>
		public static Account GetOrCreate(int id,bool local){
			
			Account result;
			if(!All.TryGetValue(id,out result)){
				
				result=new Account(id,local);
				
			}
			
			return result;
			
		}
		
		public static void Add(Account account){
			
			All[account.ID]=account;
			
		}
		
		public static void Remove(Account account){
			
			All.Remove(account.ID);
			
		}
		
	}
	
}
