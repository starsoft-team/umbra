using System;
using System.Collections;
using System.Collections.Generic;
using Wrench;


namespace Tribalwars{
	
	public class AttackReportContent:ReportContent{
		
		public UnitsSet AttackerUnits;
		public UnitsSet AttackerLosses;
		
		public UnitsSet DefenderUnits;
		public UnitsSet DefenderLosses;
		
	}
	
}