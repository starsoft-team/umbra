using System;
using Wrench;
using System.IO;
using System.Collections;
using System.Collections.Generic;


namespace Tribalwars{

	public static class Officers{
		
		public static Dictionary<string,OfficerData> All;
		
		
		public static OfficerData Get(string name){
			
			if(All==null){
				Setup();
			}
			
			OfficerData result;
			All.TryGetValue(name,out result);
			
			return result;
			
		}
		
		/// <summary>Called by Units.Setup.</summary>
		public static void Setup(){
			
			All=new Dictionary<string,OfficerData>();
			
			JSArray units=JSON.Parse(File.ReadAllText("tw-officer-info.txt")) as JSArray;
			
			foreach(KeyValuePair<string,JSObject> kvp in units.Values){
				
				OfficerData unit=new OfficerData(kvp.Key,kvp.Value);
				
				All[kvp.Key]=unit;
				
			}
			
		}
		
	}

}