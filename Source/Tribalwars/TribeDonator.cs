using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Wrench;
using PowerUI;

namespace Tribalwars
{	
	public static class TribeDonator
	{
		// Let's donate once every 12 hours
		public const int Minutes = 720;
		public const int Seconds = Minutes * 60;
		
		public static bool Active;
		private static System.Timers.Timer InternalTimer;
		
		public static bool Started
		{
			get { return InternalTimer != null; }
		}
		
		public static void Start()
		{
			if(Started)
				return;
			
			InternalTimer = new System.Timers.Timer();
			InternalTimer.Elapsed += Elapsed;
			InternalTimer.Interval = Seconds * 1000;
			InternalTimer.Start();
			
		}
		
		/// <summary>The method called when the system timer has waited for the specified interval.</summary>
		private static void Elapsed(object sender, System.Timers.ElapsedEventArgs e)
		{
			Donate();
		}
		
		// Example Message:
		//{"name":"msg","args":[{"type":"TribeSkill/donate","data":{"village_id":79331,"crowns":0,"resources":{"wood":700,"clay":600,"iron":700}},"id":44,"headers":{"traveltimes":[["browser_send",1444387127204]]}}]}
		// Example Response:
		//{"name":"msg","args":[{"id":44,"type":"TribeSkill/donated","headers":{"traveltimes":[["browser_send",1444387127204],["node_receive",1444387126486],["worker_start",1444387126493],["worker_response",1444387126509],["worker_deliver",1444387126509],["node_deliver",1444387126514]]},"data":{"level":3,"new_power":1259,"old_power":1239,"donor_id":727003,"donor_name":"TheProgrammer","honor":{"honor_total":1,"honor_this_week":1,"honor_last_week":0}}}]}
		
		public static void Donate()
		{
			Console.WriteLine("Donating Resources!");
			
			foreach(KeyValuePair<int,Account> kvp in Accounts.All){
				
				kvp.Value.Donate();
			
			}
			
		}
		
		public static int Floor(int num, int multiple)
		{
			return num - num % multiple;
		}
	}
}