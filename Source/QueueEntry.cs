using System;


public class QueueEntry{
	
	/// <summary>True if this entry is actually registered in the (ungodly short) official queues.</summary>
	public bool Official;
	/// <summary>Duration in ms.</summary>
	public int Duration;
	/// <summary>True if this is actually in progress (i.e. officially queued).</summary>
	public bool InProgress;
	/// <summary>The time this entry completes at.</summary>
	public long TimeCompletes;
	/// <summary>A "virtual" queue entry is purely for time management. It doesn't get written out to a file etc.
	/// E.g. if you dont have enough resources yet, a virtual block is added to the queue before it.</summary>
	public bool Virtual;
	public TimedQueue InQueue;
	public QueueEntry Next;
	public QueueEntry Previous;
	
	
	public void SetTime(long start,int duration){
		
		Duration=duration;
		TimeCompletes=start+duration;
		
	}
	
	public virtual void OnComplete(){
	
	}
	
	/// <summary>Gets this entry as a command line command string.</summary>
	public virtual string ToCommand(){
		
		return "";
		
	}
	
	public long TimeRemaining{
		get{
			
			long delta=TimeCompletes-Time.NowMilliseconds;
			
			if(delta<0){
				delta=0;
			}
			
			return delta;
			
		}
	}
	
	public void Cancel(){
		Remove();
	}
	
	/// <summary>Removes this entry from its queue.</summary>
	public void Remove(){
		
		if(InQueue==null){
			return;
		}
		
		if(Previous==null){
			
			InQueue.FirstEntry=Next;
			
		}else{
			
			Previous.Next=Next;
			
		}
		
		if(Next==null){
			
			InQueue.LastEntry=Previous;
			
		}else{
			
			Next.Previous=Previous;
			
		}
		
		InQueue=null;
		Previous=null;
		Next=null;
		
	}
	
}