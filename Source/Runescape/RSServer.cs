using System;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace Runescape{

	/// <summary>
	/// An active link to an RS server. One per Player (logged in user).
	/// </summary>

	public class RSServer:SocketLink{
		
		public Player Client;
		
		
		public RSServer(Player client){
			
			// Apply client:
			Client=client;
			
			// Grab world:
			World world=client.World;
			
			Socket=new Socket(AddressFamily.InterNetwork,SocketType.Stream,ProtocolType.Tcp);
			
			// Blocking mode:
			Socket.Blocking=true;
			
			// Connect now:
			try{
				
				Socket.Connect(world.IP,world.Port);
				
			}catch{
				
				Log.Add("Failed to connect to Runescape world "+world.ID);
				
			}
			
			if(Socket.Connected){
				Receive();
				Client.OnConnected();
			}
			
		}
		
		public override void Received(byte[] data,int offset,int index){
			
			Client.ReceivedFromRunescape(data,offset,index);
			
		}
		
	}
	
}