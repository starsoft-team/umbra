using System;
using System.Net;
using System.Net.Sockets;

namespace Runescape{

	public class World{
		
		public int ID;
		public int OldSchoolID;
		public bool OldSchool;
		public IPAddress IP;
		
		
		
		public string Dns{
			get{
				
				if(OldSchool){
					return "oldschool"+OldSchoolID+".runescape.com";
				}
				
				return "world"+ID+".runescape.com";
				
			}
		}
		
		/// <summary>JIC worlds are on different ports.</summary>
		public int Port{
			get{
				return Settings.Port;
			}
		}
		
	}
	
}