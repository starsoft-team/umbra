using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace Runescape{


	public static class Worlds{
		
		/// <summary>The raw world number -> world data lookup.</summary>
		public static Dictionary<int,World> Lookup;
		
		
		public static World Get(int id){
			
			World result;
			Lookup.TryGetValue(id,out result);
			
			return result;
		}
		
		public static void Load(){
			
			// Create:
			Lookup=new Dictionary<int,World>();
			
			// Parse the file now (could use RS world list - just use file for now):
			string[] worlds=File.ReadAllLines("world-info.txt");
			
			foreach(string line in worlds){
				
				string[] pieces=line.Split(',');
				
				int worldID=int.Parse(pieces[0]);
				
				// Create world data:
				World world=new World();
				world.ID=worldID;
				world.OldSchool=(pieces[1]=="oldschool");
				
				if(world.OldSchool){
					world.OldSchoolID=int.Parse(pieces[2]);
				}
				
				world.IP=IPAddress.Parse(pieces[3]);
				
				// Push to lookup:
				Lookup[worldID]=world;
				
			}
			
		}
		
	}
	
}