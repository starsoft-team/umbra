using System;
using System.Net;
using System.Net.Sockets;
using System.Collections;
using System.Collections.Generic;

namespace Runescape{

	/// <summary>
	/// Handles the client connecting locally. Used when in middleman/ passthrough mode.
	///
	/// [RS Client] -> [UMBRA.LocalServer] -> [UMBRA.LocalClient] -> [UMBRA.ServerLink] -> [RS Server]
	///
	/// </summary>

	public class LocalServer{
		
		/// <summary>The socket that this server listens on.</summary>
		public Socket ServerSocket;
		/// <summary>A straight list of local clients - will probably only be one.</summary>
		public List<LocalClient> Clients;
		/// <summary>The world this server will pass straight through to.</summary>
		public World World;
		
		
		public LocalServer(int world){
			
			// Get the world:
			World=Worlds.Get(world);
			
			// Create the socket:
			ServerSocket=new Socket(AddressFamily.InterNetwork,SocketType.Stream,ProtocolType.Tcp);
			ServerSocket.Bind(new IPEndPoint(IPAddress.Any,World.Port));
			ServerSocket.Blocking=false;
			ServerSocket.Listen(100);
			ServerSocket.BeginAccept(OnConnect,null);
			
			// Setup client set (I hate using lists like this lols, but it's only local anyways):
			Clients=new List<LocalClient>();
			
		}
		
		/// <summary>Called when an official RS client connects.
		/// This, in turn, connects to an RS server and then sits in the middle.</summary>
		private void OnConnect(IAsyncResult ar){
			if(ServerSocket==null){
				return;
			}
			
			Socket socket=ServerSocket.EndAccept(ar);
			ServerSocket.BeginAccept(OnConnect,null);
			socket.Blocking=true;
			
			// Log a connect message:
			Log.Add("Local client has connected. Forwarding now.");
			
			// Create a client:
			LocalClient client=new LocalClient(socket,World);
			
			// Add it:
			int id=Clients.Count;
			Clients.Add(client);
			
			// Apply ID:
			client.LocalID=id;
			
			// Start connecting it to RS:
			client.Player.ConnectToRunescape();
			
		}
		
	}
	
}