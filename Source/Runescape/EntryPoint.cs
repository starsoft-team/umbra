using System;
using System.IO;
using System.Threading;


namespace Runescape{


	public static class EntryPoint{
		
		/// <summary>A server used for official RS clients to connect to (in middleman mode).</summary>
		public static LocalServer LocalServer;
		
		
		public static void Start(){
			
			// Load world info:
			Worlds.Load();
			
			// World ID to use:
			int worldID=301;
			
			// Bring on the crazytime :)
			if(Arguments.IsSet("middleman")){
				
				// Middleman mode
				Log.Add("Starting in middleman mode.");
				
				// Set:
				Settings.Middleman=true;
				
				// Start for world 301:
				LocalServer=new LocalServer(worldID);
				
			}else{
				
				// Normal login mode (silent)
				
				//Player player=new Player(worldID,"MrFluffyNub","berryrun1");
				
				// Assuming all was ok, player should at this point be connected to RS and starting to do the RS handshake/ login stuff
				// (..if there is even a handshake lol)
				
			}
			
			// Halt the main thread:
			// HTTP proxy practically does this anyways.
			// Thread.Sleep(Timeout.Infinite);
			SimpleHttpProxy.HttpProxy.Start();
			
		}
		
	}
	
}