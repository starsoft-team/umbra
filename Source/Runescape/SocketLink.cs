using System;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace Runescape{

	/// <summary>
	/// Base class for both incoming connections (from the client, in middleman mode only) and outgoing (to the server) ones.
	/// </summary>

	public class SocketLink{
		
		/// <summary>True if a key is set.</summary>
		public bool GotKey;
		/// <summary>The session key.</summary>
		public long Key;
		/// <summary>The raw socket.</summary>
		public Socket Socket;
		/// <summary>A tiny buffer used for starting to receive data from the socket.
		/// This allows the main receive buffers to be pooled/shared for super light memory usage.</summary>
		public byte[] OpcodeReceiveBuffer=new byte[1];
		/// <summary>A larger pooled buffer for receiving main data from the socket.
		/// Future note: This should be .Return()'ed if the socket hasnt seen much activity 
		/// or if there's a chance it can be reused by some other client.</summary>
		private ByteBuffer ReceiveBuffer;
		
		public RSReader Reader;
		public RSWriter Writer;
		
		
		public SocketLink(Socket socket){
			Socket=socket;
			Receive();
		}
		
		public SocketLink(){}
		
		public void Receive(){
			
			try{
				Socket.BeginReceive(OpcodeReceiveBuffer,0,1,0,OnOpcodeReceived,this);
			}catch{
				Disconnect();
			}
			
		}
		
		public void SetKey(long key){
			Key=key;
			GotKey=true;
		}
		
		private void OnOpcodeReceived(IAsyncResult ar){
			int bytesRead;
			
			try{
				bytesRead=Socket.EndReceive(ar);
			}catch{
				bytesRead=0;
			}
			
			if(bytesRead==0){
				Disconnect();
				return;
			}
			
			// Received OpcodeReceiveBuffer[0]:
			Received(OpcodeReceiveBuffer,0,1);
			
			// Reset the link (this time going to OnDataReceived):
			if(ReceiveBuffer==null){
				ReceiveBuffer=BufferPool.Get();
			}
			
			try{
				Socket.BeginReceive(ReceiveBuffer.Bytes,0,ReceiveBuffer.Length,0,OnDataReceived,this);
			}catch{
				Disconnect();
			}
			
		}
		
		/// <summary>Received some or part of the data. Note that this might also contain opcodes.
		/// It's just a way of reducing overall memory usage.</summary>
		private void OnDataReceived(IAsyncResult ar){
			int bytesRead;
			
			try{
				bytesRead=Socket.EndReceive(ar);
			}catch{
				bytesRead=0;
			}
			
			if(bytesRead==0){
				Disconnect();
				return;
			}
			
			
			// Received ReceiveBuffer.Bytes:
			Received(ReceiveBuffer.Bytes,0,bytesRead);
			
			/*
			// Pooling stuff - unused atm
			// Reset the link (this time going to OnDataReceived again):
			if(ReceiveBuffer==null){
				ReceiveBuffer=BufferPool.Get();
			}
			*/
			
			try{
				Socket.BeginReceive(ReceiveBuffer.Bytes,0,ReceiveBuffer.Length,0,OnDataReceived,this);
			}catch{
				Disconnect();
			}
			
		}
		
		/// <summary>Called when this client has received some data.</summary>
		public virtual void Received(byte[] data,int offset,int length){}
		
		public virtual void Disconnect(){
			
			// Drop the socket:
			if(Socket!=null){
				Socket socket=Socket;
				Socket=null;
				
				try{
					if(socket.Connected){
						socket.Shutdown(SocketShutdown.Both);
					}
					socket.Close();
				}catch{}
			}
			
			// Drop any shared buffers etc if needed here:
			if(ReceiveBuffer!=null){
				ReceiveBuffer.Return();
				ReceiveBuffer=null;
			}
			
		}
		
		public void Send(Message message){
			
			byte[] buffer=message.GetResult();
			int length=(int)message.Length;
			
			Send(buffer,0,length);
			
		}
		
		public void Send(byte[] buffer){
			Send(buffer,0,buffer.Length);
		}
		
		public void Send(byte[] buffer,int offset,int length){
			
			if(Socket==null){
				return;
			}
			
			Socket.Send(buffer,offset,length,0);
		}
		
		
	}

}