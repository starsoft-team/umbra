using System;
using System.IO;

namespace Runescape{


	/// <summary>
	/// A binary reader which can have some extra methods added to it that are RS data specific.
	/// </summary>

	public class RSReader:BinaryReader{
		
		public RSReader(Stream stream):base(stream){}
		
		public RSReader(byte[] data):base(new MemoryStream(data)){}
		
		/*
		public override string ReadString(int length){
			Log.Add("Wrong overload! Use ReadJagString",true);
			return null;
		}
		*/
		
		public string ReadJagString(){
			
			// Read til null:
			string str="";
			
			char cur=ReadChar();
			
			while(cur!='\0'){
				// pretty ugly, but ok for now!
				str+=cur;
				cur=ReadChar();
			}
			
			return str;
			
		}
		
		public long Position{
			get{
				return BaseStream.Position;
			}
		}
		
		public long Length{
			get{
				return BaseStream.Length;
			}
		}
		
		public int Opcode(){
			return ReadByte();
		}
		
	}
	
}