using System;
using System.IO;

namespace Runescape{


	/// <summary>
	/// A binary writer which can have some extra methods added to it that are RS data specific.
	/// </summary>

	public class RSWriter:BinaryWriter{
		
		public RSWriter(Stream stream):base(stream){}
		
		public RSWriter():base(new MemoryStream()){}
		
		/// <summary>Gets the content written to the stream as a block of bytes.</summary>
		/// <returns>The block of bytes.</returns>
		public virtual byte[] GetFullResult(){
			byte[] r=((MemoryStream)BaseStream).ToArray();
			BaseStream.Close();
			BaseStream.Dispose();
			return r;
		}
		
		/// <summary>Warning: length of the result is invalid, but this is faster than GetFullResult.
		/// If this is used, use with Length.
		/// Gets the content written to the stream as a block of bytes.</summary>
		/// <returns>The block of bytes.</returns>
		public virtual byte[] GetResult(){
			return ((MemoryStream)BaseStream).GetBuffer();
		}
		
		public override void Write(string value){
			
			// RS uses a null terminated string.
			
			byte[] bytes=System.Text.Encoding.UTF8.GetBytes(value);
			
			// Write the string:
			Write(bytes);
			
			// And the null:
			Write((byte)0);
			
		}
		
		public long Length{
			get{
				return BaseStream.Length;
			}
		}
		
	}
	
}