namespace Runescape{


	public partial class Player{
		
		/// <summary>The RS server link.</summary>
		public RSServer Server;
		/// <summary>The world their logged into.</summary>
		public World World;
		public string Username;
		public string Password;
		/// <summary>Callback to call when data is received from the RS server.</summary>
		public DataCallback OnDataFromServer;
		
		
		public Player(World world){
			World=world;
		}
		
		public Player(int world,string username,string password){
			
			// Setup data:
			World=Worlds.Get(world);
			Username=username;
			Password=password;
			
			// Connect right now:
			ConnectToRunescape();
		}
		
	}
	
}