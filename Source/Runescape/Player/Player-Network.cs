namespace Runescape{


	public partial class Player{
		
		
		/// <summary>Received the given data from an actual RS server.</summary>
		public void ReceivedFromRunescape(byte[] data,int offset,int length){
			
			if(OnDataFromServer!=null){
				
				// Middleman mode - this will basically forward the data to the (official) RS client
				OnDataFromServer(data,offset,length);
				
			}
			
			// - Try decoding the message here! -
			if(Server.Writer==null){
				Server.Writer=new RSWriter();
			}
			
			// Write to the writer:
			Server.Writer.Write(data,offset,length);
			
			if(Server.Reader==null){
				Server.Reader=new RSReader(Server.Writer.BaseStream);
				
				while(Server.Reader.Position < Server.Reader.Length){
					// This will block the thread if a message hasn't been received in full.
					// That's actually what we want - another call will see Reader!=null and 
					// not enter this block anyway.
					OnMessage(Server.Reader);
				}
				
				// Clear both, allowing more messages to come in:
				Server.Writer=null;
				Server.Reader=null;
				
			}
			
		}
		
		public void OnMessage(RSReader reader){
			
			int opcode=reader.Opcode();
			
			// Encrypted?
			if(Server.GotKey){
				// Un-hash it:
				
			}
			
			Log.Add("["+opcode+"] <-");
			
			/*
			switch(opcode){
				case :
				
				break;
				
				case :
				
				break;
				
				case :
				
				break;
				
				case :
				
				break;
				
				case :
				
				break;
				
				case :
				
				break;
				
				case :
				
				break;
			}
			*/
			
		}
		
		public void ConnectToRunescape(){
			
			Server=new RSServer(this);
			
		}
		
		/// <summary>Called when an RS server is connected and ready to go.</summary>
		public void OnConnected(){
			
			// Welcome messages etc
			
			Log.Add("Connected to RS server for world "+World.ID+"!");
			
			// Login message
			// Message message=new Message();
			// message.Write(username);
			// message.Write(password);
			// Server.Send(message);
			
		}
		
		
	}
	
}