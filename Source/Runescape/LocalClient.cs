using System;
using System.Net;
using System.Net.Sockets;

namespace Runescape{

	/// <summary>
	/// Handles a client connected locally in middleman mode.
	///
	/// [RS Client] -> [UMBRA.LocalServer] -> [UMBRA.LocalClient] -> [UMBRA.RSServer] -> [RS Server]
	///
	/// </summary>


	public class LocalClient:SocketLink{
		
		/// <summary>Local client ID.</summary>
		public int LocalID;
		/// <summary>The player.</summary>
		public Player Player;
		
		
		public LocalClient(Socket socket,World world):base(socket){
			
			// Create the player:
			Player=new Player(world);
			
			// Hook up the raw data callback:
			Player.OnDataFromServer=OnDataFromServer;
			
		}
		
		/// <summary>Received the given data from an actual RS server (middleman mode).</summary>
		public void OnDataFromServer(byte[] data,int offset,int length){
			
			// Send straight through to the RS client:
			Send(data,offset,length);
			
		}
		
		/// <summary>Called when the local player disconnects. *Watch out for infinite loops here!*
		/// I.e. it would be great if the server informed this client when it disconnects too (jic the RS server actually does go down)
		/// but that could result in a loop there.</summary>
		public override void Disconnect(){
			// DC:
			base.Disconnect();
			
			// DC player:
			if(Player.Server!=null){
				
				// Grab RS link:
				RSServer server=Player.Server;
				
				// Clear (anti-loop protection)
				Player.Server=null;
				
				// DC:
				server.Disconnect();
			}
		}
		
		/// <summary>Received the given data from an actual RS client (middleman mode).</summary>
		public override void Received(byte[] data,int offset,int length){
			
			if(Player.Server==null){
				return;
			}
			
			// Pass straight through to the RS server link:
			Player.Server.Send(data,offset,length);
			
			if(Writer==null){
				Writer=new RSWriter();
			}
			
			// Write to the writer:
			Writer.Write(data,offset,length);
			
			if(Reader==null){
				Reader=new RSReader(Writer.BaseStream);
				
				while(Reader.Position < Reader.Length){
					// This will block the thread if a message hasn't been received in full.
					// That's actually what we want - another call will see Reader!=null and 
					// not enter this block anyway.
					OnMessage(Reader);
				}
				
				// Clear both, allowing more messages to come in:
				Writer=null;
				Reader=null;
				
			}
			
		}
		
		public void OnMessage(RSReader reader){
			
			int opcode=reader.Opcode();
			
			// Encrypted?
			if(GotKey){
				// Un-hash it (ISAAC - the key is in two parts; one half is from the client, other from the server during login):
				
			}
			
			Log.Add("["+opcode+"] ->");
			
			switch(opcode){
				case 14:
					// Login request
					// Body is the hashed username (just take the username and treat it like a base 37 number)
					// Response from server (opcode 0) has the session key part we're after
					// Then the actual login contains the clients key part along with user/pass
				break;
				
				/*
				case :
				
				break;
				
				case :
				
				break;
				
				case :
				
				break;
				
				case :
				
				break;
				
				case :
				
				break;
				
				case :
				
				break;
				*/
			
			}
			
		}
		
	}
	
}