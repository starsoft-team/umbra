using System;
using System.Collections;
using System.Collections.Generic;
using Wrench;
using Nitro;

/// <summary>
/// A live command line using live Nitro.
/// </summary>

public static class LiveCommandLine{
	
	/// <summary>An object containing a bunch of globals.</summary>
	public static CodeGlobals Globals=new CodeGlobals();
	
	
	public static void Run(string text){
		Run(text,null);
	}
	
	public static void Run(string text,CommandContext ctx){
		
		if(!text.StartsWith("function")){
			
			text="function Start(){"+text+"}";
			
		}
		
		try{
			
			NitroCode script=new NitroCode(text,typeof(CommandLineScript),CommandLineScript.SecurityDomain);
			
			CommandLineScript codeInstance=script.Instance() as CommandLineScript;
			codeInstance.BaseScript=script;
			codeInstance.Context=ctx;
			
			if(codeInstance!=null){
				
				// Start now:
				codeInstance.Start();
				
				// Standard method that must be called.
				// Any code outside of functions gets dropped in here:
				codeInstance.OnScriptReady();
				
			}
			
		}catch(Exception e){
			
			Console.WriteLine("Error, Invalid Nitro: "+e);
			
		}
		
	}
	
}