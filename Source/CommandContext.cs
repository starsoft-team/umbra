public class CommandContext{
	
	/// <summary>An object containing a bunch of globals.</summary>
	public CodeGlobals Globals=new CodeGlobals();
	/// <summary>A place to divert console output to.</summary>
	public ResponseHandler Response;
	
}