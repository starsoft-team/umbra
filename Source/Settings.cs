public static class Settings{
	
	/// <summary>True if in middleman mode.</summary>
	public static bool Middleman;
	/// <summary>Server port.</summary>
	public static int Port=43594;
	
}