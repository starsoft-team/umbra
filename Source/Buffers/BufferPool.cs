/// <summary>
/// This pools the allocation of blocks of 512 bytes.
/// Used when receiving data from a socket.
/// </summary>

public static class BufferPool{
	
	/// <summary>The size of buffers in this pool.</summary>
	public const int BufferSize=512;
	
	public static ByteBuffer First;
	
	
	public static ByteBuffer Get(){
		if(First==null){
			return new ByteBuffer(new byte[BufferSize],BufferSize);
		}
		
		ByteBuffer result;
		
		lock(First){
			result=First;
			First=result.After;
		}
		
		result.After=null;
		
		return result;
	}
	
}