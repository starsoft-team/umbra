/// <summary>
/// A block of bytes. These things are pooled so they can be shared out.
/// </summary>

public class ByteBuffer{
	
	/// <summary>The length of Bytes.</summary>
	public int Length;
	/// <summary>The bytes themselves.</summary>
	public byte[] Bytes;
	/// <summary>When this object is in the pool, this is the object after.</summary>
	public ByteBuffer After;
	
	
	public ByteBuffer(byte[] bytes,int length){
		Bytes=bytes;
		Length=length;
	}
	
	/// <summary>Returns this bytes object to the pool it came from.</summary>
	public void Return(){
		
		After=BufferPool.First;
		BufferPool.First=this;
		
	}
	
}