using System;
using System.IO;
using System.Threading;


public static class EntryPoint{
	
	public static void Main(string[] args){
	
		// Jump to the exe path:
		Directory.SetCurrentDirectory(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location));
		
		// Setup args:
		Arguments.Load(args);
		
		// Get the game:
		string game=Arguments.Get("game","rs").ToLower();
		
		if(game=="rs" || game=="runescape"){
			
			// Start it:
			Runescape.EntryPoint.Start();
			
		}else{
			
			// Start it:
			Tribalwars.EntryPoint.Start();
			
		}
		
		Thread.Sleep(Timeout.Infinite);
	}
	
}