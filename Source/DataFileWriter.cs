using System;
using System.IO;
using System.Text;


public class DataFileWriter{
	
	/// <summary>Total length to go between flushes.</summary>
	public const int FlushSize=0;//512;
	
	public string Path;
	public FileStream Stream;
	public StringBuilder Writer=new StringBuilder();
	
	
	public DataFileWriter(string path){
		Path=path;
		
		Stream=new FileStream(path,FileMode.OpenOrCreate,FileAccess.Write);
		Stream.SetLength(0);
		
	}
	
	public void Close(){
		
		Flush();
		Stream.Close();
		Stream=null;
		
	}
	
	public void Flush(){
		
		// Get text block:
		string block=Writer.ToString();
		
		if(block==""){
			return;
		}
		
		// Get it's bytes:
		byte[] textBytes=System.Text.Encoding.UTF8.GetBytes(block);
		
		// Write to file stream:
		Stream.Write(textBytes,0,textBytes.Length);
		
		// Flush it:
		Stream.Flush();
		
		// Clear it:
		Writer.Length=0;
		
	}
	
	public void WriteLine(string text){
		Writer.Append(text);
		Writer.Append('\n');
		
		if(Writer.Length>FlushSize){
			Flush();
		}
		
	}
	
	public void Write(string text){
		Writer.Append(text);
		
		if(Writer.Length>FlushSize){
			Flush();
		}
		
	}
	
}