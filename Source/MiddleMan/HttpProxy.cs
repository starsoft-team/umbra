using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;

namespace SimpleHttpProxy
{
    public class HttpProxy
    {
		
		public static bool ForceHttps=true;
		
		public static string[] ProxiedDomains=new string[]{
			"oldschool1.runescape.com","8.42.17.164",
			"en.tribalwars2.com","212.72.179.16"
		};
		
        public static void Start()
        {
            var listener = new HttpListener();
            listener.Prefixes.Add("http://*:80/");
            // listener.Prefixes.Add("https://*:443/");
            listener.Start();
            
			Log.Add("HTTP proxy is up!");
			
            while (true)
            {
                var ctx = listener.GetContext();
                new Thread(new Relay(ctx).ProcessRequest).Start();
            }
        }
    }

    public class Relay
    {
        private readonly HttpListenerContext originalContext;

        public Relay(HttpListenerContext originalContext)
        {
            this.originalContext = originalContext;
        }

        public void ProcessRequest()
        {
			string rawUrl=originalContext.Request.Url.ToString();
			
			// Here we make sure we don't trigger an infinite loop:
			for(int i=0;i<HttpProxy.ProxiedDomains.Length;i+=2){
				
				string dom=HttpProxy.ProxiedDomains[i];
				string ip=HttpProxy.ProxiedDomains[i+1];
				
				// swap:
				rawUrl=rawUrl.Replace(dom,ip);
				
			}
			
			if(HttpProxy.ForceHttps){
				// We request the service with https:
				rawUrl=rawUrl.Replace("http://","https://");
			}
			
			// Log.Add("Http request for "+rawUrl+"..");
			
            var relayRequest = (HttpWebRequest) WebRequest.Create(rawUrl);
            relayRequest.KeepAlive = false;
            relayRequest.Proxy.Credentials = CredentialCache.DefaultCredentials;
            relayRequest.UserAgent = this.originalContext.Request.UserAgent;
           
            var requestData = new RequestState(relayRequest, originalContext);
            relayRequest.BeginGetResponse(ResponseCallBack, requestData);
        }

        private static void ResponseCallBack(IAsyncResult asynchronousResult)
        {
            var requestData = (RequestState) asynchronousResult.AsyncState;
            
            using (var responseFromWebSiteBeingRelayed = (HttpWebResponse) requestData.webRequest.EndGetResponse(asynchronousResult))
            {
                using (var responseStreamFromWebSiteBeingRelayed = responseFromWebSiteBeingRelayed.GetResponseStream())
                {
                    var originalResponse = requestData.context.Response;
					CopyTo(responseStreamFromWebSiteBeingRelayed,originalResponse.OutputStream);
                    originalResponse.OutputStream.Close();
                }
            }
        }
		
		public static void CopyTo(Stream input, Stream output)
		{
			byte[] buffer = new byte[1024]; // Fairly arbitrary size
			int bytesRead;

			while ((bytesRead = input.Read(buffer, 0, buffer.Length)) > 0)
			{
				output.Write(buffer, 0, bytesRead);
			}
			
		}
    }

    public static class ConsoleUtilities
    {
        public static void WriteRequest(string info)
        {
            Console.WriteLine(info);
        }
        public static void WriteResponse(string info)
        {
            Console.WriteLine(info);
        }
    }

    public class RequestState
    {
        public readonly HttpWebRequest webRequest;
        public readonly HttpListenerContext context;

        public RequestState(HttpWebRequest request, HttpListenerContext context)
        {
            webRequest = request;
            this.context = context;
        }
    }

}