/// <summary>Simple delegate used when a player receives data from the server.</summary>
public delegate void DataCallback(byte[] data,int offset,int length);