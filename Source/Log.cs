using System;


public static class Log{
	
	public static bool Stacktrace;
		
	public static void Add(string message){
		Add(message,Stacktrace);
	}
	
	public static void Add(string message,bool trace){
		Console.WriteLine(message);
		if(trace){
			Console.WriteLine(Environment.StackTrace);
		}
	}
	
}