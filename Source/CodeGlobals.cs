using System;
using System.Collections;
using System.Collections.Generic;


public class CodeGlobals{
	
	/// <summary>A set of globals to use.</summary>
	public Dictionary<string,object> Globals=new Dictionary<string,object>();
	
	public void Set(string varName,object value){
		this[varName]=value;
	}
	
	public object this[string varName]{
		get{
			object result;
			Globals.TryGetValue(varName.ToLower(),out result);
			return result;
		}
		set{
			Globals[varName.ToLower()]=value;
		}
	}
	
}