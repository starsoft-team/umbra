using System;
using PowerUI;


public static class TimedQueues{
	
	/// <summary>Set of all currently ticking queues.</summary>
	public static TimedQueue Last;
	public static TimedQueue First;
	/// <summary>The main timer ticking them all.</summary>
	public static UITimer Timer;
	/// <summary>Random numbers for wait commands.</summary>
	public static System.Random Rand;
	/// <summary>used to auto collect and auto start general jobs.</summary>
	public static TimedQueue SharedQueue;
	
	
	/// <summary>Waits until a random delayed number of seconds after the given utc unix time in ms.</summary>
	public static TimeEvent WaitDelayed(long time,OnTimeEvent run){
		
		return Wait(time,3000,8000,run);
		
	}
	
	public static TimeEvent Wait(long time,int randMin,int randMax,OnTimeEvent run){
		
		if(Rand==null){
			Rand=new System.Random();
		}
		
		// Add a random amount to time:
		time+=Rand.Next(randMin,randMax);
		
		return Wait(time,run);
		
	}
	
	public static TimeEvent Wait(long time,Nitro.DynamicMethod<Nitro.Void> nitroMethod){
		
		return Wait(time,delegate(TimeEvent tr){
			
			nitroMethod.Run(tr);
			
		});
		
	}
	
	public static TimeEvent Wait(long time,OnTimeEvent run){
		
		if(SharedQueue==null){
			
			SharedQueue=new TimedQueue();
			
		}
		
		TimeEvent evt=new TimeEvent(time,run);
		
		SharedQueue.Add(evt);
		
		return evt;
		
	}
	
	private static void Setup(){
		
		if(Timer!=null){
			return;
		}
		
		// Create a timer:
		Timer=new UITimer(false,1,OnTimerTick);
		
	}
	
	private static void OnTimerTick(){
		
		long time=Time.NowMilliseconds;
		
		TimedQueue queue=First;
		
		while(queue!=null){
			
			queue.Update(time);
			
			queue=queue.NextQueue;
			
		}
		
	}
	
	/// <summary>Add to the queue.</summary>
	public static void Add(TimedQueue queue){
		
		if(Timer==null){
			Setup();
		}
		
		if(First==null){
			
			First=Last=queue;
			
		}else{
			
			queue.PreviousQueue=Last;
			Last=Last.NextQueue=queue;
			
		}
		
	}
	
}