//--------------------------------------
//               PowerUI
//
//        For documentation or 
//    if you have any issues, visit
//        powerUI.kulestar.com
//
//    Copyright � 2013 Kulestar Ltd
//          www.kulestar.com
//--------------------------------------

using System;


namespace PowerUI{
	
	/// <summary>
	/// A UI Event represents a click or keypress.
	/// An event object is always provided with any onmousedown/onmouseup/onkeydown etc.
	/// </summary>
	
	public partial class UIEvent{
		
	}
	
}