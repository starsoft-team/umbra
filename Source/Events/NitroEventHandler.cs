//--------------------------------------
//               PowerUI
//
//        For documentation or 
//    if you have any issues, visit
//        powerUI.kulestar.com
//
//    Copyright � 2013 Kulestar Ltd
//          www.kulestar.com
//--------------------------------------

using System;
using Nitro;


namespace PowerUI{
	
	/// <summary>
	/// Describes either a nitro event or a delegate.
	/// </summary>
	
	public class NitroEventHandler:EventHandler{
		
		/// <summary>The nitro method to run.</summary>
		public DynamicMethod<Nitro.Void> NitroMethod;
		
		
		/// <summary>Call this to run the method.</summary>
		public object Run(UIEvent e){
			return NitroMethod.Run(e);
		}
		
	}
	
}