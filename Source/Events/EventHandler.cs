//--------------------------------------
//               PowerUI
//
//        For documentation or 
//    if you have any issues, visit
//        powerUI.kulestar.com
//
//    Copyright � 2013 Kulestar Ltd
//          www.kulestar.com
//--------------------------------------

using System;


namespace PowerUI{
	
	/// <summary>A delegate used when e.g. the dimensions change or the document is unloaded.</summary>
	public delegate void OnWindowEvent(UIEvent e);
	
	/// <summary>
	/// Describes either a nitro event or a delegate.
	/// </summary>
	
	public interface EventHandler{
		
		object Run(UIEvent e);
		
	}
	
}