//--------------------------------------
//               PowerUI
//
//        For documentation or 
//    if you have any issues, visit
//        powerUI.kulestar.com
//
//    Copyright � 2013 Kulestar Ltd
//          www.kulestar.com
//--------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using Nitro;


namespace PowerUI{
	
	/// <summary>
	/// A set of events for a given object (document or element). Used by addEventListener.
	/// </summary>
	
	public class EventsSet{
		
		/// <summary>The handlers in this set. May be null.</summary>
		public Dictionary<string,List<EventHandler>> Handlers;
		
		
		/// <summary>Gets the set of handlers for the given event name.</summary>
		public List<EventHandler> Get(string name){
			
			if(Handlers==null){
				return null;
			}
			
			// Try getting the handler set:
			List<EventHandler> list;
			Handlers.TryGetValue(name,out list);
			
			return list;
		}
		
		/// <summary>Runs the event with the given name by passing it the given event object.</summary>
		public object Run(string name,UIEvent e){
			
			if(Handlers==null){
				return null;
			}
			
			object result=null;
			
			// Try getting the handler:
			List<EventHandler> list;
			if(Handlers.TryGetValue(name,out list)){
				
				// How many?
				int count=list.Count;
				
				// Run each one:
				for(int i=0;i<count;i++){
					result=list[i].Run(e);
				}
				
			}
			
			return result;
			
		}
		
		/// <summary>Internally adds an event handler for the given named event.</summary>
		private void Add(string name,EventHandler handler){
			
			if(Handlers==null){
				Handlers=new Dictionary<string,List<EventHandler>>();
			}
			
			List<EventHandler> list;
			if(!Handlers.TryGetValue(name,out list)){
				
				// Create a new list:
				list=new List<EventHandler>();
				
				// Add it:
				Handlers[name]=list;
				
			}
			
			list.Add(handler);
			
		}
		
		/// <summary>Adds a handler for the given function name.</summary>
		public void Add(string name,DynamicMethod<Nitro.Void> mtd){
			
			if(mtd==null){
				Handlers.Remove(name);
				return;
			}
			
			// Create the handler:
			NitroEventHandler handler=new NitroEventHandler();
			handler.NitroMethod=mtd;
			Add(name,handler);
			
		}
		
		/// <summary>Adds a handler for the given function.</summary>
		public void Add(string name,OnWindowEvent mtd){
			
			if(mtd==null){
				Handlers.Remove(name);
				return;
			}
			
			// Create the handler:
			DelegateEventHandler handler=new DelegateEventHandler();
			handler.DelegateMethod=mtd;
			Add(name,handler);
			
		}
		
		/// <summary>Gets the first delegate event handler for the given event name.</summary>
		public OnWindowEvent GetFirstDelegateEvent(string name){
			// Get the set:
			List<EventHandler> handlers=Get(name);
			
			if(handlers==null){
				return null;
			}
			
			DelegateEventHandler dHandler=handlers[0] as DelegateEventHandler;
			
			if(dHandler==null){
				return null;
			}
			
			return dHandler.DelegateMethod;
		}
		
		/// <summary>Gets the first nitro event handler for the given event name.</summary>
		public DynamicMethod<Nitro.Void> GetFirstNitroEvent(string name){
			// Get the set:
			List<EventHandler> handlers=Get(name);
			
			if(handlers==null){
				return null;
			}
			
			NitroEventHandler nHandler=handlers[0] as NitroEventHandler;
			
			if(nHandler==null){
				return null;
			}
			
			return nHandler.NitroMethod;
		}
		
		/// <summary>Removes the given method from this set.</summary>
		public void Remove(string name,DynamicMethod<Nitro.Void> mtd){
			
			// Get the set:
			List<EventHandler> handlerSet=Get(name);
			
			if(handlerSet==null){
				return;
			}
			
			for(int i=0;i<handlerSet.Count;i++){
				
				NitroEventHandler nHandler=handlerSet[i] as NitroEventHandler;
				
				if(nHandler==null){
					continue;
				}
				
				if(nHandler.NitroMethod==mtd){
					
					if(handlerSet.Count==1){
						// Remove it all:
						Handlers.Remove(name);
					}else{
						// Remove just this:
						handlerSet.RemoveAt(i);
					}
					
					return;
					
				}
				
			}
			
		}
		
		/// <summary>Removes the given method from this set.</summary>
		public void Remove(string name,OnWindowEvent mtd){
			
			// Get the set:
			List<EventHandler> handlerSet=Get(name);
			
			if(handlerSet==null){
				return;
			}
			
			for(int i=0;i<handlerSet.Count;i++){
				
				DelegateEventHandler dHandler=handlerSet[i] as DelegateEventHandler;
				
				if(dHandler==null){
					continue;
				}
				
				if(dHandler.DelegateMethod==mtd){
					
					if(handlerSet.Count==1){
						// Remove it all:
						Handlers.Remove(name);
					}else{
						// Remove just this:
						handlerSet.RemoveAt(i);
					}
					
					return;
					
				}
				
			}
			
		}
		
	}
	
}