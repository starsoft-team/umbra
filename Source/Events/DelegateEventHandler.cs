//--------------------------------------
//               PowerUI
//
//        For documentation or 
//    if you have any issues, visit
//        powerUI.kulestar.com
//
//    Copyright � 2013 Kulestar Ltd
//          www.kulestar.com
//--------------------------------------

using System;


namespace PowerUI{
	
	/// <summary>
	/// Describes either a delegate.
	/// </summary>
	
	public class DelegateEventHandler:EventHandler{
		
		/// <summary>The delegated method to run.</summary>
		public OnWindowEvent DelegateMethod;
		
		
		/// <summary>Call this to run the method.</summary>
		public object Run(UIEvent e){
			DelegateMethod(e);
			return null;
		}
		
	}
	
}