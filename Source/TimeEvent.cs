public delegate void OnTimeEvent(TimeEvent entry);
public delegate string OnDebugEvent(TimeEvent entry);

public class TimeEvent:QueueEntry{
	
	
	public OnTimeEvent EventToRun;
	public OnDebugEvent OnDebug;
	
	
	public TimeEvent(long delayUntil,OnTimeEvent onRun){ // UTC time to wait until.
		
		TimeCompletes=delayUntil;
		EventToRun=onRun;
		
	}
	
	public override string ToString(){
		
		if(OnDebug!=null){
			
			return OnDebug(this);
			
		}
		
		return "TimeEvent";
		
	}
	
	public override void OnComplete(){
		
		EventToRun(this);
		
	}
	
}