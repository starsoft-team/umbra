using System;


public delegate void QueueEventHandler(string name,QueueEntry entry,TimedQueue queue);

public class TimedQueue{
	
	/// <summary>Should this queue tick?</summary>
	public bool Tick=true;
	/// <summary>First thing in the queue. It's a linked list so loop through from here.</summary>
	public QueueEntry FirstEntry;
	/// <summary>First thing in the queue. It's a linked list so loop through from here.</summary>
	public QueueEntry LastEntry;
	/// <summary>All the queues are stored in a linked list for rapid ticking.</summary>
	public TimedQueue NextQueue;
	/// <summary>All the queues are stored in a linked list for rapid ticking.</summary>
	public TimedQueue PreviousQueue;
	/// <summary>True if this queue is active.</summary>
	private bool IsTicking;
	
	public event QueueEventHandler OnEvent;
	
	
	public void Add(){
		if(IsTicking){
			return;
		}
		
		IsTicking=true;
		
		// Add to timed set and start ticking:
		TimedQueues.Add(this);
		
	}
	
	/// <summary>Removes this queue from the timed queues set.</summary>
	public void Remove(){
		
		if(!IsTicking){
			return;
		}
		
		IsTicking=false;
		
		if(PreviousQueue==null){
			
			TimedQueues.First=NextQueue;
			
		}else{
			
			PreviousQueue.NextQueue=NextQueue;
			
		}
		
		if(NextQueue==null){
			
			TimedQueues.Last=PreviousQueue;
			
		}else{
			
			NextQueue.PreviousQueue=PreviousQueue;
			
		}
		
		if(TimedQueues.First==null){
			
			// Stop the timer:
			TimedQueues.Timer.Stop();
			TimedQueues.Timer=null;
			
		}
		
	}
	
	public void Run(string name,QueueEntry entry){
		
		if(OnEvent!=null){
			
			OnEvent(name,entry,this);
			
		}
		
	}
	
	public void Update(long time){
		
		if(FirstEntry==null){
			return;
		}
		
		QueueEntry entry=FirstEntry;
		
		while(time>entry.TimeCompletes){
			
			QueueEntry next=entry.Next;
			
			// Remove it:
			entry.Remove();
			
			// It got completed:
			entry.OnComplete();
			
			// Run an event to say something completed:
			Run("complete",entry);
			
			entry=next;
			
		}
		
	}
	
	public void Clear(){
		FirstEntry=null;
		LastEntry=null;
		
		// Remove the queue if it's ticking:
		Remove();
	}
	
	public override string ToString(){
		
		string result="";
		
		QueueEntry entry=FirstEntry;
		
		while(entry!=null){
			
			if(result!=""){
				result+="\r\n";
			}
			
			result+=entry.ToString();
			
			entry=entry.Next;
			
		}
		
		return result;
		
	}
	
	public long OfficialEnd{
		get{
			
			QueueEntry current=LastEntry;
			
			while(current!=null){
				
				// In progress?
				if(current.InProgress){
					
					return current.TimeCompletes;
					
				}
				
				current=current.Previous;
				
			}
			
			// Already ended:
			return 0;
			
		}
	
	}
	
	public bool SomethingInProgress{
		get{
			
			QueueEntry current=LastEntry;
			
			while(current!=null){
			
				// In progress?
				if(current.InProgress){
					
					// True if it completes in less than 3.1s.
					// This is because we attempt to schedule something 3s before the previous entry completes.
					// Aka this is checked at a point when we know something isn't 
					// complete but we don't want it to trigger a return true.
					if(current.TimeRemaining>3100){
						
						return true;
						
					}
					
				}
				
				current=current.Previous;
				
			}
			
			return false;
			
		}
	}
	
	public void RecomputeTimes(){
	
		// For each "non in progress" entry, make sure it's time is previous + duration.
		long previous=Time.NowMilliseconds;
		
		QueueEntry current=FirstEntry;
		
		while(current!=null){
			
			if(!current.InProgress){
				
				// Not in progress - update time:
				current.TimeCompletes=previous + current.Duration;
				
			}
			
			// Grab prev:
			previous=current.TimeCompletes;
			
			current=current.Next;
		}
		
	}
	
	public void InsertAfter(QueueEntry entry,QueueEntry after){
		
		if(!IsTicking && Tick){
			
			// Make sure this queue is being updated:
			Add();
			
		}
		
		// be sure its not in some other queue:
		entry.Remove();
		
		entry.InQueue=this;
		
		
		if(after==null){
			// Add to the front of the queue.
			entry.Next=FirstEntry;
			FirstEntry=entry;
		}else{
			
			entry.Next=after.Next;
			entry.Previous=after;
			after.Next=entry;
			
		}
		
		if(entry.Next==null){
			LastEntry=entry;
		}else{
			entry.Next.Previous=entry;
		}
		
	}
	
	/// <summary>Add to the queue.</summary>
	public void Add(QueueEntry entry){
		
		if(!IsTicking && Tick){
			
			// Make sure this queue is being updated:
			Add();
			
		}
		
		// be sure its not in some other queue:
		entry.Remove();
		
		entry.InQueue=this;
		
		if(FirstEntry==null){
			
			FirstEntry=LastEntry=entry;
			
		}else{
			
			// Must insert in time order.
			
			if(entry.TimeCompletes<=FirstEntry.TimeCompletes){
				
				// Right at the front:
				entry.Previous=null;
				entry.Next=FirstEntry;
				FirstEntry.Previous=entry;
				FirstEntry=entry;
				
			}else{
				
				QueueEntry current=LastEntry;
				
				while(current!=null){
					
					if(entry.TimeCompletes>=current.TimeCompletes){
						
						// Goes after current.
						entry.Next=current.Next;
						entry.Previous=current;
						
						if(current.Next==null){
							LastEntry=entry;
						}else{
							current.Next.Previous=entry;
						}
						
						current.Next=entry;
						
						break;
						
					}
					
					current=current.Previous;
				}
				
			}
			
		}
		
	}
	
}