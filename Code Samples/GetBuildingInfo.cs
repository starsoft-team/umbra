// Getting a particular buildings general information:
Building build=Buildings.Get("warehouse");

// Or from a connected players village:

BuildingLevelInfo build=village.GetBuilding("warehouse");

// e.g. build.Level, build.Upgrade() etc.