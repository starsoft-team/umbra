
Village max=new Village(637,677,"Burkviz");
Village loki1=new Village(656,681,"TL001");
Village loki2=new Village(654,681,"TL002");
Village capitol=new Village(632,686,"The Capitol");
Village jv=new Village(631,689,"Johnnyville");
Village banzai=new Village(634,689,"Banzaiville");
Village casaMia=new Village(664,667,"Casa mia");
Village xzen1=new Village(656,676,"001 BALSURSUR");
Village boz=new Village(668,667,"Roma");

// Target UTC landing time, 10s spacing:
LandingTimes times=new LandingTimes(1446298080,10);

// TL002
times.AddNoble(max,loki2);
times.AddNoble(banzai,loki2);
times.AddNoble(jv,loki2);
times.AddNoble(capitol,loki2);

// XZENS
times.AddNoble(boz,xzen1);
times.AddNoble(boz,xzen1);

// TL001
times.AddNoble(capitol,loki1);
times.AddNoble(casaMia,loki1);
times.AddNoble(casaMia,loki1);

Console.WriteLine(times.ToString());
