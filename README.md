### Umbra ###

Umbra is a "headless client" which mostly does all kinds of things with Tribal Wars 2, such as automated building and recruitment and infinite queue lengths. This project is *not* considered a hacking or cheating tool - it's simply intended to be a fun way to explore the game in a different way.


### How it works ###

To make it simple, Umbra is mainly designed to operate via private messaging - a player simply messages an account with things like "upgrade timber_camp to 20" and it will keep bumping it up until it's there. One player, the account being messaged, runs Umbra (possibly e.g. on a server). Other players must have connected their account to this player for it to be able to have access to things like their building queues; this is done by them first sending "cookie" over pm, or become co-players.